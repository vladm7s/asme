package com.asme.core.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author Vladyslav Mykhalets
 * @since 23.10.17
 */
@ConfigurationProperties("postgis")
public class PostgisProperties {
}
