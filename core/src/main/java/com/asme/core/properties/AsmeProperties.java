package com.asme.core.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author Vladyslav Mykhalets
 */
@ConfigurationProperties("asme")
public class AsmeProperties {

    private boolean disableTrustManager;

    public boolean isDisableTrustManager() {
        return disableTrustManager;
    }

    public void setDisableTrustManager(boolean disableTrustManager) {
        this.disableTrustManager = disableTrustManager;
    }
}
