package com.asme.core.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author Vladyslav Mykhalets
 */
@ConfigurationProperties("osrm")
public class OsrmProperties {

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
