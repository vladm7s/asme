package com.asme.core.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.asme.core.entity.jpa.JpaPlanRequest;

/**
 * @author Vladyslav Mykhalets
 * @since 07.01.18
 */
@Repository
public interface JpaPlanRequestRepository extends JpaRepository<JpaPlanRequest, Long> {
}
