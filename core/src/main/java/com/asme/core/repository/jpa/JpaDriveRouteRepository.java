package com.asme.core.repository.jpa;

import java.util.stream.Stream;

import javax.persistence.QueryHint;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.asme.core.entity.jpa.JpaDriveRoute;
import com.vividsolutions.jts.geom.LineString;

import static org.hibernate.jpa.QueryHints.HINT_FETCH_SIZE;

/**
 * @author Vladyslav Mykhalets
 * @since 22.11.17
 */
@Repository
public interface JpaDriveRouteRepository extends JpaRepository<JpaDriveRoute, Long> {

    @Query(value = "SELECT route FROM JpaDriveRoute AS route " +
            "WHERE intersects(route.route, :start) = TRUE " +
            "AND intersects(route.route, :destination) = TRUE " +
            "AND route.routeStatus = 'OPEN' " +
            "AND route.userId != :userId",
            countQuery = "SELECT count(route) FROM JpaDriveRoute AS route " +
                    "WHERE intersects(route.route, :start) = TRUE " +
                    "AND intersects(route.route, :destination) = TRUE " +
                    "AND route.routeStatus = 'OPEN' " +
                    "AND route.userId != :userId")
    Page<JpaDriveRoute> findByStartAndDestination(@Param("userId") Long userId,
                                                  @Param("start") LineString start,
                                                  @Param("destination") LineString destination,
                                                  Pageable pageable);

    @QueryHints(value = @QueryHint(name = HINT_FETCH_SIZE, value = "200"))
    @Query(value = "SELECT route FROM JpaDriveRoute AS route " +
            "WHERE intersects(route.route, :start) = TRUE " +
            "AND intersects(route.route, :destination) = TRUE " +
            "AND route.routeStatus = 'OPEN' " +
            "AND route.userId != :userId")
    Stream<JpaDriveRoute> findByStartAndDestination(@Param("userId") Long userId,
                                                    @Param("start") LineString start,
                                                    @Param("destination") LineString destination);
}
