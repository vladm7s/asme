package com.asme.core.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.asme.core.entity.jpa.JpaPlanRoute;

/**
 * @author Vladyslav Mykhalets
 * @since 23.10.17
 */
@Repository
public interface JpaPlanRouteRepository extends JpaRepository<JpaPlanRoute, Long> {
}
