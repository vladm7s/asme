package com.asme.core.repository.jpa;

import java.util.stream.Stream;

import javax.persistence.QueryHint;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.asme.core.entity.jpa.JpaDriveRequest;
import com.vividsolutions.jts.geom.LineString;

import static org.hibernate.jpa.QueryHints.HINT_FETCH_SIZE;

/**
 * @author Vladyslav Mykhalets
 * @since 22.11.17
 */
@Repository
public interface JpaDriveRequestRepository extends JpaRepository<JpaDriveRequest, Long> {

//    @Query(value = "SELECT request FROM JpaDriveRequest AS request " +
//            "WHERE intersects(" +
//                "request.startLocationBoundingBox, " +
//                "(SELECT foundRoute.route FROM JpaDriveRoute AS foundRoute WHERE foundRoute.id = :driveRouteId)" +
//            ") = TRUE " +
//            "AND " +
//                "intersects(" +
//                "request.destinationLocationBoundingBox, " +
//                "(SELECT foundRoute.route FROM JpaDriveRoute AS foundRoute WHERE foundRoute.id = :driveRouteId)" +
//            ") = TRUE",
//           countQuery = "SELECT count(request) FROM JpaDriveRequest AS request " +
//            "WHERE intersects(" +
//                "request.startLocationBoundingBox, " +
//                "(SELECT foundRoute.route FROM JpaDriveRoute AS foundRoute WHERE foundRoute.id = :driveRouteId)" +
//            ") = TRUE " +
//            "AND " +
//                "intersects(" +
//                "request.destinationLocationBoundingBox, " +
//                "(SELECT foundRoute.route FROM JpaDriveRoute AS foundRoute WHERE foundRoute.id = :driveRouteId)" +
//            ") = TRUE")
//    Page<JpaDriveRequest> findByRouteId(@Param("userId") Long userId,
//                                        @Param("driveRouteId") Long driveRouteId,
//                                        Pageable pageable);

    @Query(value = "SELECT request FROM JpaDriveRequest AS request " +
            "WHERE intersects(request.startLocationBoundingBox, :route) = TRUE " +
            "AND intersects(request.destinationLocationBoundingBox, :route) = TRUE " +
            "AND request.requestStatus = 'OPEN' " +
            "AND request.userId != :userId",
            countQuery = "SELECT count(request) FROM JpaDriveRequest AS request " +
                    "WHERE intersects(request.startLocationBoundingBox, :route) = TRUE " +
                    "AND intersects(request.destinationLocationBoundingBox, :route) = TRUE " +
                    "AND request.requestStatus = 'OPEN' " +
                    "AND request.userId != :userId")
    Page<JpaDriveRequest> findByRoute(@Param("userId") Long userId,
                                      @Param("route") LineString route,
                                      Pageable pageable);

    @QueryHints(value = @QueryHint(name = HINT_FETCH_SIZE, value = "200"))
    @Query(value = "SELECT request FROM JpaDriveRequest AS request " +
            "WHERE intersects(request.startLocationBoundingBox, :route) = TRUE " +
            "AND intersects(request.destinationLocationBoundingBox, :route) = TRUE " +
            "AND request.requestStatus = 'OPEN' " +
            "AND request.userId != :userId")
    Stream<JpaDriveRequest> streamByRoute(@Param("userId") Long userId,
                                          @Param("route") LineString route);
}
