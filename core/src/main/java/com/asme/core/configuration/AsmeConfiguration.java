package com.asme.core.configuration;

import org.springframework.boot.autoconfigure.web.WebClientAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

import com.asme.core.properties.AsmeProperties;
import com.asme.core.service.provider.RestTemplateProvider;

/**
 * @author Vladyslav Mykhalets
 */
@Configuration
@EnableConfigurationProperties(AsmeProperties.class)
@PropertySource("asme.properties")
@Import({
        WebClientAutoConfiguration.class
})
public class AsmeConfiguration {

    @Bean
    public RestTemplateProvider restTemplateProvider() {
        return new RestTemplateProvider();
    }
}
