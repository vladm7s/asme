package com.asme.core.configuration;

import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.asme.core.properties.PostgisProperties;

/**
 * @author Vladyslav Mykhalets
 * @since 23.10.17
 */
@Configuration
@EnableTransactionManagement
@EnableConfigurationProperties(PostgisProperties.class)
@PropertySource("postgis.properties")
@Import(HibernateJpaAutoConfiguration.class)
@EntityScan("com.asme.core.entity.jpa")
@EnableJpaAuditing
@EnableJpaRepositories(
        //entityManagerFactoryRef = "postgisEntityManagerFactory",
        //transactionManagerRef = "postgisTransactionManager",
        basePackages = "com.asme.core.repository.jpa")
public class PostgisConfiguration {

    // Hikari pool config
    // https://easyjava.ru/data/jpa/jpa-hibernate-i-postgresql-primery-nastrojki/

    //@Autowired
    //private PostgisProperties postgisProperties;

    @Bean//(name = "postgisDataSource")
    @ConfigurationProperties(prefix = "postgis")
    public DataSource dataSource() {
        return DataSourceBuilder.create().build();
    }

//    @Bean(name = "postgisEntityManagerFactory")
//    public LocalContainerEntityManagerFactoryBean barEntityManagerFactory(EntityManagerFactoryBuilder builder,
//                                                                          @Qualifier("postgisDataSource") DataSource dataSource) {
//        return builder.dataSource(dataSource)
//                        .packages("com.asme.core.entity.jpa")
//                        .persistenceUnit("postgis")
//                        .build();
//    }
//
//    @Bean(name = "postgisTransactionManager")
//    public PlatformTransactionManager postgisTransactionManager(@Qualifier("postgisEntityManagerFactory") EntityManagerFactory postgisEntityManagerFactory) {
//        return new JpaTransactionManager(postgisEntityManagerFactory);
//    }

    // 1 ----------------------//

//    <bean id="dataSource" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
//          <property name="driverClassName" value="${jdbc.driverClassName}" />
//          <property name="url" value="${jdbc.url}" />
//          <property name="username" value="${jdbc.username}" />
//          <property name="password" value="${jdbc.password}" />
//      </bean>
//    <bean id="entityManagerFactory" class="org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean">
//        <property name="dataSource" ref="dataSource" />
//
//        <property name="persistenceUnitName" value="myPersistenceUnit" />
//        <property name="packagesToScan" >
//            <list>
//                <value>org.mypackage.*.model</value>
//            </list>
//        </property>
//
//        <property name="jpaProperties">
//            <props>
//                <prop key="hibernate.dialect">${hibernate.dialect}</prop>
//                <prop key="hibernate.hbm2ddl.auto">${hibernate.hbm2ddl.auto}</prop>
//                <prop key="hibernate.show_sql">${hibernate.show_sql}</prop>
//                <prop key="hibernate.format_sql">${hibernate.format_sql}</prop>
//            </props>
//        </property>
//    </bean>
    // 2 --------------------//

//    @Bean
//    public DataSource dataSource() {
//        return new EmbeddedDatabaseBuilder().setType(H2).build();
//    }
//
//    @Bean
//    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource, JpaVendorAdapter jpaVendorAdapter) {
//        LocalContainerEntityManagerFactoryBean lef = new LocalContainerEntityManagerFactoryBean();
//        lef.setDataSource(dataSource);
//        lef.setJpaVendorAdapter(jpaVendorAdapter);
//        lef.setPackagesToScan("hello");
//        return lef;
//    }
//
//    @Bean
//    public JpaVendorAdapter jpaVendorAdapter() {
//        HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
//        hibernateJpaVendorAdapter.setShowSql(false);
//        hibernateJpaVendorAdapter.setGenerateDdl(true);
//        hibernateJpaVendorAdapter.setDatabase(Database.H2);
//        return hibernateJpaVendorAdapter;
//    }

    //---------------------//
}
