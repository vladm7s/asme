package com.asme.core.configuration;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.asme.core.properties.OsrmProperties;
import com.asme.core.service.osrm.OsrmService;
import com.asme.core.service.osrm.OsrmServiceImpl;

/**
 * @author Vladyslav Mykhalets
 */
@Configuration
@EnableConfigurationProperties(OsrmProperties.class)
@PropertySource("osrm.properties")
public class OsrmConfiguration {

    @Bean
    public OsrmService osrmService() {
        return new OsrmServiceImpl();
    }
}
