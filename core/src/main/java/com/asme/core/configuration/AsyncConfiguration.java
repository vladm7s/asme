package com.asme.core.configuration;

import java.util.concurrent.Executor;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurerSupport;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

/**
 * @author Vladyslav Mykhalets
 * @since 19.10.17
 */
@Configuration
@EnableAsync
public class AsyncConfiguration extends AsyncConfigurerSupport {

    @Bean
    public AsyncExecutor getExecutor() {
        ThreadFactory namedThreadFactory = new ThreadFactoryBuilder()
                .setNameFormat("AsmeCore-%d")
                .build();
        Executor executor = new ThreadPoolExecutor(0, 10000,
                60L, TimeUnit.SECONDS, new SynchronousQueue<>(), namedThreadFactory);

        return executor::execute;
    }

    @Override
    public Executor getAsyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(100);
        executor.setQueueCapacity(2000);
        executor.setThreadNamePrefix("SpringAsyncExecutor-");
        executor.initialize();
        return executor;
    }

    public interface AsyncExecutor extends Executor {
    }
}
