package com.asme.core.entity.jpa;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.vividsolutions.jts.geom.LineString;

/**
 * @author Vladyslav Mykhalets
 * @since 23.10.17
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "plan_route")
public class JpaPlanRoute {

    @Id
    @Column(name = "id")
    @GeneratedValue
    private Long id;

    @Column(name = "plan_waypoints", nullable = false, columnDefinition = "geometry(LineString,4326)")
    private LineString planWaypoints;

    @Column(name = "plan_geometry", nullable = false, columnDefinition = "text")
    private String planGeometry;

    @Column(name = "plan_distance", nullable = false)
    private BigDecimal planDistance;

    @Column(name = "plan_route", nullable = false, columnDefinition = "geometry(LineString,4326)")
    private LineString planRoute;

    @LastModifiedDate
    @Column(name = "last_modified", nullable = false)
    private Date lastModified;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LineString getPlanWaypoints() {
        return planWaypoints;
    }

    public void setPlanWaypoints(LineString planWaypoints) {
        this.planWaypoints = planWaypoints;
    }

    public String getPlanGeometry() {
        return planGeometry;
    }

    public void setPlanGeometry(String planGeometry) {
        this.planGeometry = planGeometry;
    }

    public BigDecimal getPlanDistance() {
        return planDistance;
    }

    public void setPlanDistance(BigDecimal planDistance) {
        this.planDistance = planDistance;
    }

    public LineString getPlanRoute() {
        return planRoute;
    }

    public void setPlanRoute(LineString planRoute) {
        this.planRoute = planRoute;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    @Override
    public String toString() {
        return "JpaPlanRoute{" +
                "id=" + id +
                ", planWaypoints=" + planWaypoints +
                ", planGeometry='" + planGeometry + '\'' +
                ", planDistance=" + planDistance +
                ", planRoute=" + planRoute +
                ", lastModified=" + lastModified +
                '}';
    }

    public static class PlanRouteBuilder {

        private Long id;

        private LineString planWaypoints;

        private String planGeometry;

        private BigDecimal planDistance;

        private LineString planRoute;

        public PlanRouteBuilder id(long id) {
            this.id = id;
            return this;
        }

        public PlanRouteBuilder planWaypoints(LineString waypoints) {
            this.planWaypoints = waypoints;
            return this;
        }

        public PlanRouteBuilder planRoute(LineString geoShapeLine) {
            this.planRoute = geoShapeLine;
            return this;
        }

        public PlanRouteBuilder planGeometry(String geometry) {
            this.planGeometry = geometry;
            return this;
        }

        public PlanRouteBuilder planDistance(BigDecimal planDistance) {
            this.planDistance = planDistance;
            return this;
        }

        public JpaPlanRoute build() {
            JpaPlanRoute planRouteEntity = new JpaPlanRoute();
            planRouteEntity.setId(this.id);
            planRouteEntity.setPlanWaypoints(this.planWaypoints);
            planRouteEntity.setPlanGeometry(this.planGeometry);
            planRouteEntity.setPlanRoute(this.planRoute);
            planRouteEntity.setPlanDistance(planDistance);
            return planRouteEntity;
        }
    }
}
