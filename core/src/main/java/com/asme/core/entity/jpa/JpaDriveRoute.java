package com.asme.core.entity.jpa;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.asme.common.enums.DriveRouteStatus;
import com.vividsolutions.jts.geom.LineString;

/**
 * @author Vladyslav Mykhalets
 * @since 22.11.17
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "drive_route")
public class JpaDriveRoute {

    @Id
    @Column(name = "id")
    @GeneratedValue
    private Long id;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private DriveRouteStatus routeStatus = DriveRouteStatus.OPEN;

    @Column(name = "waypoints", nullable = false, columnDefinition = "geometry(LineString,4326)")
    private LineString waypoints;

    @Column(name = "geometry", nullable = false, columnDefinition = "text")
    private String geometry;

    @Column(name = "distance", nullable = false)
    private BigDecimal distance;

    @Column(name = "route", nullable = false, columnDefinition = "geometry(LineString,4326)")
    private LineString route;

    @Column(name = "passengers_count", nullable = false)
    private int passengersCount;

    @Column(name = "max_passengers_count", nullable = false)
    private int maxPassengersCount;

    @LastModifiedDate
    @Column(name = "last_modified", nullable = false)
    private Date lastModified;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public DriveRouteStatus getRouteStatus() {
        return routeStatus;
    }

    public void setRouteStatus(DriveRouteStatus routeStatus) {
        this.routeStatus = routeStatus;
    }

    public LineString getWaypoints() {
        return waypoints;
    }

    public void setWaypoints(LineString waypoints) {
        this.waypoints = waypoints;
    }

    public String getGeometry() {
        return geometry;
    }

    public void setGeometry(String geometry) {
        this.geometry = geometry;
    }

    public BigDecimal getDistance() {
        return distance;
    }

    public void setDistance(BigDecimal distance) {
        this.distance = distance;
    }

    public LineString getRoute() {
        return route;
    }

    public void setRoute(LineString route) {
        this.route = route;
    }

    public int getPassengersCount() {
        return passengersCount;
    }

    public void setPassengersCount(int passengersCount) {
        this.passengersCount = passengersCount;
    }

    public int getMaxPassengersCount() {
        return maxPassengersCount;
    }

    public void setMaxPassengersCount(int maxPassengersCount) {
        this.maxPassengersCount = maxPassengersCount;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    @Override
    public String toString() {
        return "JpaDriveRoute{" +
                "id=" + id +
                ", userId='" + userId + '\'' +
                ", username='" + username + '\'' +
                ", routeStatus=" + routeStatus +
                ", waypoints=" + waypoints +
                ", geometry='" + geometry + '\'' +
                ", distance=" + distance +
                ", route=" + route +
                ", passengersCount=" + passengersCount +
                ", maxPassengersCount=" + maxPassengersCount +
                ", lastModified=" + lastModified +
                '}';
    }

    public static class Builder {

        private Long id;

        private Long userId;

        private String username;

        private DriveRouteStatus routeStatus = DriveRouteStatus.OPEN;

        private LineString waypoints;

        private String geometry;

        private BigDecimal distance;

        private LineString route;

        private int passengersCount;

        private int maxPassengersCount;

        public Builder id(long id) {
            this.id = id;
            return this;
        }

        public Builder userId(Long userId) {
            this.userId = userId;
            return this;
        }

        public Builder username(String username) {
            this.username = username;
            return this;
        }

        public Builder plannedRoute(JpaPlanRoute plannedRoute) {

            this.waypoints = plannedRoute.getPlanWaypoints();
            this.geometry = plannedRoute.getPlanGeometry();
            this.distance = plannedRoute.getPlanDistance();
            this.route = plannedRoute.getPlanRoute();

            return this;
        }

        public Builder routeStatus(DriveRouteStatus routeStatus) {
            this.routeStatus = routeStatus;
            return this;
        }

        public Builder waypoints(LineString waypoints) {
            this.waypoints = waypoints;
            return this;
        }

        public Builder geometry(String geometry) {
            this.geometry = geometry;
            return this;
        }

        public Builder distance(BigDecimal distance) {
            this.distance = distance;
            return this;
        }

        public Builder route(LineString geoShapeLine) {
            this.route = geoShapeLine;
            return this;
        }

        public Builder passengersCount(int passengersCount) {
            this.passengersCount = passengersCount;
            return this;
        }

        public Builder maxPassengersCount(int maxPassengersCount) {
            this.maxPassengersCount = maxPassengersCount;
            return this;
        }


        public JpaDriveRoute build() {
            JpaDriveRoute entity = new JpaDriveRoute();
            entity.setId(this.id);
            entity.setUserId(this.userId);
            entity.setUsername(this.username);
            entity.setRouteStatus(this.routeStatus);
            entity.setWaypoints(this.waypoints);
            entity.setGeometry(this.geometry);
            entity.setDistance(distance);
            entity.setRoute(this.route);
            entity.setPassengersCount(this.passengersCount);
            entity.setMaxPassengersCount(this.maxPassengersCount);
            return entity;
        }
    }
}
