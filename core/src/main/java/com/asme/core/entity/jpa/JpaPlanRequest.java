package com.asme.core.entity.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.asme.common.geo.GeoPoint;
import com.asme.core.utils.GeoUtils;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Point;

/**
 * @author Vladyslav Mykhalets
 * @since 07.01.18
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "plan_request")
public class JpaPlanRequest {

    @Id
    @Column(name = "id")
    @GeneratedValue
    private Long id;

    @Column(name = "start_location", nullable = false, columnDefinition = "geometry(Point,4326)")
    private Point startLocation;

    @Column(name = "start_location_name")
    private String startLocationName;

    @Column(name = "start_location_radius", nullable = false)
    private String startLocationRadius;

    @Column(name = "start_location_bounding_box", nullable = false, columnDefinition = "geometry(LineString,4326)")
    private LineString startLocationBoundingBox;

    @Column(name = "destination_location", nullable = false, columnDefinition = "geometry(Point,4326)")
    private Point destinationLocation;

    @Column(name = "destination_location_name")
    private String destinationLocationName;

    @Column(name = "destination_location_radius", nullable = false)
    private String destinationLocationRadius;

    @Column(name = "destination_location_bounding_box", nullable = false, columnDefinition = "geometry(LineString,4326)")
    private LineString destinationLocationBoundingBox;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Point getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(Point startLocation) {
        this.startLocation = startLocation;
    }

    public String getStartLocationName() {
        return startLocationName;
    }

    public void setStartLocationName(String startLocationName) {
        this.startLocationName = startLocationName;
    }

    public String getStartLocationRadius() {
        return startLocationRadius;
    }

    public void setStartLocationRadius(String startLocationRadius) {
        this.startLocationRadius = startLocationRadius;
    }

    public LineString getStartLocationBoundingBox() {
        return startLocationBoundingBox;
    }

    public void setStartLocationBoundingBox(LineString startLocationBoundingBox) {
        this.startLocationBoundingBox = startLocationBoundingBox;
    }

    public Point getDestinationLocation() {
        return destinationLocation;
    }

    public void setDestinationLocation(Point destinationLocation) {
        this.destinationLocation = destinationLocation;
    }

    public String getDestinationLocationName() {
        return destinationLocationName;
    }

    public void setDestinationLocationName(String destinationLocationName) {
        this.destinationLocationName = destinationLocationName;
    }

    public String getDestinationLocationRadius() {
        return destinationLocationRadius;
    }

    public void setDestinationLocationRadius(String destinationLocationRadius) {
        this.destinationLocationRadius = destinationLocationRadius;
    }

    public LineString getDestinationLocationBoundingBox() {
        return destinationLocationBoundingBox;
    }

    public void setDestinationLocationBoundingBox(LineString destinationLocationBoundingBox) {
        this.destinationLocationBoundingBox = destinationLocationBoundingBox;
    }

    public static class PlanRequestBuilder {

        private Long id;

        private Point startLocation;

        private String startLocationName;

        private String startLocationRadius;

        private LineString startLocationBoundingBox;

        private Point destinationLocation;

        private String destinationLocationName;

        private String destinationLocationRadius;

        private LineString destinationLocationBoundingBox;

        public PlanRequestBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public PlanRequestBuilder startLocation(GeoPoint startLocation, Number radiusInMeters) {
            this.startLocation = GeoUtils.toJTSPoint(startLocation);
            this.startLocationRadius = radiusInMeters.intValue() + "m";
            this.startLocationBoundingBox = GeoUtils.toJTSCircleAsRectangle(startLocation, radiusInMeters);
            return this;
        }

        public PlanRequestBuilder destination(GeoPoint destinationLocation, Number radiusInMeters) {
            this.destinationLocation = GeoUtils.toJTSPoint(destinationLocation);
            this.destinationLocationRadius = radiusInMeters.intValue() + "m";
            this.destinationLocationBoundingBox = GeoUtils.toJTSCircleAsRectangle(destinationLocation, radiusInMeters);
            return this;
        }

        public PlanRequestBuilder startLocationName(String startLocationName) {
            this.startLocationName = startLocationName;
            return this;
        }

        public PlanRequestBuilder destinationLocationName(String destinationLocationName) {
            this.destinationLocationName = destinationLocationName;
            return this;
        }

        public JpaPlanRequest build() {
            JpaPlanRequest planRequest = new JpaPlanRequest();
            planRequest.setId(this.id);

            planRequest.setStartLocation(this.startLocation);
            planRequest.setStartLocationName(this.startLocationName);
            planRequest.setStartLocationRadius(this.startLocationRadius);
            planRequest.setStartLocationBoundingBox(this.startLocationBoundingBox);

            planRequest.setDestinationLocation(this.destinationLocation);
            planRequest.setDestinationLocationName(this.destinationLocationName);
            planRequest.setDestinationLocationRadius(this.destinationLocationRadius);
            planRequest.setDestinationLocationBoundingBox(this.destinationLocationBoundingBox);

            return planRequest;
        }
    }
}
