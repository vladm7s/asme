package com.asme.core.entity.jpa;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.asme.common.enums.DriveRequestStatus;
import com.asme.common.geo.GeoPoint;
import com.asme.core.utils.GeoUtils;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Point;

/**
 * @author Vladyslav Mykhalets
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "drive_request")
public class JpaDriveRequest {

    @Id
    @Column(name = "id")
    @GeneratedValue
    private Long id;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "request_status", nullable = false)
    @Enumerated(EnumType.STRING)
    private DriveRequestStatus requestStatus = DriveRequestStatus.OPEN;

    @Column(name = "start_location", nullable = false, columnDefinition = "geometry(Point,4326)")
    private Point startLocation;

    @Column(name = "start_location_name")
    private String startLocationName;

    @Column(name = "start_location_radius", nullable = false)
    private String startLocationRadius;

    @Column(name = "start_location_bounding_box", nullable = false, columnDefinition = "geometry(LineString,4326)")
    private LineString startLocationBoundingBox;

    @Column(name = "destination_location", nullable = false, columnDefinition = "geometry(Point,4326)")
    private Point destinationLocation;

    @Column(name = "destination_location_name")
    private String destinationLocationName;

    @Column(name = "destination_location_radius", nullable = false)
    private String destinationLocationRadius;

    @Column(name = "destination_location_bounding_box", nullable = false, columnDefinition = "geometry(LineString,4326)")
    private LineString destinationLocationBoundingBox;

    @LastModifiedDate
    @Column(name = "last_modified", nullable = false)
    private Date lastModified;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LineString getStartLocationBoundingBox() {
        return startLocationBoundingBox;
    }

    public void setStartLocationBoundingBox(LineString startLocationBoundingBox) {
        this.startLocationBoundingBox = startLocationBoundingBox;
    }

    public LineString getDestinationLocationBoundingBox() {
        return destinationLocationBoundingBox;
    }

    public void setDestinationLocationBoundingBox(LineString destinationLocationBoundingBox) {
        this.destinationLocationBoundingBox = destinationLocationBoundingBox;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public DriveRequestStatus getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(DriveRequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getStartLocationName() {
        return startLocationName;
    }

    public void setStartLocationName(String startLocationName) {
        this.startLocationName = startLocationName;
    }

    public String getDestinationLocationName() {
        return destinationLocationName;
    }

    public void setDestinationLocationName(String destinationLocationName) {
        this.destinationLocationName = destinationLocationName;
    }

    public Point getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(Point startLocation) {
        this.startLocation = startLocation;
    }

    public String getStartLocationRadius() {
        return startLocationRadius;
    }

    public void setStartLocationRadius(String startLocationRadius) {
        this.startLocationRadius = startLocationRadius;
    }

    public Point getDestinationLocation() {
        return destinationLocation;
    }

    public void setDestinationLocation(Point destinationLocation) {
        this.destinationLocation = destinationLocation;
    }

    public String getDestinationLocationRadius() {
        return destinationLocationRadius;
    }

    public void setDestinationLocationRadius(String destinationLocationRadius) {
        this.destinationLocationRadius = destinationLocationRadius;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    @Override
    public String toString() {
        return "JpaDriveRequest{" +
                "id=" + id +
                ", userId='" + userId + '\'' +
                ", username='" + username + '\'' +
                ", requestStatus=" + requestStatus +
                ", startLocation=" + startLocation +
                ", startLocationName='" + startLocationName + '\'' +
                ", startLocationRadius='" + startLocationRadius + '\'' +
                ", startLocationBoundingBox=" + startLocationBoundingBox +
                ", destinationLocation=" + destinationLocation +
                ", destinationLocationName='" + destinationLocationName + '\'' +
                ", destinationLocationRadius='" + destinationLocationRadius + '\'' +
                ", destinationLocationBoundingBox=" + destinationLocationBoundingBox +
                ", lastModified=" + lastModified +
                '}';
    }

    public static class Builder {

        private Long id;

        private Long userId;

        private String username;

        private DriveRequestStatus requestStatus = DriveRequestStatus.OPEN;

        private Point startLocation;

        private String startLocationName;

        private String startLocationRadius;

        private LineString startLocationBoundingBox;

        private Point destinationLocation;

        private String destinationLocationName;

        private String destinationLocationRadius;

        private LineString destinationLocationBoundingBox;

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder userId(Long userId) {
            this.userId = userId;
            return this;
        }

        public Builder username(String username) {
            this.username = username;
            return this;
        }

        public Builder plannedRequest(JpaPlanRequest plannedRequest) {

            this.startLocation = plannedRequest.getStartLocation();
            this.startLocationRadius = plannedRequest.getStartLocationRadius();
            this.startLocationBoundingBox = plannedRequest.getStartLocationBoundingBox();
            this.startLocationName = plannedRequest.getStartLocationName();

            this.destinationLocation = plannedRequest.getDestinationLocation();
            this.destinationLocationRadius = plannedRequest.getDestinationLocationRadius();
            this.destinationLocationBoundingBox = plannedRequest.getDestinationLocationBoundingBox();
            this.destinationLocationName = plannedRequest.getDestinationLocationName();

            return this;
        }

        public Builder requestStatus(DriveRequestStatus requestStatus) {
            this.requestStatus = requestStatus;
            return this;
        }

        public Builder startLocation(GeoPoint startLocation, Number radiusInMeters) {
            this.startLocation = GeoUtils.toJTSPoint(startLocation);
            this.startLocationRadius = radiusInMeters.intValue() + "m";
            this.startLocationBoundingBox = GeoUtils.toJTSCircleAsRectangle(startLocation, radiusInMeters);
            return this;
        }

        public Builder destination(GeoPoint destinationLocation, Number radiusInMeters) {
            this.destinationLocation = GeoUtils.toJTSPoint(destinationLocation);
            this.destinationLocationRadius = radiusInMeters.intValue() + "m";
            this.destinationLocationBoundingBox = GeoUtils.toJTSCircleAsRectangle(destinationLocation, radiusInMeters);
            return this;
        }

        public Builder startLocationName(String startLocationName) {
            this.startLocationName = startLocationName;
            return this;
        }

        public Builder destinationLocationName(String destinationLocationName) {
            this.destinationLocationName = destinationLocationName;
            return this;
        }

        public JpaDriveRequest build() {
            JpaDriveRequest driveRequest = new JpaDriveRequest();
            driveRequest.setId(this.id);
            driveRequest.setUserId(this.userId);
            driveRequest.setUsername(this.username);
            driveRequest.setRequestStatus(this.requestStatus);

            driveRequest.setStartLocation(this.startLocation);
            driveRequest.setStartLocationName(this.startLocationName);
            driveRequest.setStartLocationRadius(this.startLocationRadius);
            driveRequest.setStartLocationBoundingBox(this.startLocationBoundingBox);

            driveRequest.setDestinationLocation(this.destinationLocation);
            driveRequest.setDestinationLocationName(this.destinationLocationName);
            driveRequest.setDestinationLocationRadius(this.destinationLocationRadius);
            driveRequest.setDestinationLocationBoundingBox(this.destinationLocationBoundingBox);

            return driveRequest;
        }
    }
}
