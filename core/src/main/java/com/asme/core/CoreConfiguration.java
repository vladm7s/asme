package com.asme.core;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.asme.core.configuration.AsmeConfiguration;
import com.asme.core.configuration.AsyncConfiguration;
import com.asme.core.configuration.OsrmConfiguration;
import com.asme.core.configuration.PostgisConfiguration;

/**
 * @author Vladyslav Mykhalets
 */
@Configuration
@ComponentScan({
        "com.asme.core.service.jpa",
        "com.asme.core.service.osrm",
        "com.asme.core.service.provider"
})
@Import({
        AsmeConfiguration.class,
        AsyncConfiguration.class,
        PostgisConfiguration.class,
        OsrmConfiguration.class
})
public class CoreConfiguration {
}
