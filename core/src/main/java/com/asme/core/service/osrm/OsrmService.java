package com.asme.core.service.osrm;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import com.asme.common.geo.GeoPoint;
import com.asme.common.osrm.OsrmRouteResponse;

/**
 * @author Vladyslav Mykhalets
 */
public interface OsrmService {

    OsrmRouteResponse route(List<GeoPoint> waypoints);

    CompletableFuture<OsrmRouteResponse> routeAsync(List<GeoPoint> waypoints);
}
