package com.asme.core.service.jpa;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import com.asme.common.geo.GeoPoint;
import com.asme.core.entity.jpa.JpaPlanRoute;

/**
 * @author Vladyslav Mykhalets
 */
public interface JpaPlanRouteService {

    JpaPlanRoute findOne(Long planRouteId);

    JpaPlanRoute route(List<GeoPoint> waypoints);

    CompletableFuture<JpaPlanRoute> routeAsync(List<GeoPoint> waypoints);

    void deleteAll();
}
