package com.asme.core.service.jpa;

import com.asme.core.entity.jpa.JpaPlanRequest;

/**
 * @author Vladyslav Mykhalets
 * @since 07.01.18
 */
public interface JpaPlanRequestService {

    JpaPlanRequest save(JpaPlanRequest planRequest);

    JpaPlanRequest findOne(Long planRequestId);

    void deleteAll();
}
