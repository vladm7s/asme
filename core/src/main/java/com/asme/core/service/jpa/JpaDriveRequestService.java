package com.asme.core.service.jpa;

import java.util.concurrent.CompletableFuture;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import com.asme.common.functional.Consumer;
import com.asme.common.geo.GeoShapeLine;
import com.asme.core.entity.jpa.JpaDriveRequest;
import com.vividsolutions.jts.geom.LineString;

/**
 * @author Vladyslav Mykhalets
 */
public interface JpaDriveRequestService {

    JpaDriveRequest save(JpaDriveRequest driveRequest);

    JpaDriveRequest createFromPlanRequest(Long planRequestId, Long userId, String username);

    CompletableFuture<JpaDriveRequest> createFromPlanRequestAsync(Long planRequestId, Long userId, String username);

    JpaDriveRequest findOne(Long driveRequestId);

    @Transactional(readOnly = true)
    void findByDriveRoute(Long driveRouteId, Consumer<JpaDriveRequest> consumer);

    @Transactional(readOnly = true)
    void findByRouteLine(Long userId, LineString route, Consumer<JpaDriveRequest> consumer);

    Page<JpaDriveRequest> findByDriveRoute(Long driveRouteId, Pageable pageable);

    Page<JpaDriveRequest> findByPlanRoute(Long userId, Long planRouteId, Pageable pageable);

    Page<JpaDriveRequest> findByRouteLine(Long userId, LineString route, Pageable pageable);

    Page<JpaDriveRequest> findByRouteLine(Long userId, GeoShapeLine route, Pageable pageable);

    void deleteAll();
}
