package com.asme.core.service.provider;

import java.security.cert.X509Certificate;

import javax.annotation.PostConstruct;
import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.apache.http.ssl.SSLContexts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.client.HttpComponentsAsyncClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.AsyncRestTemplate;
import org.springframework.web.client.RestTemplate;

import com.asme.common.exception.AsmeRuntimeException;
import com.asme.core.configuration.AsyncConfiguration.AsyncExecutor;
import com.asme.core.properties.AsmeProperties;

/**
 * @author Vladyslav Mykhalets
 */
public class RestTemplateProvider {
    private static final Logger log = LoggerFactory.getLogger(RestTemplateProvider.class);

    @Autowired
    private AsmeProperties asmeProperties;

    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    @Autowired
    private AsyncExecutor asyncExecutor;

    private RestTemplate restTemplate;

    private AsyncRestTemplate asyncRestTemplate;

    @PostConstruct
    public void postConstruct() {
        try {
            SSLContext sslContext;

            if (asmeProperties.isDisableTrustManager()) {
                TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
                sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
            } else {
                sslContext = SSLContexts.createDefault();
            }

            PoolingHttpClientConnectionManager poolingHttpClientConnectionManager = new PoolingHttpClientConnectionManager();
            poolingHttpClientConnectionManager.setMaxTotal(10000);
            poolingHttpClientConnectionManager.setDefaultMaxPerRoute(2000);

            CloseableHttpClient httpClient = HttpClientBuilder.create()
                    .setSSLContext(sslContext)
                    .setSSLHostnameVerifier(new NoopHostnameVerifier())
                    .setConnectionManager(poolingHttpClientConnectionManager)
                    .build();

            HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
            requestFactory.setHttpClient(httpClient);

            this.restTemplateBuilder = this.restTemplateBuilder
                    .detectRequestFactory(false)
                    .requestFactory(requestFactory);

            this.restTemplate = this.restTemplateBuilder.build();

            this.asyncRestTemplate = new AsyncRestTemplate(new HttpComponentsAsyncClientHttpRequestFactory(HttpAsyncClients.createDefault()), requestFactory);

        } catch (Exception e) {
            log.error("Could not configure rest template with disabled trust manager", e);
            throw new AsmeRuntimeException("Could not configure rest template with disabled trust manager", e);
        }
    }

    public RestTemplate get() {
        return this.restTemplate;
    }

    public AsyncRestTemplate getAsync() {
        return this.asyncRestTemplate;
    }
}
