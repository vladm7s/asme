package com.asme.core.service.osrm;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.client.AsyncRestTemplate;
import org.springframework.web.client.RestTemplate;

import com.asme.common.geo.GeoPoint;
import com.asme.common.osrm.OsrmRouteResponse;
import com.asme.core.properties.OsrmProperties;
import com.asme.core.service.provider.RestTemplateProvider;

import static com.google.common.collect.Maps.newHashMap;

/**
 * @author Vladyslav Mykhalets
 */
public class OsrmServiceImpl implements OsrmService {
    private static final Logger log = LoggerFactory.getLogger(OsrmServiceImpl.class);

    private static final String OK = "Ok";

    private static final String REQUEST_URL = "/route/v1/driving/{lonLatArray}" +
            "?overview={overview}&alternatives={alternatives}&steps={steps}";

    private static final String MESSAGE = "Route request to OSRM took: {} ms";

    @Autowired
    private OsrmProperties osrmProperties;

    @Autowired
    private RestTemplateProvider restTemplateProvider;

    @Override
    public OsrmRouteResponse route(List<GeoPoint> waypoints) {
        long start = System.currentTimeMillis();

        RestTemplate restTemplate = restTemplateProvider.get();

        String lonLatArray = waypoints.stream()
                .map(point -> point.getLon() + "," + point.getLat())
                .collect(Collectors.joining(";"));

        try {
            final String url = osrmProperties.getUrl() + REQUEST_URL;

            Map<String, Object> urlVariables = newHashMap();

            urlVariables.put("lonLatArray", lonLatArray);
            urlVariables.put("overview", "full");
            urlVariables.put("alternatives", true);
            urlVariables.put("steps", true);

            ResponseEntity<OsrmRouteResponse> response = restTemplate.getForEntity(url, OsrmRouteResponse.class, urlVariables);

            if (isSuccess(response)) {
                if (log.isDebugEnabled()) {
                    log.debug("Success response from OSRM service: {}", response.getBody());
                }

                long intermediate = System.currentTimeMillis();
                log.info(MESSAGE, (intermediate - start));

                return response.getBody();
            } else {
                log.error("Error response from OSRM service: {}. Body of the response: {}",
                        response.getStatusCode(), response.getBody());
            }
        }
        catch (Exception e) {
            log.error("Error requesting OSRM service.", e);
        }

        long stop = System.currentTimeMillis();
        log.info(MESSAGE, (stop - start));

        return null;
    }

    @Override
    public CompletableFuture<OsrmRouteResponse> routeAsync(List<GeoPoint> waypoints) {
        long start = System.currentTimeMillis();

        AsyncRestTemplate asyncRestTemplate = restTemplateProvider.getAsync();

        String lonLatArray = waypoints.stream()
                .map(point -> point.getLon() + "," + point.getLat())
                .collect(Collectors.joining(";"));


        final String url = osrmProperties.getUrl() + REQUEST_URL;

        Map<String, Object> urlVariables = newHashMap();

        urlVariables.put("lonLatArray", lonLatArray);
        urlVariables.put("overview", "full");
        urlVariables.put("alternatives", true);
        urlVariables.put("steps", true);

        CompletableFuture<OsrmRouteResponse> osrmRouteResponse = new CompletableFuture<>();

        ListenableFuture<ResponseEntity<OsrmRouteResponse>> listenableFutureResponse = asyncRestTemplate.getForEntity(url, OsrmRouteResponse.class, urlVariables);

        listenableFutureResponse.addCallback(result -> {

            if (isSuccess(result)) {
                if (log.isDebugEnabled()) {
                    log.debug("Success response from OSRM service: {}", result.getBody());
                }

                long intermediate = System.currentTimeMillis();
                log.info(MESSAGE, (intermediate - start));

                osrmRouteResponse.complete(result.getBody());
            } else {
                long intermediate = System.currentTimeMillis();
                log.info(MESSAGE, (intermediate - start));

                log.error("Error response from OSRM service: {}. Body of the response: {}",
                        result.getStatusCode(), result.getBody());

                osrmRouteResponse.completeExceptionally(new IllegalStateException("Http response received is not OK (200)!"));
            }
        }, ex -> {

            long intermediate = System.currentTimeMillis();
            log.info(MESSAGE, (intermediate - start));

            log.error("Error requesting OSRM service.", ex);
            osrmRouteResponse.completeExceptionally(ex);
        });

        return osrmRouteResponse;
    }

    private boolean isSuccess(ResponseEntity<OsrmRouteResponse> response) {
        return response.getStatusCode() == HttpStatus.OK && OK.equals(response.getBody().getCode());
    }
}
