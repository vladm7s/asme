package com.asme.core.service.jpa;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.asme.common.enums.Direction;
import com.asme.common.functional.Consumer;
import com.asme.common.geo.GeoShapeLine;
import com.asme.core.configuration.AsyncConfiguration.AsyncExecutor;
import com.asme.core.entity.jpa.JpaDriveRequest;
import com.asme.core.entity.jpa.JpaDriveRoute;
import com.asme.core.entity.jpa.JpaPlanRequest;
import com.asme.core.entity.jpa.JpaPlanRoute;
import com.asme.core.repository.jpa.JpaDriveRequestRepository;
import com.asme.core.repository.jpa.JpaPlanRequestRepository;
import com.asme.core.utils.GeoUtils;
import com.vividsolutions.jts.geom.LineString;

/**
 * @author Vladyslav Mykhalets
 * @since 22.11.17
 */
@Component
public class JpaDriveRequestServiceImpl implements JpaDriveRequestService {
    private static final Logger log = LoggerFactory.getLogger(JpaDriveRequestServiceImpl.class);

    @Autowired
    private JpaDriveRequestRepository driveRequestRepository;

    @Autowired
    private JpaPlanRequestRepository planRequestRepository;

    @Autowired
    private JpaDriveRouteService driveRouteService;

    @Autowired
    private JpaPlanRouteService planRouteService;

    @Autowired
    private AsyncExecutor asyncExecutor;

    public JpaDriveRequest findOne(Long driveRequestId) {
        return driveRequestRepository.findOne(driveRequestId);
    }

    public JpaDriveRequest save(JpaDriveRequest driveRequest) {
        return driveRequestRepository.save(driveRequest);
    }

    @Override
    public CompletableFuture<JpaDriveRequest> createFromPlanRequestAsync(Long planRequestId, Long userId, String username) {
        return CompletableFuture.supplyAsync(() -> createFromPlanRequest(planRequestId, userId, username), asyncExecutor);
    }

    public JpaDriveRequest createFromPlanRequest(Long planRequestId, Long userId, String username) {
        long start = System.currentTimeMillis();

        JpaPlanRequest planRequest = planRequestRepository.findOne(planRequestId);

        // TODO: Add check that current geo-position confirms with the planned request

        JpaDriveRequest driveRequest = new JpaDriveRequest.Builder()
                .userId(userId)
                .username(username)
                .plannedRequest(planRequest)
                .build();

        JpaDriveRequest savedDriveRequest = driveRequestRepository.save(driveRequest);

        long end = System.currentTimeMillis();
        log.info("Saving DRIVE REQUEST from planned request took: {} ms", (end - start));

        return savedDriveRequest;
    }

    @Override
    public void deleteAll() {
        driveRequestRepository.deleteAll();
    }

    @Transactional(readOnly = true)
    public void findByDriveRoute(Long driveRouteId, Consumer<JpaDriveRequest> consumer) {

        JpaDriveRoute driveRoute = driveRouteService.findOne(driveRouteId);
        Stream<JpaDriveRequest> biDirectionRequestsStream = driveRequestRepository.streamByRoute(driveRoute.getUserId(), driveRoute.getRoute());

        filterForwardDirection(driveRoute.getRoute(), biDirectionRequestsStream)
                .forEach(consumer::accept);
    }

    @Transactional(readOnly = true)
    public void findByRouteLine(Long userId, LineString route, Consumer<JpaDriveRequest> consumer) {

        Stream<JpaDriveRequest> biDirectionRequestsStream = driveRequestRepository.streamByRoute(userId, route);
        filterForwardDirection(route, biDirectionRequestsStream)
                .forEach(consumer::accept);
    }

    public Page<JpaDriveRequest> findByDriveRoute(Long driveRouteId, Pageable pageable) {
        long start = System.currentTimeMillis();

        JpaDriveRoute driveRoute = driveRouteService.findOne(driveRouteId);
        Page<JpaDriveRequest> biDirectionRequests = driveRequestRepository.findByRoute(driveRoute.getUserId(), driveRoute.getRoute(), pageable);

        long intermediate = System.currentTimeMillis();
        log.info("Searching DRIVE REQUESTS in PostGIS took: {} ms", (intermediate - start));

        Page<JpaDriveRequest> forwardRequests = filterForwardDirection(driveRoute.getRoute(), biDirectionRequests);

        long end = System.currentTimeMillis();
        log.info("Filtering DRIVE REQUESTS took: {} ms", (end - intermediate));

        return forwardRequests;
    }

    public Page<JpaDriveRequest> findByPlanRoute(Long userId, Long planRouteId, Pageable pageable) {
        long start = System.currentTimeMillis();

        JpaPlanRoute planRoute = planRouteService.findOne(planRouteId);
        Page<JpaDriveRequest> biDirectionRequests = driveRequestRepository.findByRoute(userId, planRoute.getPlanRoute(), pageable);

        long intermediate = System.currentTimeMillis();
        log.info("Searching DRIVE REQUESTS in PostGIS took: {} ms", (intermediate - start));

        Page<JpaDriveRequest> forwardRequests = filterForwardDirection(planRoute.getPlanRoute(), biDirectionRequests);

        long end = System.currentTimeMillis();
        log.info("Filtering DRIVE REQUESTS took: {} ms", (end - intermediate));

        return forwardRequests;
    }

    public Page<JpaDriveRequest> findByRouteLine(Long userId, GeoShapeLine geoShapeLine, Pageable pageable) {
        LineString route = GeoUtils.toJTSLineString(geoShapeLine);
        return findByRouteLine(userId, route, pageable);
    }

    public Page<JpaDriveRequest> findByRouteLine(Long userId, LineString route, Pageable pageable) {
        long start = System.currentTimeMillis();

        Page<JpaDriveRequest> biDirectionRequests = driveRequestRepository.findByRoute(userId, route, pageable);

        long intermediate = System.currentTimeMillis();
        log.info("Search DRIVE REQUESTS in PostGIS took: {} ms", (intermediate - start));

        Page<JpaDriveRequest> forwardRequests = filterForwardDirection(route, biDirectionRequests);

        long end = System.currentTimeMillis();
        log.info("Filtering DRIVE REQUESTS took: {} ms", (end - intermediate));

        return forwardRequests;
    }

    private Page<JpaDriveRequest> filterForwardDirection(LineString driveRoute, Page<JpaDriveRequest> biDirectionRequests) {
        List<JpaDriveRequest> requests = biDirectionRequests.getContent();
        List<JpaDriveRequest> filteredRequests = requests.stream()
                .filter(driveRequest -> Direction.FORWARD == GeoUtils.resolveDirection(driveRoute, driveRequest.getStartLocation(), driveRequest.getDestinationLocation(),
                        driveRequest.getStartLocationBoundingBox(), driveRequest.getDestinationLocationBoundingBox()))
                .collect(Collectors.toList());
        return new PageImpl<>(filteredRequests);
    }

    private Stream<JpaDriveRequest> filterForwardDirection(LineString driveRoute, Stream<JpaDriveRequest> biDirectionRequests) {
        return biDirectionRequests
                .filter(driveRequest -> Direction.FORWARD == GeoUtils.resolveDirection(driveRoute, driveRequest.getStartLocation(), driveRequest.getDestinationLocation(),
                        driveRequest.getStartLocationBoundingBox(), driveRequest.getDestinationLocationBoundingBox()));
    }
}
