package com.asme.core.service.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asme.core.entity.jpa.JpaPlanRequest;
import com.asme.core.repository.jpa.JpaPlanRequestRepository;

/**
 * @author Vladyslav Mykhalets
 * @since 07.01.18
 */
@Service
public class JpaPlanRequestServiceImpl implements JpaPlanRequestService {

    @Autowired
    private JpaPlanRequestRepository planRequestRepository;

    @Override
    public JpaPlanRequest save(JpaPlanRequest planRequest) {
        return planRequestRepository.save(planRequest);
    }

    @Override
    public JpaPlanRequest findOne(Long planRequestId) {
        return planRequestRepository.findOne(planRequestId);
    }

    @Override
    public void deleteAll() {
        planRequestRepository.deleteAll();
    }
}
