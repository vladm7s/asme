package com.asme.core.service.jpa;

import java.util.concurrent.CompletableFuture;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import com.asme.common.dto.route.DriveRouteSearchParameters;
import com.asme.common.functional.Consumer;
import com.asme.core.entity.jpa.JpaDriveRoute;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Point;

/**
 * @author Vladyslav Mykhalets
 */
public interface JpaDriveRouteService {

    JpaDriveRoute save(JpaDriveRoute driveRoute);

    JpaDriveRoute createFromPlanRoute(Long planRouteId, Long userId, String username);

    CompletableFuture<JpaDriveRoute> createFromPlanRouteAsync(Long planRouteId, Long userId, String username);

    JpaDriveRoute findOne(Long driveRouteId);

    Page<JpaDriveRoute> findByDriveRequest(Long driveRequestId, Pageable pageable);

    Page<JpaDriveRoute> findByPlanRequest(Long userId, Long planRequestId, Pageable pageable);

    @Transactional(readOnly = true)
    void findByStartAndDestination(Long userId,
                                   Point startLocation,
                                   Point destinationLocation,
                                   LineString startLocationBoundingBox,
                                   LineString destinationLocationBoundingBox,
                                   Consumer<JpaDriveRoute> consumer);

    Page<JpaDriveRoute> findByStartAndDestination(Long userId,
                                                  Point startLocation,
                                                  Point destinationLocation,
                                                  LineString startLocationBoundingBox,
                                                  LineString destinationLocationBoundingBox,
                                                  Pageable pageable);

    Page<JpaDriveRoute> findByStartAndDestination(Long userId, DriveRouteSearchParameters searchParameters, Pageable pageable);

    void deleteAll();
}
