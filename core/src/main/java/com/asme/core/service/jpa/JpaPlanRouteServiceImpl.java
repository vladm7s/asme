package com.asme.core.service.jpa;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asme.common.geo.GeoPoint;
import com.asme.common.geo.GeoShapeLine;
import com.asme.common.osrm.OsrmRoute;
import com.asme.common.osrm.OsrmRouteResponse;
import com.asme.common.utils.CommonGeoUtils;
import com.asme.core.configuration.AsyncConfiguration.AsyncExecutor;
import com.asme.core.entity.jpa.JpaPlanRoute;
import com.asme.core.repository.jpa.JpaPlanRouteRepository;
import com.asme.core.service.osrm.OsrmService;
import com.asme.core.utils.GeoUtils;

/**
 * @author Vladyslav Mykhalets
 */
@Service
public class JpaPlanRouteServiceImpl implements JpaPlanRouteService {
    private static final Logger log = LoggerFactory.getLogger(JpaPlanRouteServiceImpl.class);

    @Autowired
    private JpaPlanRouteRepository planRouteRepository;

    @Autowired
    private OsrmService osrmService;

    @Autowired
    private AsyncExecutor asyncExecutor;

    @Override
    public JpaPlanRoute findOne(Long planRouteId) {
        return planRouteRepository.findOne(planRouteId);
    }

    @Override
    public void deleteAll() {
        planRouteRepository.deleteAll();
    }

    @Override
    public JpaPlanRoute route(List<GeoPoint> waypoints) {

        OsrmRouteResponse routeResponse = osrmService.route(waypoints);

        if (routeResponse == null) {
            if (log.isErrorEnabled()) {
                log.error("Could not plan route for waypoints: {}", CommonGeoUtils.geoPointsToString(waypoints));
            }
            return null;
        }

        long start = System.currentTimeMillis();
        OsrmRoute route = routeResponse.getRoutes().get(0);

        String planGeometry = route.getGeometry();
        log.info("OSRM route geometry: {}", planGeometry);

        BigDecimal planDistance = route.getDistance();
        log.info("OSRM route distance: {} meters", planDistance);

        GeoShapeLine routeLine = GeoUtils.fromOsrmRoute(route);
        if (log.isDebugEnabled()) {
            log.debug("OSRM route response converted to GeoShapeLine: {}", routeLine);
        }

        JpaPlanRoute planRoute = new JpaPlanRoute.PlanRouteBuilder()
                .planWaypoints(GeoUtils.toJTSLineString(waypoints))
                .planDistance(planDistance)
                .planGeometry(planGeometry)
                .planRoute(GeoUtils.toJTSLineString(routeLine))
                .build();

        JpaPlanRoute savedPlanRoute = planRouteRepository.save(planRoute);

        long stop = System.currentTimeMillis();
        log.info("Saving PlanRoute in PostGIS took: {} ms", (stop - start));

        return savedPlanRoute;
    }

    @Override
    public CompletableFuture<JpaPlanRoute> routeAsync(List<GeoPoint> waypoints) {

        CompletableFuture<OsrmRouteResponse> routeResponseFuture = osrmService.routeAsync(waypoints);

        CompletableFuture<JpaPlanRoute> planRouteFuture = routeResponseFuture.thenApplyAsync(routeResponse -> {

            if (routeResponse == null) {
                log.error("Could not plan route for waypoints: {}", CommonGeoUtils.geoPointsToString(waypoints));
                return null;
            }

            OsrmRoute route = routeResponse.getRoutes().get(0);

            String planGeometry = route.getGeometry();
            log.info("OSRM route geometry: {}", planGeometry);

            BigDecimal planDistance = route.getDistance();
            log.info("OSRM route distance: {} meters", planDistance);

            GeoShapeLine routeLine = GeoUtils.fromOsrmRoute(route);
            if (log.isDebugEnabled()) {
                log.debug("OSRM route response converted to GeoShapeLine: {}", routeLine);
            }

            return new JpaPlanRoute.PlanRouteBuilder()
                    .planWaypoints(GeoUtils.toJTSLineString(waypoints))
                    .planDistance(planDistance)
                    .planGeometry(planGeometry)
                    .planRoute(GeoUtils.toJTSLineString(routeLine))
                    .build();
        }, asyncExecutor);

        return planRouteFuture.thenApply(jpaPlanRoute -> {

            long start = System.currentTimeMillis();
            JpaPlanRoute savedPlanRoute = planRouteRepository.save(jpaPlanRoute);

            long stop = System.currentTimeMillis();
            log.info("Saving PlanRoute in PostGIS took: {} ms", (stop - start));

            return savedPlanRoute;
        });
    }
}
