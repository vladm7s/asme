package com.asme.core.service.jpa;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.asme.common.dto.route.DriveRouteSearchParameters;
import com.asme.common.enums.Direction;
import com.asme.common.functional.Consumer;
import com.asme.common.geo.GeoPoint;
import com.asme.core.configuration.AsyncConfiguration.AsyncExecutor;
import com.asme.core.entity.jpa.JpaDriveRequest;
import com.asme.core.entity.jpa.JpaDriveRoute;
import com.asme.core.entity.jpa.JpaPlanRequest;
import com.asme.core.entity.jpa.JpaPlanRoute;
import com.asme.core.repository.jpa.JpaDriveRouteRepository;
import com.asme.core.utils.GeoUtils;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Point;

/**
 * @author Vladyslav Mykhalets
 */
@Service
public class JpaDriveRouteServiceImpl implements JpaDriveRouteService {
    private static final Logger log = LoggerFactory.getLogger(JpaDriveRouteServiceImpl.class);

    @Autowired
    private JpaDriveRouteRepository driveRouteRepository;

    @Autowired
    private JpaDriveRequestService driveRequestService;

    @Autowired
    private JpaPlanRequestService planRequestService;

    @Autowired
    private JpaPlanRouteService planRouteService;

    @Autowired
    private AsyncExecutor asyncExecutor;

    public JpaDriveRoute save(JpaDriveRoute driveRoute) {
        return driveRouteRepository.save(driveRoute);
    }

    public CompletableFuture<JpaDriveRoute> createFromPlanRouteAsync(Long planRouteId, Long userId, String username) {
        return CompletableFuture.supplyAsync(() -> createFromPlanRoute(planRouteId, userId, username), asyncExecutor);
    }

    public JpaDriveRoute createFromPlanRoute(Long planRouteId, Long userId, String username) {
        long start = System.currentTimeMillis();

        JpaPlanRoute planRoute = planRouteService.findOne(planRouteId);

        // TODO: Add check that current geo-position confirms with the planned route

        JpaDriveRoute driveRoute = new JpaDriveRoute.Builder()
                .userId(userId)
                .username(username)
                .plannedRoute(planRoute)
                .build();

        JpaDriveRoute savedDriveRoute = driveRouteRepository.save(driveRoute);

        long end = System.currentTimeMillis();
        log.info("Saving DRIVE ROUTE from planned route took: {} ms", (end - start));

        return savedDriveRoute;
    }

    public void deleteAll() {
        driveRouteRepository.deleteAll();
    }

    public JpaDriveRoute findOne(Long driveRouteId) {
        return driveRouteRepository.findOne(driveRouteId);
    }

    public Page<JpaDriveRoute> findByDriveRequest(Long driveRequestId, Pageable pageable) {
        long start = System.currentTimeMillis();

        JpaDriveRequest driveRequest = driveRequestService.findOne(driveRequestId);
        Page<JpaDriveRoute> biDirectionRoutes = driveRouteRepository.findByStartAndDestination(driveRequest.getUserId(),
                driveRequest.getStartLocationBoundingBox(), driveRequest.getDestinationLocationBoundingBox(), pageable);

        long intermediate = System.currentTimeMillis();
        log.info("Searching DRIVE ROUTES in PostGIS took: {} ms", (intermediate - start));

        Page<JpaDriveRoute> forwardRoutes = filterForwardDirection(
                driveRequest.getStartLocation(), driveRequest.getDestinationLocation(),
                driveRequest.getStartLocationBoundingBox(), driveRequest.getDestinationLocationBoundingBox(),
                biDirectionRoutes);

        long end = System.currentTimeMillis();
        log.info("Filtering DRIVE ROUTES took: {} ms", (end - intermediate));

        return forwardRoutes;
    }

    public Page<JpaDriveRoute> findByPlanRequest(Long userId, Long planRequestId, Pageable pageable) {
        long start = System.currentTimeMillis();

        JpaPlanRequest planRequest = planRequestService.findOne(planRequestId);
        Page<JpaDriveRoute> biDirectionRoutes = driveRouteRepository.findByStartAndDestination(userId,
                planRequest.getStartLocationBoundingBox(), planRequest.getDestinationLocationBoundingBox(), pageable);

        long intermediate = System.currentTimeMillis();
        log.info("Searching DRIVE ROUTES in PostGIS took: {} ms", (intermediate - start));

        Page<JpaDriveRoute> forwardRoutes = filterForwardDirection(
                planRequest.getStartLocation(), planRequest.getDestinationLocation(),
                planRequest.getStartLocationBoundingBox(), planRequest.getDestinationLocationBoundingBox(),
                biDirectionRoutes);

        long end = System.currentTimeMillis();
        log.info("Filtering DRIVE ROUTES took: {} ms", (end - intermediate));

        return forwardRoutes;
    }

    @Transactional(readOnly = true)
    public void findByStartAndDestination(Long userId,
                                          Point startLocation,
                                          Point destinationLocation,
                                          LineString startLocationBoundingBox,
                                          LineString destinationLocationBoundingBox,
                                          Consumer<JpaDriveRoute> consumer) {

        Stream<JpaDriveRoute> biDirectionRoutes = driveRouteRepository.findByStartAndDestination(
                userId, startLocationBoundingBox, destinationLocationBoundingBox);

        filterForwardDirection(
                startLocation, destinationLocation,
                startLocationBoundingBox, destinationLocationBoundingBox,
                biDirectionRoutes).forEach(consumer::accept);
    }

    public Page<JpaDriveRoute> findByStartAndDestination(Long userId, Point startLocation, Point destinationLocation,
                                                         LineString startLocationBoundingBox, LineString destinationLocationBoundingBox,
                                                         Pageable pageable) {
        long start = System.currentTimeMillis();

        Page<JpaDriveRoute> biDirectionRoutes = driveRouteRepository.findByStartAndDestination(
                userId, startLocationBoundingBox, destinationLocationBoundingBox, pageable);

        long intermediate = System.currentTimeMillis();
        log.info("Searching DRIVE ROUTES in PostGIS took: {} ms", (intermediate - start));

        Page<JpaDriveRoute> forwardRoutes = filterForwardDirection(
                startLocation, destinationLocation,
                startLocationBoundingBox, destinationLocationBoundingBox,
                biDirectionRoutes);

        long end = System.currentTimeMillis();
        log.info("Filtering DRIVE ROUTES took: {} ms", (end - intermediate));

        return forwardRoutes;
    }

    @Override
    public Page<JpaDriveRoute> findByStartAndDestination(Long userId, DriveRouteSearchParameters searchParameters, Pageable pageable) {
        long start = System.currentTimeMillis();

        LineString startLocationBoundingBox = GeoUtils.toJTSCircleAsRectangle(
                new GeoPoint(searchParameters.getStartLocation().getCoordinates()[0], searchParameters.getStartLocation().getCoordinates()[1]),
                new BigDecimal(searchParameters.getStartLocation().getRadiusInMeters()));

        LineString destinationLocationBoundingBox = GeoUtils.toJTSCircleAsRectangle(
                new GeoPoint(searchParameters.getDestination().getCoordinates()[0], searchParameters.getDestination().getCoordinates()[1]),
                new BigDecimal(searchParameters.getDestination().getRadiusInMeters()));

        Page<JpaDriveRoute> biDirectionRoutes = driveRouteRepository.findByStartAndDestination(userId, startLocationBoundingBox, destinationLocationBoundingBox, pageable);

        long intermediate = System.currentTimeMillis();
        log.info("Searching DRIVE ROUTES in PostGIS took: {} ms", (intermediate - start));

        Page<JpaDriveRoute> forwardRoutes = filterForwardDirection(
                GeoUtils.toJTSPoint(searchParameters.getStartLocation().getCoordinates()),
                GeoUtils.toJTSPoint(searchParameters.getDestination().getCoordinates()),
                startLocationBoundingBox,
                destinationLocationBoundingBox,
                biDirectionRoutes);

        long end = System.currentTimeMillis();
        log.info("Filtering DRIVE ROUTES took: {} ms", (end - intermediate));

        return forwardRoutes;
    }

    private Page<JpaDriveRoute> filterForwardDirection(Point startLocation, Point destination,
                                                       LineString startLocationBoundingBox, LineString destinationBoundingBox,
                                                       Page<JpaDriveRoute> biDirectionRoutes) {
        List<JpaDriveRoute> routes = biDirectionRoutes.getContent();
        List<JpaDriveRoute> filteredRoutes = routes.stream()
                .filter(driveRoute -> Direction.FORWARD == GeoUtils.resolveDirection(driveRoute.getRoute(),
                        startLocation, destination, startLocationBoundingBox, destinationBoundingBox))
                .collect(Collectors.toList());
        return new PageImpl<>(filteredRoutes);
    }

    private Stream<JpaDriveRoute> filterForwardDirection(Point startLocation, Point destination,
                                                         LineString startLocationBoundingBox, LineString destinationBoundingBox,
                                                         Stream<JpaDriveRoute> biDirectionRoutes) {
        return biDirectionRoutes
                .filter(driveRoute -> Direction.FORWARD == GeoUtils.resolveDirection(driveRoute.getRoute(),
                        startLocation, destination, startLocationBoundingBox, destinationBoundingBox));
    }
}
