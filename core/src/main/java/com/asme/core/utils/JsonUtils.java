package com.asme.core.utils;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Vladyslav Mykhalets
 * @since 20.10.17
 */
public class JsonUtils {

    private static final Logger log = LoggerFactory.getLogger(JsonUtils.class);

    public static String toFormattedJsonString(Object o) {
        ObjectMapper mapper = new ObjectMapper();

        try {
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(o);
        } catch (IOException e) {
            log.error("JsonUtils exception", e);

            return null;
        }
    }

    public static <T> T fromJsonString(String s, Class<T> clazz) {
        ObjectMapper mapper = new ObjectMapper();

        try {
            return mapper.readValue(s, clazz);
        } catch (IOException e) {
            log.error("JsonUtils exception", e);

            return null;
        }
    }

    public static <T> T fromJsonFile(String location, Class<T> clazz) {
        ResourceLoader resourceLoader = new DefaultResourceLoader();
        Resource resource = resourceLoader.getResource(location);

        try {
            return fromJsonString(IOUtils.toString(resource.getInputStream()), clazz);
        } catch (IOException e) {
            log.error("JsonUtils exception", e);

            return null;
        }
    }

    public static String toJsonStringWithClass(Object o) {
        ObjectMapper mapper = new ObjectMapper();

        //mapper.registerModule(new JodaModule());
        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);

        try {
            return mapper.writer().writeValueAsString(o);
        } catch (IOException e) {
            log.error("JsonUtils exception", e);

            return null;
        }
    }

    public static <T> T fromJsonStringWithClass(String s, Class<T> clazz) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);

        try {
            return mapper.readValue(s, clazz);
        } catch (IOException e) {
            log.error("JsonUtils exception", e);

            return null;
        }
    }

    public static <T> T fromJsonFileWithClass(String location, Class<T> clazz) {
        ResourceLoader resourceLoader = new DefaultResourceLoader();
        Resource resource = resourceLoader.getResource(location);

        try {
            return fromJsonStringWithClass(IOUtils.toString(resource.getInputStream()), clazz);
        } catch (IOException e) {
            log.error("JsonUtils exception", e);

            return null;
        }
    }
}
