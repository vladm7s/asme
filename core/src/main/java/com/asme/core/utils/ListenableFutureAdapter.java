package com.asme.core.utils;

import java.util.concurrent.CompletableFuture;

import org.springframework.util.concurrent.ListenableFuture;

/**
 * @author Vladyslav Mykhalets
 * @since 20.10.17
 */
public class ListenableFutureAdapter<T> {

    private final CompletableFuture<T> completableFuture;

    public ListenableFutureAdapter(ListenableFuture<T> listenableFuture) {

        this.completableFuture = new CompletableFuture<T>() {
            @Override
            public boolean cancel(boolean mayInterruptIfRunning) {
                boolean cancelled = listenableFuture.cancel(mayInterruptIfRunning);
                super.cancel(cancelled);
                return cancelled;
            }
        };

        listenableFuture.addCallback(completableFuture::complete, completableFuture::completeExceptionally);
    }

    private CompletableFuture<T> getCompletableFuture() {
        return completableFuture;
    }

    public static final <T> CompletableFuture<T> toCompletable(ListenableFuture<T> listenableFuture) {
        ListenableFutureAdapter<T> listenableFutureAdapter = new ListenableFutureAdapter<>(listenableFuture);
        return listenableFutureAdapter.getCompletableFuture();
    }
}
