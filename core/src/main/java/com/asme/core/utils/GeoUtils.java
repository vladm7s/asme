package com.asme.core.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.asme.common.enums.Direction;
import com.asme.common.geo.GeoPoint;
import com.asme.common.geo.GeoShapeCircle;
import com.asme.common.geo.GeoShapeLine;
import com.asme.common.osrm.OsrmRoute;
import com.asme.common.utils.CommonGeoUtils;
import com.spatial4j.core.context.jts.JtsSpatialContext;
import com.spatial4j.core.shape.Circle;
import com.spatial4j.core.shape.Rectangle;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateFilter;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.PrecisionModel;

/**
 * @author Vladyslav Mykhalets
 */
public class GeoUtils {

    public static final Integer DEFAULT_SRID = 4326;

    public static final GeometryFactory defaultGeometryFactory = new GeometryFactory(new PrecisionModel(), DEFAULT_SRID);

    private static final Logger log = LoggerFactory.getLogger(GeoUtils.class);

    private GeoUtils() {
    }

    public static Circle toSpatial4JCircle(GeoShapeCircle geoShapeCircle) {
        Coordinate center = new Coordinate(geoShapeCircle.getCoordinates()[0].doubleValue(), geoShapeCircle.getCoordinates()[1].doubleValue());
        return JtsSpatialContext.GEO.makeCircle(center.x, center.y,
                360.0D * geoShapeCircle.getRadiusInMeters() / CommonGeoUtils.getEarthCircumference());
    }

    public static GeoShapeLine fromJTSLineString(LineString lineString) {
        List<BigDecimal[]> coordinates = new ArrayList<>();
        lineString.apply((CoordinateFilter) coordinate -> coordinates.add(new BigDecimal[]{
                BigDecimal.valueOf(coordinate.getOrdinate(Coordinate.X)),
                BigDecimal.valueOf(coordinate.getOrdinate(Coordinate.Y))
        }));
        GeoShapeLine geoShapeLine = new GeoShapeLine();
        geoShapeLine.setCoordinates(coordinates);
        return geoShapeLine;
    }

    public static GeoPoint toGeoPoint(Point point) {
        return new GeoPoint(String.valueOf(point.getCoordinate().getOrdinate(Coordinate.X)), String.valueOf(point.getCoordinate().getOrdinate(Coordinate.Y)));
    }

    public static List<GeoPoint> toGeoPointsList(LineString lineString) {
        List<GeoPoint> geoPoints = new ArrayList<>();
        if (lineString != null) {
            lineString.apply((CoordinateFilter) coordinate -> geoPoints.add(
                    new GeoPoint(String.valueOf(coordinate.getOrdinate(Coordinate.X)), String.valueOf(coordinate.getOrdinate(Coordinate.Y))))
            );
        }
        return geoPoints;
    }

    public static Point toJTSPoint(GeoPoint center) {
        double lon = center.getLon().doubleValue();
        double lat = center.getLat().doubleValue();
        return defaultGeometryFactory.createPoint(new Coordinate(lon, lat));
    }

    public static Point toJTSPoint(BigDecimal[] center) {
        double lon = center[0].doubleValue();
        double lat = center[1].doubleValue();
        return defaultGeometryFactory.createPoint(new Coordinate(lon, lat));
    }

    public static LineString toJTSCircleAsRectangle(GeoPoint geoPoint, Number radiusInMeters) {
        Coordinate[] coordinates = new Coordinate[5];

        Coordinate center = new Coordinate(geoPoint.getLon().doubleValue(), geoPoint.getLat().doubleValue());
        Circle circle = JtsSpatialContext.GEO.makeCircle(center.x, center.y,
                360.0D * radiusInMeters.doubleValue() / CommonGeoUtils.getEarthCircumference());

        Rectangle rectangle = circle.getBoundingBox();

        coordinates[0] = new Coordinate(rectangle.getMinX(), rectangle.getMaxY());
        coordinates[1] = new Coordinate(rectangle.getMaxX(), rectangle.getMaxY());
        coordinates[2] = new Coordinate(rectangle.getMaxX(), rectangle.getMinY());
        coordinates[3] = new Coordinate(rectangle.getMinX(), rectangle.getMinY());
        coordinates[4] = new Coordinate(rectangle.getMinX(), rectangle.getMaxY());

        return defaultGeometryFactory.createLineString(coordinates);
    }

    public static LineString toJTSLineString(List<GeoPoint> geoPoints) {
        Coordinate[] coordinates = new Coordinate[geoPoints.size()];

        for (int i = 0; i < geoPoints.size(); i++) {
            GeoPoint geoPoint = geoPoints.get(i);

            double lon = geoPoint.getLon().doubleValue();
            double lat = geoPoint.getLat().doubleValue();

            coordinates[i] = new Coordinate(lon, lat);
        }
        return defaultGeometryFactory.createLineString(coordinates);
    }

    public static LineString toJTSLineString(GeoShapeLine geoShapeLine) {
        List<BigDecimal[]> geoPoints = geoShapeLine.getCoordinates();

        Coordinate[] coordinates = new Coordinate[geoPoints.size()];

        for (int i = 0; i < geoPoints.size(); i++) {
            BigDecimal[] geoPoint = geoPoints.get(i);

            double lon = geoPoint[0].doubleValue();
            double lat = geoPoint[1].doubleValue();

            coordinates[i] = new Coordinate(lon, lat);
        }
        return defaultGeometryFactory.createLineString(coordinates);
    }

    public static GeoShapeLine fromOsrmRoute(OsrmRoute osrmRoute) {
        // use precision 5 for OSRMv5
        return CommonGeoUtils.fromGeometry(osrmRoute.getGeometry(), 5);
    }

    // https://gis.stackexchange.com/questions/110249/coordinate-conversion-epsg3857-to-epsg4326-using-opengis-jts-too-slow
    public static Direction resolveDirection(GeoShapeLine driveRoute, GeoShapeCircle startLocation, GeoShapeCircle destination) {

        Circle circleStartLocation = toSpatial4JCircle(startLocation);
        Circle circleDestination = toSpatial4JCircle(destination);

        Geometry circleStartLocationGeometry = JtsSpatialContext.GEO.getGeometryFrom(circleStartLocation);
        Geometry circleDestinationGeometry = JtsSpatialContext.GEO.getGeometryFrom(circleDestination);

        Coordinate routeStepStart = null;

        for (BigDecimal[] coordinate : driveRoute.getCoordinates()) {
            if (routeStepStart == null) {
                routeStepStart = new Coordinate(coordinate[0].doubleValue(), coordinate[1].doubleValue());
            } else {
                Coordinate routeStepEnd = new Coordinate(coordinate[0].doubleValue(), coordinate[1].doubleValue());

                Geometry routeStepGeometry = defaultGeometryFactory.createLineString(new Coordinate[]{routeStepStart, routeStepEnd});

                boolean circleStartLocationIntersects = routeStepGeometry.intersects(circleStartLocationGeometry);
                boolean circleDestinationIntersects = routeStepGeometry.intersects(circleDestinationGeometry);

                if (circleStartLocationIntersects) {
                    if (!circleDestinationIntersects) {
                        return Direction.FORWARD;
                    } else {
                        log.info("Driver request start location and destination are within single route step. Need to calculate distances...");

                        double distanceToStart = routeStepStart.distance(new Coordinate(startLocation.getCoordinates()[0].doubleValue(), startLocation.getCoordinates()[1].doubleValue()));
                        double distanceToDestination = routeStepStart.distance(new Coordinate(destination.getCoordinates()[0].doubleValue(), destination.getCoordinates()[1].doubleValue()));

                        return distanceToStart < distanceToDestination ? Direction.FORWARD : Direction.REVERSE;
                    }
                } else if (circleDestinationIntersects) {
                    return Direction.REVERSE;
                }

                routeStepStart = routeStepEnd;
            }
        }

        throw new IllegalStateException(String.format("Could not resolve direction for: %n drive request start = %s %n drive request destination = %s %n drive route = %s",
                startLocation, destination, driveRoute));
    }

    public static Direction resolveDirection(LineString driveRoute,
                                             Point startLocation, Point destination,
                                             LineString startLocationBoundingBox, LineString destinationBoundingBox) {

        Coordinate routeStepStart = null;

        for (Coordinate coordinate : driveRoute.getCoordinates()) {
            if (routeStepStart == null) {
                routeStepStart = coordinate;
            } else {
                final Coordinate routeStepEnd = coordinate;

                Geometry routeStepGeometry = defaultGeometryFactory.createLineString(new Coordinate[]{routeStepStart, routeStepEnd});

                boolean circleStartLocationIntersects = routeStepGeometry.intersects(startLocationBoundingBox);
                boolean circleDestinationIntersects = routeStepGeometry.intersects(destinationBoundingBox);

                if (circleStartLocationIntersects) {
                    if (!circleDestinationIntersects) {
                        return Direction.FORWARD;
                    } else {
                        log.info("Driver request start location and destination are within single route step. Need to calculate distances...");

                        double distanceToStart = routeStepStart.distance(startLocation.getCoordinate());
                        double distanceToDestination = routeStepStart.distance(destination.getCoordinate());

                        return distanceToStart < distanceToDestination ? Direction.FORWARD : Direction.REVERSE;
                    }
                } else if (circleDestinationIntersects) {
                    return Direction.REVERSE;
                }

                routeStepStart = routeStepEnd;
            }
        }

        throw new IllegalStateException(String.format("Could not resolve direction for: %n drive request start = %s %n drive request destination = %s %n drive route = %s",
                startLocation, destination, driveRoute));
    }
}
