package com.asme.core.utils;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

/**
 * @author Vladyslav Mykhalets
 * @since 23.10.17
 */
public class PathUtils {

    private static final Logger log = LoggerFactory.getLogger(PathUtils.class);

    public static String readFileToString(String location) {
        ResourceLoader resourceLoader = new DefaultResourceLoader();
        Resource resource = resourceLoader.getResource(location);

        try {
            return IOUtils.toString(resource.getInputStream());
        } catch (IOException e) {
            log.error("JsonUtils exception", e);

            return null;
        }
    }
}
