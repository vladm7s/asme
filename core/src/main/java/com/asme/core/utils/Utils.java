package com.asme.core.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.BiConsumer;
import java.util.function.IntPredicate;
import java.util.function.ObjIntConsumer;
import java.util.function.Supplier;

/**
 * @author Vladyslav Mykhalets
 */
public class Utils {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        scan.nextInt();

        Map<Integer, Integer> heightByCyclesNumberCache = new TreeMap<>();
        heightByCyclesNumberCache.put(0, 1);

        while (scan.hasNext()) {
            int cyclesNumber = scan.nextInt();

            List<Integer> availableKeys = new ArrayList<>(heightByCyclesNumberCache.keySet());
            Integer maxKey = availableKeys.get(availableKeys.size() - 1);

            int height = heightByCyclesNumberCache.get(Math.min(maxKey, cyclesNumber));
            for (int i = Math.min(maxKey, cyclesNumber) + 1; i <= cyclesNumber; i++) {

                if (i % 2 == 1) {
                    height = height * 2;
                } else {
                    height = height + 1;
                }
            }

            heightByCyclesNumberCache.put(cyclesNumber, height);
            System.out.println(height);
        }
    }

    static int[] utopianTree(int[] cyclesNumberArray) {
        Map<Integer, Integer> heightByCyclesNumberCache = new TreeMap<>();
        heightByCyclesNumberCache.put(0, 1);

        int[] results = new int[cyclesNumberArray.length];

        int k = 0;
        for (int cyclesNumber : cyclesNumberArray) {

            List<Integer> availableKeys = new ArrayList<>(heightByCyclesNumberCache.keySet());
            Integer maxKey = availableKeys.get(availableKeys.size() - 1);

            int height = heightByCyclesNumberCache.get(Math.min(maxKey, cyclesNumber));
            for (int i = Math.min(maxKey, cyclesNumber) + 1; i <= cyclesNumber; i++) {

                if (i % 2 == 1) {
                    height = height * 2;
                } else {
                    height = height + 1;
                }

                heightByCyclesNumberCache.put(i, height);
            }

            results[k] = height;

            k++;
        }

        return results;
    }

    static int[] isPowerOf2(int[] nums) {

        Set<Integer> powersOfTwo = new LinkedHashSet<>();
        int current = 2;
        for (int i = 2; i < 30; i++) {
            current = current * 2;
            powersOfTwo.add(current);
        }

        final int count = nums.length;
        int[] result = new int[count];

        for (int i = 0; i < count; i++) {
            int next = nums[i];

            if (next == 1 || next == 2) {
                result[i] = 1;
            } else {
                if (next % 2 == 1) {
                    result[i] = 0;
                } else {
                    if (powersOfTwo.contains(next)) {
                        result[i] = 1;
                    }
                }
            }
        }

        return result;

    }

    static String compress(String str) {

        StringBuilder compressed = new StringBuilder();

        char[] array = str.toCharArray();

        int count = 1;
        char currentChar = array[0];

        for (int i = 1; i < array.length; i++) {

            if (array[i] == currentChar) {
                count++;
            } else {
                compressed.append(currentChar);

                if (count > 1) {
                    compressed.append(count);
                }

                currentChar = array[i];
                count = 1;
            }

            if (i == array.length - 1) {
                compressed.append(currentChar);

                if (count > 1) {
                    compressed.append(count);
                }
            }
        }

        return compressed.toString();
    }


    static String isPangram(String[] strings) {

        Set<Integer> fullCharsSet = new HashSet<>();
        for (int i = 10; i <=35; i++) {
            fullCharsSet.add(i);
        }

        StringBuilder result = new StringBuilder();
        for (String next : strings) {

            Set<Integer> charsSet = new HashSet<>();
            for (Character nextChar : next.toCharArray()) {
                int charIntValue = Character.getNumericValue(nextChar);

                if (charIntValue >= 10 && charIntValue <=35) {
                    charsSet.add(charIntValue);
                }
            }

            if (charsSet.containsAll(fullCharsSet)) {
                result.append("1");
            } else {
                result.append("0");
            }
        }

        return result.toString();
    }

    public int solution(int[] A) {

        Set<Integer> allLocations = new HashSet<>();

        for (int nextLocation : A) {
            allLocations.add(nextLocation);
        }


        int minCount = A.length;

        for (int i = 0; i < A.length; i++) {
            Set<Integer> copy = new HashSet<>(allLocations);
            int nextMinCount = 0;

            copy.remove(A[i]);
            nextMinCount++;

            for (int j = i + 1; j < A.length; j++) {

                copy.remove(A[j]);
                nextMinCount++;

                if (copy.isEmpty()) {
                    minCount = Math.min(minCount, nextMinCount);
                    break;
                }
            }
        }

        return minCount;
    }

    public static String solution(String phone) {

        String phone2 = phone.chars().filter(new IntPredicate() {
            @Override
            public boolean test(int value) {
                return Character.isDigit(value);
            }
        }).<StringBuilder>collect(new Supplier<StringBuilder>() {
            @Override
            public StringBuilder get() {
                return new StringBuilder();
            }
        }, new ObjIntConsumer<StringBuilder>() {
            @Override
            public void accept(StringBuilder builder, int value) {
                builder.append(Character.toChars(value));
            }
        }, new BiConsumer<StringBuilder, StringBuilder>() {
            @Override
            public void accept(StringBuilder r, StringBuilder r2) {
                r.append(r2);
            }
        }).toString();

        int digitsCount = 0;
        StringBuilder onlyDigitsBuilder = new StringBuilder();
        for (Character nextChar : phone.toCharArray()) {
            if (Character.isDigit(nextChar)) {
                digitsCount++;
                onlyDigitsBuilder.append(nextChar);
            }
        }

        String onlyDigits = onlyDigitsBuilder.toString();

        int count = 0;
        StringBuilder newPhoneBuilder  = new StringBuilder();
        for (int i = 0; i < onlyDigits.length(); i++) {
            Character nextChar = onlyDigits.toCharArray()[i];

            if (Character.isDigit(nextChar)) {
                count++;
                if (count > 3 || (i == onlyDigits.length() - 2 && digitsCount % 3 == 1)) {
                    newPhoneBuilder.append("-");
                    count = 1;
                }
                newPhoneBuilder.append(nextChar);
            }
        }
        return newPhoneBuilder.toString();
    }
}
