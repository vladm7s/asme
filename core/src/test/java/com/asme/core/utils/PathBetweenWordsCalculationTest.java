package com.asme.core.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static org.junit.Assert.assertEquals;

/**
 * @author Vladyslav Mykhalets
 * @since 28.03.18
 */
public class PathBetweenWordsCalculationTest {

    @Test
    public void calculatePathTest() {

        // start: cat
        // end: dog
        // cat cot cog dog
        List<String> path = execute(new String[] {
                "cat", "dog", "cat", "bat", "bet", "bed", "cat", "cog", "dog", "cot", "dot"
        });

        assertEquals(path, Arrays.asList("cat", "cot", "cog", "dog"));

        // start: cat
        // end: dog
        // cat cot cog dog
        path = execute(new String[] {
                "cat", "dog", "cat", "dog", "pog", "bog", "cot", "all", "yew", "nit", "cog", "raw", "sew", "mow", "saw", "was", "pam", "sam", "lit", "bit", "wit", "vim", "war", "rat", "mat", "log", "cow", "paw"
        });

        assertEquals(path, Arrays.asList("cat", "cot", "cog", "dog"));

        // start: cat
        // end: dog
        // cat cot cog dog
        path = execute(new String[] {
                "cat", "dog", "cat", "dog", "cap", "cat", "cup", "cod", "fog", "cut", "hut", "hot", "hit", "cot", "cog", "dog"
        });

        assertEquals(path, Arrays.asList("cat", "cot", "cog", "dog"));
    }

    private static List<String> execute(String[] args) {

        String startWord = args[0];
        String endWord = args[1];

        String[] dict = new String[args.length - 2];
        for (int i = 2; i < args.length; i++) {
            dict[i - 2] = args[i];
        }

        Map<String, List<String>> graph = new HashMap<>();

        for (int i = 0; i < dict.length; i++) {
            graph.put(dict[i], new ArrayList<>());

            for (int j = 0; j < dict.length; j++) {

                if (i == j) {
                    continue;
                }

                if (isOneLetterDifference(dict[i], dict[j])) {
                    graph.get(dict[i]).add(dict[j]);
                }
            }
        }

        List<String> path = calculatePath(graph, startWord, endWord, new HashSet<>());

        System.out.println(path);

        return path;
    }


    private static List<String> calculatePath(Map<String, List<String>> graph, String startWord, String endWord, Set<String> usedWords) {

        List<String> nextWords = graph.get(startWord);
        List<String> path = newArrayList();

        for (int i = 0; i < nextWords.size(); i++) {
            String next = nextWords.get(i);

            if (next.equals(endWord)) {
                path.add(startWord);
                path.add(next);
                return path;
            }
        }

        List<String> bestSubPath = null;

        for (int i = 0; i < nextWords.size(); i++) {
            String next = nextWords.get(i);

            if (usedWords.contains(next)) {
                continue;
            }

            Set<String> newUsedWords = newHashSet(usedWords);
            newUsedWords.add(next);

            List<String> subPath = calculatePath(graph, next, endWord, newUsedWords);
            if (!subPath.isEmpty()) {

                if (bestSubPath == null) {
                    bestSubPath = subPath;
                } else {
                    if (subPath.size() < bestSubPath.size()) {
                        bestSubPath = subPath;
                    }
                }
            }
        }

        if (bestSubPath != null) {
            path.add(startWord);
            path.addAll(bestSubPath);
        }

        return path;
    }

    private static boolean isOneLetterDifference(String word, String anotherWord) {
        int defCount = 0;
        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) != anotherWord.charAt(i)) {
                defCount++;
            }
        }
        return defCount == 1;
    }
}
