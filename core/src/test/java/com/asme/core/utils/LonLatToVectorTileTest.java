package com.asme.core.utils;

import org.junit.Test;

/**
 * @author Vladyslav Mykhalets
 *
 * For more details see: https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
 */
public class LonLatToVectorTileTest {

    @Test
    public void test() {

        // Kyiv, Svyatoshyno
        int zoom = 12;
        double lat = 50.448723d;
        double lon = 30.387956d;

        String tileNumber = getTileNumber(lat, lon, zoom);
        BoundingBox boundingBox = tileToBoundingBox(tileNumber);

        System.out.println("Bounding box: " + boundingBox);
        System.out.println("http://tile.openstreetmap.org/" + tileNumber + ".png");
    }

    private static String getTileNumber(final double lat, final double lon, final int zoom) {
        int xTile = (int) Math.floor((lon + 180) / 360 * (1 << zoom));
        int yTile = (int) Math.floor((1 - Math.log(Math.tan(Math.toRadians(lat)) + 1 / Math.cos(Math.toRadians(lat))) / Math.PI) / 2 * (1 << zoom));
        if (xTile < 0)
            xTile = 0;
        if (xTile >= (1 << zoom))
            xTile = ((1 << zoom) - 1);
        if (yTile < 0)
            yTile = 0;
        if (yTile >= (1 << zoom))
            yTile = ((1 << zoom) - 1);
        return (zoom + "/" + xTile + "/" + yTile);
    }

    static class BoundingBox {
        double north;
        double south;
        double east;
        double west;

        @Override
        public String toString() {
            return "BoundingBox{" +
                    "north=" + north +
                    ", south=" + south +
                    ", east=" + east +
                    ", west=" + west +
                    '}';
        }
    }

    private static BoundingBox tileToBoundingBox(final String tileNumber) {
        String[] parts = tileNumber.split("/");
        return tileToBoundingBox(Integer.parseInt(parts[1]), Integer.parseInt(parts[2]), Integer.parseInt(parts[0]));
    }

    private static BoundingBox tileToBoundingBox(final int x, final int y, final int zoom) {
        BoundingBox bb = new BoundingBox();
        bb.north = tile2lat(y, zoom);
        bb.south = tile2lat(y + 1, zoom);
        bb.west = tile2lon(x, zoom);
        bb.east = tile2lon(x + 1, zoom);
        return bb;
    }

    private static double tile2lon(int x, int z) {
        return x / Math.pow(2.0, z) * 360.0 - 180;
    }

    private static double tile2lat(int y, int z) {
        double n = Math.PI - (2.0 * Math.PI * y) / Math.pow(2.0, z);
        return Math.toDegrees(Math.atan(Math.sinh(n)));
    }
}
