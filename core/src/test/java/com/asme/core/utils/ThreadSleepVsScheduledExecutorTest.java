package com.asme.core.utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.junit.Test;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

/**
 * @author Vladyslav Mykhalets
 * @since 23.11.17
 */
public class ThreadSleepVsScheduledExecutorTest {

    @Test
    public void testPowerOfTwo() {

    }

    public static void main(String[] args) {

        // Thread.sleep(1000) in each thread
//        ThreadFactory namedThreadFactory = new ThreadFactoryBuilder()
//                .setNameFormat("WorkThread-%d")
//                .build();
//        ExecutorService executorService = new ThreadPoolExecutor(0, 32, 60L, TimeUnit.SECONDS, new SynchronousQueue<>(), namedThreadFactory);
//
//        for (int i = 0; i < 32; i++) {
//            executorService.submit(new WorkThreadWithInfiniteLoop());
//        }

        // Scheduled executor with delay 1000 ms
        ScheduledExecutorService scheduledExecutorService = new ScheduledThreadPoolExecutor(32);

//        for (int i = 0; i < 32; i++) {
//            scheduledExecutorService.scheduleWithFixedDelay(new WorkThreadSingleLoop(), 0, 1000, TimeUnit.MILLISECONDS);
//        }

        //new MeasureThread().run();
    }

    public static long calculateSmth() {
        Thread t = Thread.currentThread();
        String threadName = t.getName();

        long sum = 0;
        for (int i = 0; i < 1000; i++) {
            sum = sum + i;
        }
        System.out.println(threadName + "::Calculation done. Result = " + sum);
        return sum;
    }

    /** Cleaner invocation thread. */
    public static class WorkThreadWithInfiniteLoop implements Runnable {

        @Override
        public void run() {
            while (true) {
                try {
                    try {
                        Thread.sleep(1000);
                        calculateSmth();
                    } catch (InterruptedException ignore) {
                    }
                } catch (Exception e) {
                    System.out.println("Exception in WorkThread");
                }
            }
        }
    }

    /** Cleaner invocation thread. */
    public static class WorkThreadSingleLoop implements Runnable {

        @Override
        public void run() {
            try {
                calculateSmth();
            } catch (Exception e) {
                System.out.println("Exception in WorkThread");
            }
        }
    }

    /** Cleaner invocation thread. */
    public static class MeasureThread implements Runnable {

        @Override
        public void run() {
            long start = System.nanoTime();
            while (true) {
                try {
                    System.out.println("===============================================================================");
                } catch (Exception e) {
                    System.out.println("Exception in WorkThread");
                }
            }
        }
    }
}
