package com.asme.core.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author Vladyslav Mykhalets
 */
public class UtilsTest {

    public static class Node {
        public int level;

        public Node getChild() {
            Node child = new Node();
            child.level = level + 1;
            return child;
        }

    }

    public static void testRecursion1(Node node) {
        for (int i = 0; i < 10; i++) {
            if (node.level < 9) {
                Node child = node.getChild();
                testRecursion1(child);
            }
        }
    }

    // same code as above, but calls testRecursion1
    public static void testRecursion2(Node node) {
        for (int i = 0; i < 10; i++) {
            if (node.level < 9) {
                Node child = node.getChild();
                testRecursion1(child);
            }
        }
    }

//    public static void main(String[] args) {
//        for (int i = 0; i < 10; i++) {
//            long t = System.currentTimeMillis();
//            Node node = new Node();
//            testRecursion1(node);
//            t = System.currentTimeMillis() - t;
//            System.out.println("testRecursion1: " + t);
//            System.gc();
//        }
//        for (int i = 0; i < 10; i++) {
//            long t = System.currentTimeMillis();
//            Node node = new Node();
//            testRecursion2(node);
//            t = System.currentTimeMillis() - t;
//            System.out.println("testRecursion2: " + t);
//            System.gc();
//        }
//    }

    @Test
    public void testDouble() {

        int r1 = 5;
        int r2 = 4;

        double r = (r1 + r2) / 2;

        assertThat(r, is(4.0));

        r = (r1 + r2) / 2.0;

        assertThat(r, is(4.5));
    }

    @Test
    public void testDecimalFormat() throws Exception {
        Locale locale  = new Locale("en", "UK");
        String pattern = "###,###.00";

        DecimalFormat decimalFormat = (DecimalFormat)
                NumberFormat.getNumberInstance(locale);
        decimalFormat.applyPattern(pattern);

        DecimalFormat dec = new DecimalFormat();
        dec.applyPattern(pattern);
        //dec.setGroupingUsed(false);
        String format = dec.format(new BigDecimal(123456723489.262d));
        System.out.println(format);

        dec.setParseBigDecimal(true);
        Number number = dec.parse(format);

        format = decimalFormat.format(123456789.123);
        System.out.println(number);
    }

    @Test
    public void testCountWords() {
        Pattern wordPattern = Pattern.compile("\\w+");

        String test = "RegExr v3 was created by gskinner.com, and is proudly hosted by Media Temple.";

        Matcher matcher = wordPattern.matcher(test);
        int count = 0;

        while (matcher.find()) {
            count++;
        }

        assertThat(count, is(14));
    }
    @Test
    public void testUtopianTree() {
        int[] result = Utils.utopianTree(new int[] {0, 1, 4});

        assertThat(result, is(new int[]{
                1, 2, 7
        }));

        result = Utils.utopianTree(new int[] {4, 3});

        assertThat(result, is(new int[]{
                7, 6
        }));
    }

    @Test
    public void testPowerOfTwo() {

        int[] result = Utils.isPowerOf2(new int[]{
                2,
                4,
                1024,
                1023,
                9,
                2048,
                34534566
        });

        assertThat(result, is(new int[]{
                1, 1, 1, 0, 0, 1, 0
        }));

        int[] result2 = Utils.isPowerOf2(new int[]{
                2,
                3,
                4
        });

        assertThat(result2, is(new int[]{
                1, 0, 1
        }));
    }

    @Test
    public void testCompressed() {

        String result = Utils.compress("aaaaaabbbbjhBBBBBBddddk");

        String result2 = Utils.compress("aaaaaabbbbjhbbbbBBddddkkk");

        assertThat(result, is("a6b4jhB6d4k"));

        assertThat(result2, is("a6b4jhb4B2d4k3"));
    }

    @Test
    public void testIsPangram() {

        String result = Utils.isPangram(new String[]{
                "abcdef",
                "abcdefg hijklmn opqrstuv wxyz",
                "abcdeg hijklmn opqrstuv wxyz",
                "abcdefg hijklmn opqrstuv wxyz sdfhjksdhfkjsdhf asdweweqw eqwefsvsdfdsf"
        });

        assertThat(result, is("0101"));
    }


    @Test
    public void test() {

        String result = Utils.solution("123 3443 04-466");

        assertThat(result, is("123-344-304-466"));

        String result2 = Utils.solution("12 -- 3 3443 04-466 45");

        assertThat(result2, is("123-344-304-466-45"));

        String result3 = Utils.solution("0 - 22 1985--324");
        assertThat(result3, is("022-198-53-24"));

        String result4 = Utils.solution("0 - 2");
        assertThat(result4, is("02"));

    }
}
