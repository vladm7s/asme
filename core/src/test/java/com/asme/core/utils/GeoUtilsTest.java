package com.asme.core.utils;

import java.math.BigDecimal;

import org.junit.Test;

import com.asme.common.geo.GeoPoint;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.LineString;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author Vladyslav Mykhalets
 * @since 19.11.17
 */
public class GeoUtilsTest {

    @Test
    public void toJTSCircleAsRectangle() {

        LineString rectangle = GeoUtils.toJTSCircleAsRectangle(new GeoPoint("30.387956", "50.448723"),
                new BigDecimal("50.00"));

        assertThat(rectangle.getCoordinateN(0), is(new Coordinate(
                new BigDecimal("30.38725062965122").doubleValue(),
                new BigDecimal("50.44917215764206").doubleValue()
        )));

        assertThat(rectangle.getCoordinateN(1), is(new Coordinate(
                new BigDecimal("30.38866137034878").doubleValue(),
                new BigDecimal("50.44917215764206").doubleValue()
        )));
    }
}
