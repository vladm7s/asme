#!/usr/bin/env bash

sudo apt-get update
sudo apt-get install -y python default-jre-headless python-tk python-pip python-dev \
  libxml2-dev libxslt-dev zlib1g-dev htop
sudo pip install bzt
sudo pip install --upgrade bzt