package com.asme.messaging.configuration;

import java.util.UUID;

import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.listener.MessageListenerContainer;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.asme.messaging.configuration.MessagingConfiguration.RevokedTokenMessagingConfiguration;
import com.asme.messaging.configuration.MessagingConfiguration.UserMessagingConfiguration;
import com.asme.messaging.service.messaging.token.TokenQueueMessageListener;
import com.asme.messaging.service.messaging.user.UserQueueMessageListener;

/**
 * @author Vladyslav Mykhalets
 * @since 12.02.18
 */
@Configuration
@Import({
        RabbitmqConfiguration.class,
        UserMessagingConfiguration.class,
        RevokedTokenMessagingConfiguration.class
})
public class MessagingConfiguration {

    @Configuration
    @ConditionalOnProperty("asme.messaging.user.enabled")
    @ComponentScan({
            "com.asme.messaging.service.messaging.user"
    })
    public static class UserMessagingConfiguration {

        @Bean
        public MessageListenerContainer userMessageListenerContainer(UserQueueMessageListener userQueueMessageListener,
                                                                     CachingConnectionFactory cachingConnectionFactory) {

            //TODO: switch to DirectMessageListenerContainer with spring boot 2.0
            SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();

            container.setConnectionFactory(cachingConnectionFactory);
            container.setQueues(userMessageQueue());
            container.setMessageListener(userQueueMessageListener);
            container.setAcknowledgeMode(AcknowledgeMode.MANUAL);

            return container;
        }

        @Bean
        @Qualifier("userMessageQueue")
        public Queue userMessageQueue() {
            return new Queue("user-message-queue-" + UUID.randomUUID(), false);
        }

        @Bean
        public DirectExchange exchange() {
            return new DirectExchange("user-message-exchange");
        }

    }

    @Configuration
    @ConditionalOnProperty("asme.messaging.token.enabled")
    @ComponentScan({
            "com.asme.messaging.service.messaging.token"
    })
    public static class RevokedTokenMessagingConfiguration {

        @Bean
        public MessageListenerContainer tokenMessageListenerContainer(TokenQueueMessageListener tokenQueueMessageListener,
                                                                      CachingConnectionFactory cachingConnectionFactory) {

            //TODO: switch to DirectMessageListenerContainer with spring boot 2.0
            SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();

            container.setConnectionFactory(cachingConnectionFactory);
            container.setQueues(revokedTokenQueue());
            container.setMessageListener(tokenQueueMessageListener);
            container.setAcknowledgeMode(AcknowledgeMode.MANUAL);

            return container;
        }

        @Bean
        public FanoutExchange fanout() {
            return new FanoutExchange("revoked-token-exchange");
        }

        @Bean
        @Qualifier("revokedTokenQueue")
        public Queue revokedTokenQueue() {
            return new Queue("revoked-token-queue-" + UUID.randomUUID(), false);
        }

        @Bean
        public Binding binding() {
            return BindingBuilder.bind(revokedTokenQueue()).to(fanout());
        }
    }
}
