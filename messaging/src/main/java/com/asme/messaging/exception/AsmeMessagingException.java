package com.asme.messaging.exception;

/**
 * @author Vladyslav Mykhalets
 * @since 23.03.18
 */
public class AsmeMessagingException extends Exception {

    public AsmeMessagingException(String message) {
        super(message);
    }

    public AsmeMessagingException(String message, Throwable cause) {
        super(message, cause);
    }
}
