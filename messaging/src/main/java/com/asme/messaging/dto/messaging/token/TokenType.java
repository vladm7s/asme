package com.asme.messaging.dto.messaging.token;

/**
 * @author Vladyslav Mykhalets
 * @since 17.02.18
 */
public enum TokenType {

    ACCESS_TOKEN,
    REFRESH_TOKEN
}
