package com.asme.messaging.dto.messaging.token;

/**
 * @author Vladyslav Mykhalets
 * @since 16.02.18
 */
public class TokenRevokeMessage {

    private TokenType tokenType;

    private String tokenId;

    public TokenType getTokenType() {
        return tokenType;
    }

    public void setTokenType(TokenType tokenType) {
        this.tokenType = tokenType;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }
}
