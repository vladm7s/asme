package com.asme.messaging.service.messaging.user;

import com.asme.messaging.exception.AsmeMessagingException;

/**
 * @author Vladyslav Mykhalets
 * @since 21.01.18
 */
public interface UserMessageListener {

    void onMessage(String message) throws AsmeMessagingException;
}
