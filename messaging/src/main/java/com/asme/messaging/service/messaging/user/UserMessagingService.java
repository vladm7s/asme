package com.asme.messaging.service.messaging.user;

/**
 * @author Vladyslav Mykhalets
 * @since 21.01.18
 */
public interface UserMessagingService {

    boolean subscribeUser(Long userId, UserMessageListener messageListener);

    boolean unsubscribeUser(Long userId);

    void sendMessageToUser(Long userId, String message);
}
