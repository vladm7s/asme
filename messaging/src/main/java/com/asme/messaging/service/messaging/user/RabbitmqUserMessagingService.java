package com.asme.messaging.service.messaging.user;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * @author Vladyslav Mykhalets
 * @since 21.01.18
 */
@Service
public class RabbitmqUserMessagingService implements UserMessagingService {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private RabbitAdmin rabbitAdmin;

    @Autowired
    private UserQueueMessageListener userQueueMessageListener;

    @Autowired
    private DirectExchange userExchange;

    @Autowired
    @Qualifier("userMessageQueue")
    private Queue userQueue;

    private Map<Long, Binding> bindingByUser = new ConcurrentHashMap<>();

    public boolean subscribeUser(Long userId, UserMessageListener messageListener) {

        try {
            userQueueMessageListener.registerUserListener(userId, messageListener);

            Binding binding = BindingBuilder.bind(userQueue).to(userExchange).with(userId.toString());
            bindingByUser.put(userId, binding);

            rabbitAdmin.declareBinding(binding);
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    public boolean unsubscribeUser(Long userId) {

        try {
            userQueueMessageListener.unregisterUserListener(userId);

            Binding binding = bindingByUser.remove(userId);

            if (binding != null) {
                rabbitAdmin.removeBinding(binding);
            }
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    @Override
    public void sendMessageToUser(Long userId, String message) {

        rabbitTemplate.convertAndSend(userExchange.getName(), userId.toString(), message);
    }
}
