package com.asme.messaging.service.messaging.user;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.ChannelAwareMessageListener;
import org.springframework.stereotype.Component;

import com.asme.messaging.exception.AsmeMessagingException;
import com.rabbitmq.client.Channel;

/**
 * @author Vladyslav Mykhalets
 * @since 21.01.18
 */
@Component
public class UserQueueMessageListener implements ChannelAwareMessageListener {
    private static final Logger log = LoggerFactory.getLogger(UserQueueMessageListener.class);

    private Map<Long, UserMessageListener> messageListenerByUser = new ConcurrentHashMap<>();

    @Override
    public void onMessage(Message message, Channel channel) throws Exception {
        String routingKey = message.getMessageProperties().getReceivedRoutingKey();
        long deliveryTag = message.getMessageProperties().getDeliveryTag();

        UserMessageListener userMessageListener = messageListenerByUser.get(Long.parseLong(routingKey));

        if (userMessageListener != null) {
            try {
                userMessageListener.onMessage(new String(message.getBody()));
            } catch (AsmeMessagingException e) {
                log.error("Exception propagating message to listener by routingKey: " + routingKey, e);
            }

            channel.basicAck(deliveryTag, false);
        } else {
            channel.basicNack(deliveryTag, false, true);
        }
    }

    public void registerUserListener(Long userId, UserMessageListener messageListener) {
        messageListenerByUser.put(userId, messageListener);
    }

    public void unregisterUserListener(Long userId) {
        messageListenerByUser.remove(userId);
    }
}
