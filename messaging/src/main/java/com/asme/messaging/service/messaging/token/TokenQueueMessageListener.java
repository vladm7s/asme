package com.asme.messaging.service.messaging.token;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.ChannelAwareMessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asme.messaging.dto.messaging.token.TokenRevokeMessage;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;

/**
 * @author Vladyslav Mykhalets
 * @since 12.02.18
 */
@Service
public class TokenQueueMessageListener implements ChannelAwareMessageListener {
    private static final Logger log = LoggerFactory.getLogger(TokenQueueMessageListener.class);

    private ObjectMapper mapper = new ObjectMapper();

    @Autowired(required = false)
    private List<TokenMessageListener> tokenMessageListeners = new ArrayList<>();

    @Override
    public void onMessage(Message message, Channel channel) throws Exception {

        TokenRevokeMessage body = mapper.readValue(message.getBody(), TokenRevokeMessage.class);
        log.info("Received message to revoke token id: " + body.getTokenId());

        long deliveryTag = message.getMessageProperties().getDeliveryTag();

        try {
            for (TokenMessageListener tokenMessageListener : tokenMessageListeners) {
                tokenMessageListener.onRevoke(body.getTokenType(), body.getTokenId());
            }

            channel.basicAck(deliveryTag, false);
        } catch (Exception e) {
            channel.basicNack(deliveryTag, false, true);

            log.error("Exception when delivering information about revoked token " + body.getTokenId(), e);
        }
    }
}
