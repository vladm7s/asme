package com.asme.messaging.service.messaging.token;

import com.asme.messaging.dto.messaging.token.TokenType;

/**
 * @author Vladyslav Mykhalets
 * @since 12.02.18
 */
public interface TokenMessageListener {

    void onRevoke(TokenType tokenType, String tokenId);
}
