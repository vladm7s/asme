package com.asme.messaging.service.messaging.token;

import com.asme.messaging.dto.messaging.token.TokenType;

/**
 * @author Vladyslav Mykhalets
 * @since 12.02.18
 */
public interface TokenMessagingService {

    void revokeToken(TokenType tokenType, String tokenId);
}
