package com.asme.messaging.service.messaging.token;

import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asme.messaging.dto.messaging.token.TokenRevokeMessage;
import com.asme.messaging.dto.messaging.token.TokenType;

/**
 * @author Vladyslav Mykhalets
 * @since 12.02.18
 */
@Service
public class RabbitmqTokenMessageService implements TokenMessagingService {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private FanoutExchange fanoutExchange;

    @Override
    public void revokeToken(TokenType tokenType, String tokenId) {

        TokenRevokeMessage message = new TokenRevokeMessage();
        message.setTokenType(tokenType);
        message.setTokenId(tokenId);

        rabbitTemplate.convertAndSend(fanoutExchange.getName(), "", message);
    }
}
