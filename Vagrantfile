
require 'yaml'

ROOT = File.dirname(File.absolute_path(__FILE__))

settings = YAML.load(File.open(File.absolute_path('env.configuration.yml', ROOT)).read)

# REQUIRED to enable disk size change plugin: #: vagrant plugin install vagrant-disksize

Vagrant.configure("2") do |config|

  config.vm.define "server" do |server|
    server.vm.box = "ubuntu/xenial64"
    server.disksize.size = '30GB'

    server.vm.box_check_update = false
    server.vm.network "private_network", ip: "192.168.33.10"

    server.vm.synced_folder %{./virtual/provision/registry/#{settings["docker-registry"]['domain']}/ca/},
                            %{/etc/docker/certs.d/#{settings["docker-registry"]['domain']}}

    server.vm.network "forwarded_port", guest: 8001, host: 8001, auto_correct: true
    server.vm.network "forwarded_port", guest: 5001, host: 5001, auto_correct: true
    server.vm.network "forwarded_port", guest: 8080, host: 8080, auto_correct: true
    server.vm.network "forwarded_port", guest: 8081, host: 8081, auto_correct: true
    server.vm.network "forwarded_port", guest: 8084, host: 8084, auto_correct: true
    server.vm.network "forwarded_port", guest: 5432, host: 5432, auto_correct: true
    server.vm.network "forwarded_port", guest: 5672, host: 5672, auto_correct: true

    server.vm.provider "virtualbox" do |vb|
      vb.name = "asme-server"
      vb.memory = "4096"
    end

    server.vm.provision "provision-vm", type: "shell" do |s|
      s.path = "virtual/provision/provision-vm.sh"
    end

    server.vm.provision "provision-docker", type: "shell" do |s|
      s.path = "virtual/provision/provision-docker.sh"
    end

    server.vm.provision "provision-docker-registry-certificates", type: "shell" do |s|
      s.path = "virtual/provision/provision-docker-registry-certificates.sh"
      s.env = {
          :DOCKER_REGISTRY_DOMAIN => settings['docker-registry']['domain'],
          :DOCKER_REGISTRY_PORT => settings['docker-registry']['port'],
          :DOCKER_REGISTRY_USERNAME => settings['docker-registry']['username'],
          :DOCKER_REGISTRY_PASSWORD => settings['docker-registry']['password'],
          :DOCKER_REGISTRY_BASIC_AUTH => settings['docker-registry']['basic_auth']
      }
    end

  end

  config.vm.define "docker-registry" do |registry|

    registry.vm.box = "ubuntu/xenial64"
    registry.vm.box_check_update = false
    registry.vm.network "private_network", ip: "#{settings['docker-registry']['domain']}"

    registry.vm.synced_folder '.', '/vagrant', disabled: true
    registry.vm.synced_folder %{./virtual/provision/registry/#{settings['docker-registry']['domain']}/certs/},
                              %{/certs/#{settings['docker-registry']['domain']}},
                              type: "rsync",
                              rsync__args: ["--verbose", "--archive", "--delete", "-z"],
                              rsync__auto: false

    registry.vm.provider "virtualbox" do |vb|
      vb.name = "asme-docker-registry"
      vb.memory = "1024"
    end

    registry.vm.provision "provision-docker", type: "shell" do |s|
      s.path = "virtual/provision/provision-docker.sh"
    end

    registry.vm.provision "provision-docker-registry", type: "shell" do |s|
      s.privileged = true
      s.path = "virtual/provision/registry/provision-docker-registry.sh"
      s.env = {
          :DOCKER_REGISTRY_DOMAIN => settings['docker-registry']['domain'],
          :DOCKER_REGISTRY_PORT => settings['docker-registry']['port'],
          :DOCKER_REGISTRY_USERNAME => settings['docker-registry']['username'],
          :DOCKER_REGISTRY_PASSWORD => settings['docker-registry']['password'],
          :DOCKER_REGISTRY_BASIC_AUTH => settings['docker-registry']['basic_auth']
      }
    end

  end

  config.vm.define "kubernetes-master" do |kubernetes|

    kubernetes.vm.box = "ubuntu/xenial64"
    kubernetes.vm.box_check_update = false
    kubernetes.vm.network "private_network", ip: "#{settings['kubernetes-master']['domain']}"
    kubernetes.vm.network :forwarded_port, guest: 22, host: 2244

    kubernetes.vm.synced_folder '.', '/vagrant', disabled: true
    kubernetes.vm.synced_folder %{./virtual/bin/kubernetes},
                                %{/vagrant/virtual/bin/kubernetes}
    kubernetes.vm.synced_folder %{./virtual/provision/registry/#{settings["docker-registry"]['domain']}/ca/},
                                %{/etc/docker/certs.d/#{settings["docker-registry"]['domain']}}

    kubernetes.vm.provider "virtualbox" do |vb|
      vb.name = "asme-kubernetes-master"
      vb.memory = "3072"
      vb.cpus = 2
    end

    kubernetes.vm.provision "provision-docker", type: "shell" do |s|
      s.path = "virtual/provision/kubernetes/provision-docker.sh"
    end

    kubernetes.vm.provision "provision-docker-registry-certificates", type: "shell" do |s|
      s.path = "virtual/provision/provision-docker-registry-certificates.sh"
      s.env = {
          :DOCKER_REGISTRY_DOMAIN => settings['docker-registry']['domain'],
          :DOCKER_REGISTRY_PORT => settings['docker-registry']['port'],
          :DOCKER_REGISTRY_USERNAME => settings['docker-registry']['username'],
          :DOCKER_REGISTRY_PASSWORD => settings['docker-registry']['password'],
          :DOCKER_REGISTRY_BASIC_AUTH => settings['docker-registry']['basic_auth']
      }
    end

    kubernetes.vm.provision "provision-kubernetes", type: "shell" do |s|
      s.path = "virtual/provision/kubernetes/provision-kubernetes.sh"
      s.env = {
          :KUBERNETES_MASTER_DOMAIN => settings['kubernetes-master']['domain'],
          :DOCKER_REGISTRY_DOMAIN => settings['docker-registry']['domain'],
          :DOCKER_REGISTRY_PORT => settings['docker-registry']['port'],
          :DOCKER_REGISTRY_USERNAME => settings['docker-registry']['username'],
          :DOCKER_REGISTRY_PASSWORD => settings['docker-registry']['password']
      }
    end

  end

  # NUM_WORKERS = 1
  #
  # (1..NUM_WORKERS).each do |n|
  #   config.vm.define "worker#{n}" do |worker|
  #     worker.vm.box = 'joatmon08/k8s-kubeadm'
  #     worker.vm.hostname = "worker#{n}"
  #
  #     worker.vm.network 'private_network', ip: "192.168.205.1#{n}"
  #
  #     worker.vm.provider 'virtualbox' do |vb|
  #       vb.memory = '8192'
  #     end
  #
  #     worker.vm.provision 'file', source: 'init-worker.sh', \
  #                                 destination: '/tmp/init-worker.sh'
  #   end
  # end

end
