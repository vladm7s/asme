package com.asme.rest.api.controller.jpa;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.asme.authclient.domain.JwtTokenAuthentication;
import com.asme.common.dto.request.PlanRequestDto;
import com.asme.core.entity.jpa.JpaDriveRoute;
import com.asme.core.entity.jpa.JpaPlanRequest;
import com.asme.core.service.jpa.JpaDriveRouteService;
import com.asme.core.service.jpa.JpaPlanRequestService;
import com.asme.rest.api.transformer.PlanRequestTransformer;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @author Vladyslav Mykhalets
 * @since 07.01.18
 */
@RestController
@RequestMapping(value = "/plan/request", produces = APPLICATION_JSON_VALUE)
public class PlanRequestController {
    private static final Logger log = LoggerFactory.getLogger(PlanRequestController.class);

    @Autowired
    private JpaPlanRequestService planRequestService;

    @Autowired
    private JpaDriveRouteService driveRouteService;

    @Autowired
    private PlanRequestTransformer planRequestTransformer;

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public PlanRequestDto create(@RequestBody @Valid PlanRequestDto planRequestDto) {

        JwtTokenAuthentication authentication = (JwtTokenAuthentication) SecurityContextHolder.getContext().getAuthentication();
        final Long userId = authentication.getUserId();

        JpaPlanRequest jpaPlanRequest = new JpaPlanRequest.PlanRequestBuilder()
                .startLocation(planRequestDto.getStartLocation(), planRequestDto.getStartLocationRadius())
                .startLocationName(planRequestDto.getStartLocationName())
                .destination(planRequestDto.getDestination(), planRequestDto.getDestinationRadius())
                .destinationLocationName(planRequestDto.getDestinationName())
                .build();

        JpaPlanRequest savedPlanRequest = planRequestService.save(jpaPlanRequest);

        Page<JpaDriveRoute> driveRoutes = driveRouteService.findByStartAndDestination(
                userId,
                savedPlanRequest.getStartLocation(), savedPlanRequest.getDestinationLocation(),
                savedPlanRequest.getStartLocationBoundingBox(), savedPlanRequest.getDestinationLocationBoundingBox(),
                new PageRequest(0, 5));

        return planRequestTransformer.transform(savedPlanRequest, driveRoutes.getContent());
    }
}
