package com.asme.rest.api.transformer;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.asme.common.dto.route.DriveRouteDto;
import com.asme.common.dto.request.PlanRequestDto;
import com.asme.core.entity.jpa.JpaDriveRoute;
import com.asme.core.entity.jpa.JpaPlanRequest;
import com.asme.core.utils.GeoUtils;

/**
 * @author Vladyslav Mykhalets
 * @since 07.01.18
 */
@Component
public class PlanRequestTransformer {

    @Autowired
    private DriveRouteTransformer driveRouteTransformer;

    public PlanRequestDto transform(JpaPlanRequest planRequest, List<JpaDriveRoute> driveRoutes) {

        List<DriveRouteDto> driveRoutesDto = driveRoutes
                .stream()
                .map(driveRouteTransformer::transform)
                .collect(Collectors.toList());

        int startRadius = Integer.parseInt(planRequest.getStartLocationRadius().substring(0, planRequest.getStartLocationRadius().length() - 1));
        int destinationRadius = Integer.parseInt(planRequest.getDestinationLocationRadius().substring(0, planRequest.getDestinationLocationRadius().length() - 1));

        return new PlanRequestDto.Builder()
                .id(planRequest.getId().toString())
                .startLocation(GeoUtils.toGeoPoint(planRequest.getStartLocation()))
                .startLocationRadius(startRadius)
                .startLocationName(planRequest.getStartLocationName())
                .destination(GeoUtils.toGeoPoint(planRequest.getDestinationLocation()))
                .destinationRadius(destinationRadius)
                .destinationName(planRequest.getDestinationLocationName())
                .driveRoutes(driveRoutesDto)
                .build();
    }
}
