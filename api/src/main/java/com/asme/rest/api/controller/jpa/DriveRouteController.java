package com.asme.rest.api.controller.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.asme.authclient.domain.JwtTokenAuthentication;
import com.asme.common.dto.route.DriveRouteDto;
import com.asme.core.entity.jpa.JpaDriveRoute;
import com.asme.core.service.jpa.JpaDriveRouteService;
import com.asme.rest.api.transformer.DriveRouteTransformer;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @author Vladyslav Mykhalets
 * @since 07.01.18
 */
@RestController
@RequestMapping(value = "/drive/route", produces = APPLICATION_JSON_VALUE)
public class DriveRouteController {

    @Autowired
    private JpaDriveRouteService driveRouteService;

    @Autowired
    private DriveRouteTransformer driveRouteTransformer;

    @RequestMapping(value = "/fromPlanRoute/{planRouteId}/", method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public Long create(@PathVariable Long planRouteId) {

        JwtTokenAuthentication authentication = (JwtTokenAuthentication) SecurityContextHolder.getContext().getAuthentication();
        final Long userId = authentication.getUserId();
        final String username = authentication.getName();

        JpaDriveRoute savedDriveRoute = driveRouteService.createFromPlanRoute(planRouteId, userId, username);
        return savedDriveRoute.getId();
    }

    @RequestMapping(value = "/search/byDriveRequest/{driveRequestId}", method = RequestMethod.GET)
    @ResponseBody
    public Page<DriveRouteDto> searchByDriveRequest(@PathVariable Long driveRequestId, @PageableDefault Pageable pageable) {
        return driveRouteTransformer.transform(driveRouteService.findByDriveRequest(driveRequestId, pageable),
                pageable);
    }

    @RequestMapping(value = "/search/byPlanRequest/{planRequestId}", method = RequestMethod.GET)
    @ResponseBody
    public Page<DriveRouteDto> searchByPlanRequest(@PathVariable Long planRequestId, @PageableDefault Pageable pageable) {

        JwtTokenAuthentication authentication = (JwtTokenAuthentication) SecurityContextHolder.getContext().getAuthentication();
        final Long userId = authentication.getUserId();

        return driveRouteTransformer.transform(driveRouteService.findByPlanRequest(userId, planRequestId, pageable),
                pageable);
    }

}
