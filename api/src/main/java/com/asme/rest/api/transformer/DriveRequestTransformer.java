package com.asme.rest.api.transformer;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.asme.common.dto.request.DriveRequestDto;
import com.asme.core.entity.jpa.JpaDriveRequest;
import com.asme.core.utils.GeoUtils;

/**
 * @author Vladyslav Mykhalets
 */
@Component
public class DriveRequestTransformer {

    public DriveRequestDto transformJpa(JpaDriveRequest driveRequest) {
        return new DriveRequestDto.Builder()
                .id(driveRequest.getId().toString())
                .userId(driveRequest.getUserId())
                .startLocation(GeoUtils.toGeoPoint(driveRequest.getStartLocation()))
                .destination(GeoUtils.toGeoPoint(driveRequest.getDestinationLocation()))
                .startLocationName(driveRequest.getStartLocationName())
                .destinationName(driveRequest.getDestinationLocationName())
                .build();
    }

    public Page<DriveRequestDto> transformJpa(Page<JpaDriveRequest> driveRequests, Pageable pageable) {

        List<DriveRequestDto> driveRequestsDto = driveRequests.getContent()
                .stream()
                .map(this::transformJpa)
                .collect(Collectors.toList());

        return new PageImpl<>(driveRequestsDto, pageable, driveRequests.getTotalElements());
    }
}
