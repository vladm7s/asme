package com.asme.rest.api.transformer;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.asme.common.dto.route.DriveRouteDto;
import com.asme.core.entity.jpa.JpaDriveRoute;
import com.asme.core.utils.GeoUtils;

/**
 * @author Vladyslav Mykhalets
 * @since 07.01.18
 */
@Component
public class DriveRouteTransformer {

    public DriveRouteDto transform(JpaDriveRoute source) {
        return new DriveRouteDto.Builder()
                .id(String.valueOf(source.getId()))
                .userId(source.getUserId())
                .waypoints(GeoUtils.toGeoPointsList(source.getWaypoints()))
                .geometry(source.getGeometry())
                .distance(source.getDistance())
                .build();
    }

    public Page<DriveRouteDto> transform(Page<JpaDriveRoute> driveRoutes, Pageable pageable) {

        List<DriveRouteDto> driveRoutesDto = driveRoutes.getContent()
                .stream()
                .map(this::transform)
                .collect(Collectors.toList());

        return new PageImpl<>(driveRoutesDto, pageable, driveRoutes.getTotalElements());
    }
}
