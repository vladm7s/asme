package com.asme.rest.api.controller.jpa;

import java.util.Arrays;
import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.asme.authclient.domain.JwtTokenAuthentication;
import com.asme.common.dto.route.PlanRouteDto;
import com.asme.common.dto.route.PlanRouteRequest;
import com.asme.common.geo.GeoPoint;
import com.asme.core.entity.jpa.JpaDriveRequest;
import com.asme.core.entity.jpa.JpaPlanRoute;
import com.asme.core.service.jpa.JpaDriveRequestService;
import com.asme.core.service.jpa.JpaPlanRouteService;
import com.asme.rest.api.transformer.PlanRouteTransformer;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @author Vladyslav Mykhalets
 * @since 02.01.18
 */
@RestController
@RequestMapping(value = "/plan/route", produces = APPLICATION_JSON_VALUE)
public class PlanRouteController {
    private static final Logger log = LoggerFactory.getLogger(PlanRouteController.class);

    @Autowired
    private JpaPlanRouteService jpaPlanRouteService;

    @Autowired
    private JpaDriveRequestService driveRequestService;

    @Autowired
    private PlanRouteTransformer planRouteTransformer;

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public PlanRouteDto create(@RequestBody PlanRouteRequest planRouteRequest) {

        JwtTokenAuthentication authentication = (JwtTokenAuthentication) SecurityContextHolder.getContext().getAuthentication();
        final Long userId = authentication.getUserId();

        JpaPlanRoute planRoute = jpaPlanRouteService.route(planRouteRequest.getWaypoints());

        Page<JpaDriveRequest> driveRequests = driveRequestService.findByRouteLine(userId, planRoute.getPlanRoute(),
                new PageRequest(0, 5));

        return planRouteTransformer.transform(planRoute, driveRequests.getContent());
    }

    @RequestMapping(value = "/async/{latStart}/{lonStart}/{latDestination}/{lonDestination}/", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public CompletableFuture<PlanRouteDto> createAsync(@PathVariable String latStart, @PathVariable String lonStart,
                                                       @PathVariable String latDestination, @PathVariable String lonDestination) {
        log.info("Entering JpaPlanRouteController::createAsync");

        JwtTokenAuthentication authentication = (JwtTokenAuthentication) SecurityContextHolder.getContext().getAuthentication();
        final Long userId = authentication.getUserId();

        CompletableFuture<JpaPlanRoute> planRouteCompletableFuture = jpaPlanRouteService.routeAsync(Arrays.asList(new GeoPoint(lonStart, latStart), new GeoPoint(lonDestination, latDestination)));

        CompletableFuture<PlanRouteDto> planRouteDto = planRouteCompletableFuture.thenApply((JpaPlanRoute planRoute) -> {
            Page<JpaDriveRequest> driveRequests = driveRequestService.findByRouteLine(userId,
                    planRoute.getPlanRoute(), new PageRequest(0, 5));
            return planRouteTransformer.transform(planRoute, driveRequests.getContent());
        });

        log.info("Leaving JpaPlanRouteController::createAsync");

        return planRouteDto;
    }
}
