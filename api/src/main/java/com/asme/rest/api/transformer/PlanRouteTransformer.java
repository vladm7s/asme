package com.asme.rest.api.transformer;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.asme.common.dto.request.DriveRequestDto;
import com.asme.common.dto.route.PlanRouteDto;
import com.asme.core.entity.jpa.JpaDriveRequest;
import com.asme.core.entity.jpa.JpaPlanRoute;
import com.asme.core.utils.GeoUtils;

/**
 * @author Vladyslav Mykhalets
 */
@Component
public class PlanRouteTransformer {

    @Autowired
    private DriveRequestTransformer driveRequestTransformer;

    public PlanRouteDto transform(JpaPlanRoute source, List<JpaDriveRequest> driveRequests) {

        List<DriveRequestDto> driveRequestsDto = driveRequests
                .stream()
                .map(driveRequestTransformer::transformJpa)
                .collect(Collectors.toList());

        return new PlanRouteDto.Builder()
                .id(String.valueOf(source.getId()))
                .planWaypoints(GeoUtils.toGeoPointsList(source.getPlanWaypoints()))
                .planGeometry(source.getPlanGeometry())
                .planDistance(source.getPlanDistance())
                .driveRequests(driveRequestsDto)
                .build();
    }
}
