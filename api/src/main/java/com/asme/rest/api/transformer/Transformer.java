package com.asme.rest.api.transformer;

/**
 * @author Vladyslav Mykhalets
 */
public interface Transformer<S, T> {
    T transform(S source);
}
