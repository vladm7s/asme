package com.asme.rest.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.DispatcherServletAutoConfiguration;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.DispatcherServlet;

import com.asme.authclient.JwtTokenAuthClient;
import com.asme.core.CoreConfiguration;
import com.asme.rest.api.customize.TomcatEmbeddedServletContainerCustomizer;

/**
 * @author Vladyslav Mykhalets
 */
@SpringBootApplication(scanBasePackages = {
        "com.asme.rest.api.controller.advice",
        "com.asme.rest.api.controller.jpa",
        "com.asme.rest.api.customize",
        "com.asme.rest.api.transformer"
})
@Import(value = {
        CoreConfiguration.class,
        JwtTokenAuthClient.class
})
public class Api {

    public static void main(String[] args) {
        SpringApplication.run(Api.class, args);
    }

    @Bean
    public EmbeddedServletContainerCustomizer embeddedServletCustomizer(){
        return new TomcatEmbeddedServletContainerCustomizer();
    }

    @Bean
    public DispatcherServlet dispatcherServlet() {
        return new DispatcherServlet();
    }

    @Bean
    public ServletRegistrationBean dispatcherServletRegistration() {
        ServletRegistrationBean registration = new ServletRegistrationBean(dispatcherServlet(), "/");
        registration.setAsyncSupported(true);
        registration.setName(DispatcherServletAutoConfiguration.DEFAULT_DISPATCHER_SERVLET_REGISTRATION_BEAN_NAME);
        return registration;
    }
}
