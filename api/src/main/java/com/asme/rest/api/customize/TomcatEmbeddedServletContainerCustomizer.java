package com.asme.rest.api.customize;

import org.apache.catalina.connector.Connector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;

/**
 * @author Vladyslav Mykhalets
 * @since 19.10.17
 */
public class TomcatEmbeddedServletContainerCustomizer implements EmbeddedServletContainerCustomizer {

    private static final Logger LOG = LoggerFactory.getLogger(TomcatEmbeddedServletContainerCustomizer.class);

    private int MAX_THREADS = 100;

    @Override
    public void customize(ConfigurableEmbeddedServletContainer factory) {
        if (factory instanceof TomcatEmbeddedServletContainerFactory) {
            customizeTomcat((TomcatEmbeddedServletContainerFactory) factory);
        }
    }

    private Connector additionalHttpConnector() {
        Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
        connector.setScheme("http");
        connector.setPort(8080);
        connector.setSecure(false);
        connector.setRedirectPort(8443);
        return connector;
    }

    public void customizeTomcat(TomcatEmbeddedServletContainerFactory factory) {

        //factory.addAdditionalTomcatConnectors(additionalHttpConnector());

//        factory.addContextCustomizers(context -> {
//            SecurityConstraint constraint = new SecurityConstraint();
//            constraint.setUserConstraint("CONFIDENTIAL");
//            SecurityCollection collection = new SecurityCollection();
//            collection.addPattern("/*");
//            constraint.addCollection(collection);
//            context.addConstraint(constraint);
//        });

        factory.addConnectorCustomizers((TomcatConnectorCustomizer) connector -> {
            Object defaultMaxThreads = connector.getAttribute("maxThreads");
            connector.setAttribute("maxThreads", MAX_THREADS);
            LOG.info("Changed Tomcat connector maxThreads from " + defaultMaxThreads + " to " + MAX_THREADS);
        });
    }
}
