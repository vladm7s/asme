package com.asme.rest.api.controller.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.asme.authclient.domain.JwtTokenAuthentication;
import com.asme.common.dto.request.DriveRequestDto;
import com.asme.core.entity.jpa.JpaDriveRequest;
import com.asme.core.service.jpa.JpaDriveRequestService;
import com.asme.rest.api.transformer.DriveRequestTransformer;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @author Vladyslav Mykhalets
 * @since 02.01.18
 */
@RestController
@RequestMapping(value = "/drive/request", produces = APPLICATION_JSON_VALUE)
public class DriveRequestController {

    @Autowired
    private JpaDriveRequestService driveRequestService;

    @Autowired
    private DriveRequestTransformer driveRequestTransformer;

    @RequestMapping(value = "/fromPlanRequest/{planRequestId}/", method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public Long create(@PathVariable Long planRequestId) {

        JwtTokenAuthentication authentication = (JwtTokenAuthentication) SecurityContextHolder.getContext().getAuthentication();
        final Long userId = authentication.getUserId();
        final String username = authentication.getName();

        JpaDriveRequest savedDriveRequest = driveRequestService.createFromPlanRequest(planRequestId, userId, username);
        return savedDriveRequest.getId();
    }

    @RequestMapping(value = "/search/byDriveRoute/{driveRouteId}", method = RequestMethod.GET)
    @ResponseBody
    public Page<DriveRequestDto> searchByDriveRoute(@PathVariable Long driveRouteId, @PageableDefault Pageable pageable) {
        return driveRequestTransformer.transformJpa(driveRequestService.findByDriveRoute(driveRouteId, pageable),
                pageable);
    }

    @RequestMapping(value = "/search/byPlanRoute/{planRouteId}", method = RequestMethod.GET)
    @ResponseBody
    public Page<DriveRequestDto> searchByPlanRoute(@PathVariable Long planRouteId, @PageableDefault Pageable pageable) {

        JwtTokenAuthentication authentication = (JwtTokenAuthentication) SecurityContextHolder.getContext().getAuthentication();
        final Long userId = authentication.getUserId();

        return driveRequestTransformer.transformJpa(driveRequestService.findByPlanRoute(userId, planRouteId, pageable),
                pageable);
    }
}
