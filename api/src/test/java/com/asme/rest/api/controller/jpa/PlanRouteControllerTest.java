package com.asme.rest.api.controller.jpa;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import com.asme.authclient.domain.JwtTokenAuthentication;
import com.asme.common.dto.route.PlanRouteRequest;
import com.asme.common.geo.GeoPoint;
import com.asme.core.configuration.AsyncConfiguration;
import com.asme.core.entity.jpa.JpaPlanRoute;
import com.asme.core.service.jpa.JpaDriveRequestService;
import com.asme.core.service.jpa.JpaPlanRouteService;
import com.asme.core.service.osrm.OsrmService;
import com.asme.rest.api.controller.jpa.PlanRouteControllerTest.MocksConfig;
import com.asme.rest.api.controller.jpa.PlanRouteControllerTest.TestWebConfig;
import com.asme.rest.api.transformer.DriveRequestTransformer;
import com.asme.rest.api.transformer.PlanRouteTransformer;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.reset;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * @author Vladyslav Mykhalets
 * @since 02.01.18
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
        PlanRouteController.class,
        AsyncConfiguration.class,
        PlanRouteTransformer.class,
        DriveRequestTransformer.class,
        TestWebConfig.class,
        MocksConfig.class
})
@WebAppConfiguration
public class PlanRouteControllerTest {

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private JpaPlanRouteService planRouteService;

    @Autowired
    private JpaDriveRequestService driveRequestService;

    private MockMvc mockMvc;

    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.stream(converters)
                .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
                .findAny()
                .orElse(null);

        assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();

        Long userId = 1L;
        String username = "test-username";
        String tokenId = "test-tokenId";
        String email = "test@test.com";

        Authentication authentication = new JwtTokenAuthentication(tokenId, userId, email, username, null, new ArrayList<>());
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Test
    public void createPost() throws Exception {

        String lonStart = "30.387588";
        String latStart = "50.452806";

        String lonDestination = "30.448539";
        String latDestination = "50.517392";

        List<GeoPoint> geoPoints = Arrays.asList(new GeoPoint(lonStart, latStart), new GeoPoint(lonDestination, latDestination));

        PlanRouteRequest planRouteRequest = new PlanRouteRequest();
        planRouteRequest.setWaypoints(geoPoints);

        JpaPlanRoute planRoute = new JpaPlanRoute();
        Pageable pageable = new PageRequest(0, 5);

        reset(planRouteService);
        expect(planRouteService.route(eq(geoPoints))).andReturn(planRoute);
        replay(planRouteService);

        reset(driveRequestService);
        expect(driveRequestService.findByRouteLine(eq(1L), eq(planRoute.getPlanRoute()), eq(pageable))).andReturn(new PageImpl<>(new ArrayList<>()));
        replay(driveRequestService);

        mockMvc.perform(post("/plan/route/").content(json(planRouteRequest)).contentType(contentType))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.planWaypoints", hasSize(0)));
    }

    private String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        mappingJackson2HttpMessageConverter.write(o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }

    @Configuration
    public static class MocksConfig {

        @Bean
        public JpaPlanRouteService jpaPlanRouteService() {
            return createMock(JpaPlanRouteService.class);
        }

        @Bean
        public JpaDriveRequestService jpaDriveRequestService() {
            return createMock(JpaDriveRequestService.class);
        }

        @Bean
        public OsrmService osrmService() {
            return createMock(OsrmService.class);
        }
    }

    @Configuration
    public static class TestWebConfig extends WebMvcConfigurationSupport {

        @Bean
        public MappingJackson2HttpMessageConverter customJackson2HttpMessageConverter() {
            MappingJackson2HttpMessageConverter jsonConverter = new MappingJackson2HttpMessageConverter();
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            jsonConverter.setObjectMapper(objectMapper);
            return jsonConverter;
        }

        @Override
        public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
            converters.add(customJackson2HttpMessageConverter());
            super.addDefaultHttpMessageConverters(converters);
        }
    }
}
