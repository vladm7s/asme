package com.asme.core.elasticsearch.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Mapping;

import com.asme.common.geo.GeoPoint;
import com.asme.common.geo.GeoShapeLine;
import com.asme.common.utils.CommonGeoUtils;
import com.asme.core.utils.GeoUtils;
import com.google.common.collect.Lists;
import com.vividsolutions.jts.geom.LineString;

/**
 * @author Vladyslav Mykhalets
 */
@Document(indexName = Constants.INDEX_NAME, type = PlanRoute.PLAN_ROUTE_INDEX_TYPE)
@Mapping(mappingPath = "/mappings/planRoute.json")
public class PlanRoute {

    public static final String PLAN_ROUTE_INDEX_TYPE = "planRoute";

    @Id
    private String id;

    private List<GeoPoint> planWaypoints = new ArrayList<>();

    private String planGeometry;

    private BigDecimal planDistance;

    private GeoShapeLine planRoute;

    @LastModifiedDate
    private Date lastModified;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public GeoShapeLine getPlanRoute() {
        return planRoute;
    }

    public void setPlanRoute(GeoShapeLine planRoute) {
        this.planRoute = planRoute;
    }

    public List<GeoPoint> getPlanWaypoints() {
        return planWaypoints;
    }

    public void setPlanWaypoints(List<GeoPoint> planWaypoints) {
        this.planWaypoints = planWaypoints;
    }

    public String getPlanGeometry() {
        return planGeometry;
    }

    public void setPlanGeometry(String planGeometry) {
        this.planGeometry = planGeometry;
    }

    public BigDecimal getPlanDistance() {
        return planDistance;
    }

    public void setPlanDistance(BigDecimal planDistance) {
        this.planDistance = planDistance;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    @Override
    public String toString() {
        return "PlanRoute{" +
                "id='" + id + '\'' +
                ", planWaypoints=" + CommonGeoUtils.geoPointsToString(planWaypoints) +
                ", planDistance=" + planDistance +
                ", planRoute=" + planRoute +
                ", lastModified=" + lastModified +
                '}';
    }

    public static class PlanRouteBuilder {

        private String id;

        private List<GeoPoint> planWaypoints = new ArrayList<>();

        private String planGeometry;

        private BigDecimal planDistance;

        private GeoShapeLine planRoute;

        private Date lastModified;

        public PlanRouteBuilder id(String id) {
            this.id = id;
            return this;
        }

        public PlanRouteBuilder lastModified(Date date) {
            this.lastModified = date;
            return this;
        }

        public PlanRouteBuilder planWaypoints(List<GeoPoint> waypoints) {
            this.planWaypoints = Lists.newArrayList(waypoints);
            return this;
        }

        public PlanRouteBuilder nextPlanWaypoint(GeoPoint waypoint) {
            this.planWaypoints.add(waypoint);
            return this;
        }

        public PlanRouteBuilder planRoute(GeoShapeLine geoShapeLine) {
            this.planRoute = geoShapeLine;
            return this;
        }

        public PlanRouteBuilder planGeometry(String geometry) {
            this.planGeometry = geometry;
            return this;
        }

        public PlanRouteBuilder planDistance(BigDecimal planDistance) {
            this.planDistance = planDistance;
            return this;
        }

        public PlanRouteBuilder planRoute(LineString lineString) {
            this.planRoute = GeoUtils.fromJTSLineString(lineString);
            return this;
        }

        public PlanRoute build() {
            PlanRoute planRoute = new PlanRoute();
            planRoute.setId(this.id);
            planRoute.setPlanWaypoints(this.planWaypoints);
            planRoute.setPlanGeometry(this.planGeometry);
            planRoute.setPlanRoute(this.planRoute);
            planRoute.setPlanDistance(planDistance);
            planRoute.setLastModified(this.lastModified);
            return planRoute;
        }
    }
}
