package com.asme.core.elasticsearch.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.asme.common.dto.route.DriveRouteSearchParameters;
import com.asme.core.elasticsearch.entity.DriveRoute;

/**
 * @author Vladyslav Mykhalets
 */
public interface DriveRouteService {

    DriveRoute save(DriveRoute driveRoute);

    DriveRoute findOne(String driveRouteId);

    Page<DriveRoute> find(String driveRequestId, Pageable pageable);

    Page<DriveRoute> find(DriveRouteSearchParameters searchParameters, Pageable pageable);
}
