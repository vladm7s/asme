package com.asme.core.elasticsearch.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.asme.core.elasticsearch.entity.DriveRequest;

/**
 * @author Vladyslav Mykhalets
 */
public interface DriveRequestRepository extends ElasticsearchRepository<DriveRequest, String> {

}
