package com.asme.core.elasticsearch.entity;

/**
 * @author Vladyslav Mykhalets
 */
public class Constants {

    private Constants() {
    }

    public static final String INDEX_NAME = "asme";
}
