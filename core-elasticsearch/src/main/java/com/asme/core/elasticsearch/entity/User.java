package com.asme.core.elasticsearch.entity;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import com.asme.common.geo.GeoPoint;

/**
 * @author Vladyslav Mykhalets
 */
@Document(indexName = Constants.INDEX_NAME, type = User.USER_INDEX_TYPE)
public class User implements Serializable {

    public static final String USER_INDEX_TYPE = "user";

    @Id
    private String id;

    private String name;

    private GeoPoint location;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GeoPoint getLocation() {
        return location;
    }

    public void setLocation(GeoPoint location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", location=" + location +
                '}';
    }
}
