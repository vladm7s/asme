package com.asme.core.elasticsearch.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import org.elasticsearch.common.geo.ShapeRelation;
import org.elasticsearch.common.geo.builders.LineStringBuilder;
import org.elasticsearch.common.geo.builders.ShapeBuilder;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.GeoShapeQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.asme.common.dto.request.DriveRequestSearchParameters;
import com.asme.common.enums.Direction;
import com.asme.common.geo.GeoShapeLine;
import com.asme.core.elasticsearch.entity.Constants;
import com.asme.core.elasticsearch.entity.DriveRequest;
import com.asme.core.elasticsearch.entity.DriveRoute;
import com.asme.core.elasticsearch.repository.DriveRequestRepository;
import com.asme.core.utils.GeoUtils;

/**
 * @author Vladyslav Mykhalets
 */
@Component
public class DriveRequestServiceImpl implements DriveRequestService {
    private static final Logger log = LoggerFactory.getLogger(DriveRequestServiceImpl.class);

    @Autowired
    private DriveRequestRepository driveRequestRepository;

    @Autowired
    private DriveRouteService driveRouteService;

    public DriveRequest findOne(String driveRequestId) {
        return driveRequestRepository.findOne(driveRequestId);
    }

    public DriveRequest save(DriveRequest driveRequest) {
        return driveRequestRepository.save(driveRequest);
    }

    public Page<DriveRequest> find(String driveRouteId, Pageable pageable) {
        long start = System.currentTimeMillis();
        QueryBuilder query = buildQuery(driveRouteId);

        DriveRoute driveRoute = driveRouteService.findOne(driveRouteId);
        Page<DriveRequest> biDirectionRequests = driveRequestRepository.search(query, pageable);

        long intermediate = System.currentTimeMillis();
        log.info("Searching DRIVE REQUESTS in ElasticSearch took: %s ms", (intermediate - start));

        Page<DriveRequest> forwardRequests = filterForwardDirection(driveRoute.getRoute(), biDirectionRequests);

        long end = System.currentTimeMillis();
        log.info("Filtering DRIVE REQUESTS took: %s ms", (end - intermediate));

        return forwardRequests;
    }

    public Page<DriveRequest> find(GeoShapeLine route, Pageable pageable) {
        long start = System.currentTimeMillis();
        QueryBuilder query = buildQuery(route);

        Page<DriveRequest> biDirectionRequests = driveRequestRepository.search(query, pageable);

        long intermediate = System.currentTimeMillis();
        log.info("Search DRIVE REQUESTS in ElasticSearch took: %s ms", (intermediate - start));

        Page<DriveRequest> forwardRequests = filterForwardDirection(route, biDirectionRequests);

        long end = System.currentTimeMillis();
        log.info("Filtering DRIVE REQUESTS took: %s ms", (end - intermediate));

        return forwardRequests;
    }

    public Page<DriveRequest> find(DriveRequestSearchParameters searchParameters, Pageable pageable) {
        QueryBuilder query = buildQuery(searchParameters);
        return driveRequestRepository.search(query, pageable);
    }

    private Page<DriveRequest> filterForwardDirection(GeoShapeLine driveRoute, Page<DriveRequest> biDirectionRequests) {
        List<DriveRequest> requests = biDirectionRequests.getContent();
        List<DriveRequest> filteredRequests = requests.stream()
                .filter(driveRequest -> Direction.FORWARD == GeoUtils.resolveDirection(driveRoute, driveRequest.getStartLocation(), driveRequest.getDestination()))
                .collect(Collectors.toList());
        return new PageImpl<>(filteredRequests);
    }

    private QueryBuilder buildQuery(GeoShapeLine route) {
        BoolQueryBuilder query = QueryBuilders.boolQuery();

        LineStringBuilder routeBuilder = ShapeBuilder.newLineString();
        for (BigDecimal[] coordinate : route.getCoordinates()) {
            routeBuilder.point(coordinate[0].doubleValue(), coordinate[1].doubleValue());
        }

        GeoShapeQueryBuilder geoQueryStartLocation = QueryBuilders.geoIntersectionQuery(DriveRequest.START_LOCATION_FIELD, routeBuilder);
        GeoShapeQueryBuilder geoQueryDestination = QueryBuilders.geoIntersectionQuery(DriveRequest.DESTINATION_FIELD, routeBuilder);

        query.filter(geoQueryStartLocation).filter(geoQueryDestination);

        if (log.isDebugEnabled()) {
            log.debug("Search route: {} \nJSON Query built: {}", route, query.toString());
        }

        return query;
    }

    private QueryBuilder buildQuery(String routeId) {

        BoolQueryBuilder query = QueryBuilders.boolQuery();

        GeoShapeQueryBuilder geoQueryStartLocation = QueryBuilders.geoIntersectionQuery(DriveRequest.START_LOCATION_FIELD, routeId,
                DriveRoute.DRIVE_ROUTE_INDEX_TYPE)
                .indexedShapeIndex(Constants.INDEX_NAME)
                .indexedShapePath(DriveRoute.ROUTE_FIELD);

        GeoShapeQueryBuilder geoQueryDestination = QueryBuilders.geoIntersectionQuery(DriveRequest.DESTINATION_FIELD, routeId,
                DriveRoute.DRIVE_ROUTE_INDEX_TYPE)
                .indexedShapeIndex(Constants.INDEX_NAME)
                .indexedShapePath(DriveRoute.ROUTE_FIELD);

        query.filter(geoQueryStartLocation).filter(geoQueryDestination);

        if (log.isDebugEnabled()) {
            log.debug("Search route id: {} \nJSON Query built: {}", routeId, query.toString());
        }

        return query;
    }

    private QueryBuilder buildQuery(DriveRequestSearchParameters searchParameters) {

        if (searchParameters == null
                || (searchParameters.getStartLocation() == null && searchParameters.getDestination() == null)) {

            QueryBuilder query = QueryBuilders.matchAllQuery();

            if (log.isDebugEnabled()) {
                log.debug("Search parameters processed: {} \nJSON Query built: {}", searchParameters, query.toString());
            }

            return query;
        }

        BoolQueryBuilder query = QueryBuilders.boolQuery();

        if (searchParameters.getStartLocation() != null) {

            double lon = searchParameters.getStartLocation().getCoordinates()[0].doubleValue();
            double lat = searchParameters.getStartLocation().getCoordinates()[1].doubleValue();

            ShapeBuilder shapeBuilder = ShapeBuilder.newPoint(lon, lat);
            GeoShapeQueryBuilder geoQuery = QueryBuilders.geoShapeQuery(DriveRequest.START_LOCATION_FIELD, shapeBuilder, ShapeRelation.CONTAINS);
            query.filter(geoQuery);
        }

        if (searchParameters.getDestination() != null) {

            double lon = searchParameters.getDestination().getCoordinates()[0].doubleValue();
            double lat = searchParameters.getDestination().getCoordinates()[1].doubleValue();

            ShapeBuilder shapeBuilder = ShapeBuilder.newPoint(lon, lat);
            GeoShapeQueryBuilder geoQuery = QueryBuilders.geoShapeQuery(DriveRequest.DESTINATION_FIELD, shapeBuilder, ShapeRelation.CONTAINS);
            query.filter(geoQuery);
        }

        if (log.isDebugEnabled()) {
            log.debug("Search parameters processed: {} \nJSON Query built: {}", searchParameters, query.toString());
        }

        return query;
    }
}
