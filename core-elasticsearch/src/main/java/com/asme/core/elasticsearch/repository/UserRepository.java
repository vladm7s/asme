package com.asme.core.elasticsearch.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.asme.core.elasticsearch.entity.User;


/**
 * @author Vladyslav Mykhalets
 */
public interface UserRepository extends ElasticsearchRepository<User, String> {

}
