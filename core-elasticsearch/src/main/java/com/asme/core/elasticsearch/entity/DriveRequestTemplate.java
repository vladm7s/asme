package com.asme.core.elasticsearch.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import com.asme.common.geo.GeoPoint;

/**
 * @author Vladyslav Mykhalets
 */
@Document(indexName = Constants.INDEX_NAME, type = "driveRequestTemplate")
public class DriveRequestTemplate {

    @Id
    private String id;

    private String userId;

    private String destinationName;

    private GeoPoint destination;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDestinationName() {
        return destinationName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    public GeoPoint getDestination() {
        return destination;
    }

    public void setDestination(GeoPoint destination) {
        this.destination = destination;
    }

    @Override
    public String toString() {
        return "DriveRequestTemplate{" +
                "id='" + id + '\'' +
                ", userId='" + userId + '\'' +
                ", destinationName='" + destinationName + '\'' +
                ", destination=" + destination +
                '}';
    }
}
