package com.asme.core.elasticsearch.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Mapping;

import com.asme.common.enums.DriveRequestStatus;
import com.asme.common.geo.GeoShapeCircle;

/**
 * @author Vladyslav Mykhalets
 */
@Document(indexName = Constants.INDEX_NAME, type = DriveRequest.DRIVE_REQUEST_INDEX_TYPE)
@Mapping(mappingPath = "/mappings/driveRequest.json")
public class DriveRequest {

    public static final String DRIVE_REQUEST_INDEX_TYPE = "driveRequest";

    public static final String START_LOCATION_FIELD = "startLocation";

    public static final String DESTINATION_FIELD = "destination";

    @Id
    private String id;

    private Long userId;

    private DriveRequestStatus requestStatus = DriveRequestStatus.OPEN;

    private String startLocationName;

    private GeoShapeCircle startLocation;

    private String destinationName;

    private GeoShapeCircle destination;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public DriveRequestStatus getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(DriveRequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }

    public GeoShapeCircle getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(GeoShapeCircle startLocation) {
        this.startLocation = startLocation;
    }

    public GeoShapeCircle getDestination() {
        return destination;
    }

    public void setDestination(GeoShapeCircle destination) {
        this.destination = destination;
    }

    public String getStartLocationName() {
        return startLocationName;
    }

    public void setStartLocationName(String startLocationName) {
        this.startLocationName = startLocationName;
    }

    public String getDestinationName() {
        return destinationName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    @Override
    public String toString() {
        return "DriveRequest{" +
                "id='" + id + '\'' +
                ", userId='" + userId + '\'' +
                ", requestStatus=" + requestStatus +
                ", startLocationName='" + startLocationName + '\'' +
                ", startLocation=" + startLocation +
                ", destinationName='" + destinationName + '\'' +
                ", destination=" + destination +
                '}';
    }

    public static class DriveRequestBuilder {

        private String id;

        private Long userId;

        private DriveRequestStatus requestStatus = DriveRequestStatus.OPEN;

        private GeoShapeCircle startLocation;

        private GeoShapeCircle destination;

        private String startLocationName;

        private String destinationName;

        public DriveRequestBuilder id(String id) {
            this.id = id;
            return this;
        }

        public DriveRequestBuilder userId(Long userId) {
            this.userId = userId;
            return this;
        }

        public DriveRequestBuilder requestStatus(DriveRequestStatus requestStatus) {
            this.requestStatus = requestStatus;
            return this;
        }

        public DriveRequestBuilder startLocation(GeoShapeCircle startLocation) {
            this.startLocation = startLocation;
            return this;
        }

        public DriveRequestBuilder destination(GeoShapeCircle destination) {
            this.destination = destination;
            return this;
        }

        public DriveRequestBuilder startLocationName(String startLocationName) {
            this.startLocationName = startLocationName;
            return this;
        }

        public DriveRequestBuilder destinationName(String destinationName) {
            this.destinationName = destinationName;
            return this;
        }

        public DriveRequest build() {
            DriveRequest driveRequest = new DriveRequest();
            driveRequest.setId(this.id);
            driveRequest.setUserId(this.userId);
            driveRequest.setRequestStatus(this.requestStatus);
            driveRequest.setStartLocation(this.startLocation);
            driveRequest.setDestination(this.destination);
            driveRequest.setStartLocationName(this.startLocationName);
            driveRequest.setDestinationName(this.destinationName);
            return driveRequest;
        }
    }
}
