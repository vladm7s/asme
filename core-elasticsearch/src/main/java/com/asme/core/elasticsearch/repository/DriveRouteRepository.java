package com.asme.core.elasticsearch.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.asme.core.elasticsearch.entity.DriveRoute;

/**
 * @author Vladyslav Mykhalets
 */
public interface DriveRouteRepository extends ElasticsearchRepository<DriveRoute, String> {

}
