package com.asme.core.elasticsearch.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asme.common.geo.GeoPoint;
import com.asme.common.geo.GeoShapeLine;
import com.asme.common.osrm.OsrmRoute;
import com.asme.common.osrm.OsrmRouteResponse;
import com.asme.common.utils.CommonGeoUtils;
import com.asme.core.elasticsearch.entity.PlanRoute;
import com.asme.core.elasticsearch.entity.PlanRoute.PlanRouteBuilder;
import com.asme.core.elasticsearch.repository.PlanRouteRepository;
import com.asme.core.service.osrm.OsrmService;
import com.asme.core.utils.GeoUtils;

/**
 * @author Vladyslav Mykhalets
 */
@Service
public class PlanRouteServiceImpl implements PlanRouteService {
    private static final Logger log = LoggerFactory.getLogger(PlanRouteServiceImpl.class);

    @Autowired
    private PlanRouteRepository planRouteRepository;

    @Autowired
    private OsrmService osrmService;

    @Autowired
    private Executor executorService;

    @Override
    public PlanRoute findOne(String planRouteId) {
        return planRouteRepository.findOne(planRouteId);
    }

    @Override
    public PlanRoute route(List<GeoPoint> waypoints) {

        OsrmRouteResponse routeResponse = osrmService.route(waypoints);

        if (routeResponse == null) {
            if (log.isErrorEnabled()) {
                log.error("Could not plan route for waypoints: {}", CommonGeoUtils.geoPointsToString(waypoints));
            }
            return null;
        }

        long start = System.currentTimeMillis();
        OsrmRoute route = routeResponse.getRoutes().get(0);

        String planGeometry = route.getGeometry();
        log.info("OSRM route geometry: {}", planGeometry);

        BigDecimal planDistance = route.getDistance();
        log.info("OSRM route distance: {} meters", planDistance);

        GeoShapeLine routeLine = GeoUtils.fromOsrmRoute(route);
        if (log.isDebugEnabled()) {
            log.debug("OSRM route response converted to GeoShapeLine: {}", routeLine);
        }

        PlanRoute planRoute = new PlanRouteBuilder()
                .id(UUID.randomUUID().toString())
                .lastModified(new Date())
                .planWaypoints(waypoints)
                .planDistance(planDistance)
                .planGeometry(planGeometry)
                .planRoute(routeLine)
                .build();

        PlanRoute savedPlanRoute = planRouteRepository.save(planRoute);

        long stop = System.currentTimeMillis();
        if (log.isInfoEnabled()) {
            log.info("Saving PlanRoute in ElasticSearch took: {} ms", (stop - start));
        }

        return savedPlanRoute;
    }

    @Override
    public CompletableFuture<PlanRoute> routeAsync(List<GeoPoint> waypoints) {

        CompletableFuture<OsrmRouteResponse> routeResponseFuture = osrmService.routeAsync(waypoints);

        CompletableFuture<PlanRoute> planRouteFuture = routeResponseFuture.thenApplyAsync(routeResponse -> {

            if (routeResponse == null) {
                log.error("Could not plan route for waypoints: {}", CommonGeoUtils.geoPointsToString(waypoints));
                return null;
            }

            OsrmRoute route = routeResponse.getRoutes().get(0);

            String planGeometry = route.getGeometry();
            log.info("OSRM route geometry: {}", planGeometry);

            BigDecimal planDistance = route.getDistance();
            log.info("OSRM route distance: {} meters", planDistance);

            GeoShapeLine routeLine = GeoUtils.fromOsrmRoute(route);
            if (log.isDebugEnabled()) {
                log.debug("OSRM route response converted to GeoShapeLine: {}", routeLine);
            }

            return new PlanRouteBuilder()
                    .id(UUID.randomUUID().toString())
                    .lastModified(new Date())
                    .planWaypoints(waypoints)
                    .planDistance(planDistance)
                    .planGeometry(planGeometry)
                    .planRoute(routeLine)
                    .build();
        }, executorService);

        return planRouteFuture.thenApply(planRoute -> {

            long start = System.currentTimeMillis();
            PlanRoute savedPlanRoute = planRouteRepository.save(planRoute);

            long stop = System.currentTimeMillis();
            log.info("Saving PlanRoute in ElasticSearch took: {} ms", (stop - start));

            return savedPlanRoute;
        });
    }
}
