package com.asme.core.elasticsearch.configuration;

import java.net.InetAddress;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

import com.asme.core.elasticsearch.properties.ElasticSearchProperties;

/**
 * @author Vladyslav Mykhalets
 * @since 03.04.18
 */
@Configuration
@EnableConfigurationProperties(ElasticSearchProperties.class)
@PropertySource("elasticsearch.properties")
@EnableElasticsearchRepositories(basePackages = "com.asme.core.elasticsearch.repository")
public class ElasticSearchConfiguration {

    @Autowired
    private ElasticSearchProperties elasticSearchProperties;

    @Bean
    public Client client() throws Exception {

        Settings esSettings = Settings.settingsBuilder()
                .put("cluster.name", elasticSearchProperties.getClusterName())
                .build();

        return TransportClient.builder()
                .settings(esSettings)
                .build()
                .addTransportAddress(
                        new InetSocketTransportAddress(InetAddress.getByName(elasticSearchProperties.getHost()),
                                elasticSearchProperties.getPort()));
    }

    @Bean
    public ElasticsearchOperations elasticSearchTemplate() throws Exception {
        return new ElasticsearchTemplate(client());
    }
}
