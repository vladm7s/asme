package com.asme.core.elasticsearch.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import com.asme.common.geo.GeoShapeLine;

/**
 * @author Vladyslav Mykhalets
 */
@Document(indexName = Constants.INDEX_NAME, type = "driveRouteTemplate")
public class DriveRouteTemplate {

    @Id
    private String id;

    private String userId;

    private GeoShapeLine route;

    private int maxPassengersCount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public GeoShapeLine getRoute() {
        return route;
    }

    public void setRoute(GeoShapeLine route) {
        this.route = route;
    }

    public int getMaxPassengersCount() {
        return maxPassengersCount;
    }

    public void setMaxPassengersCount(int maxPassengersCount) {
        this.maxPassengersCount = maxPassengersCount;
    }

    @Override
    public String toString() {
        return "DriveRouteTemplate{" +
                "id='" + id + '\'' +
                ", userId='" + userId + '\'' +
                ", route=" + route +
                ", maxPassengersCount=" + maxPassengersCount +
                '}';
    }
}
