package com.asme.core.elasticsearch.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Mapping;

import com.asme.common.geo.GeoPoint;
import com.asme.common.geo.GeoShapeLine;
import com.asme.common.enums.DriveRouteStatus;
import com.asme.core.utils.GeoUtils;
import com.vividsolutions.jts.geom.LineString;

/**
 * @author Vladyslav Mykhalets
 */
@Document(indexName = Constants.INDEX_NAME, type = DriveRoute.DRIVE_ROUTE_INDEX_TYPE)
@Mapping(mappingPath = "/mappings/driveRoute.json")
public class DriveRoute {

    public static final String DRIVE_ROUTE_INDEX_TYPE = "driveRoute";

    public static final String ROUTE_FIELD = "route";

    @Id
    private String id;

    private String userId;

    private DriveRouteStatus routeStatus = DriveRouteStatus.OPEN;

    private GeoPoint driverLocation;

    private GeoShapeLine route;

    private int passengersCount;

    private int maxPassengersCount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public GeoPoint getDriverLocation() {
        return driverLocation;
    }

    public void setDriverLocation(GeoPoint driverLocation) {
        this.driverLocation = driverLocation;
    }

    public GeoShapeLine getRoute() {
        return route;
    }

    public void setRoute(GeoShapeLine route) {
        this.route = route;
    }

    public int getPassengersCount() {
        return passengersCount;
    }

    public void setPassengersCount(int passengersCount) {
        this.passengersCount = passengersCount;
    }

    public int getMaxPassengersCount() {
        return maxPassengersCount;
    }

    public void setMaxPassengersCount(int maxPassengersCount) {
        this.maxPassengersCount = maxPassengersCount;
    }

    public DriveRouteStatus getRouteStatus() {
        return routeStatus;
    }

    public void setRouteStatus(DriveRouteStatus routeStatus) {
        this.routeStatus = routeStatus;
    }

    @Override
    public String toString() {
        return "DriveRoute{" +
                "id='" + id + '\'' +
                ", userId='" + userId + '\'' +
                ", routeStatus=" + routeStatus +
                ", driverLocation=" + driverLocation +
                ", route=" + route +
                ", passengersCount=" + passengersCount +
                ", maxPassengersCount=" + maxPassengersCount +
                '}';
    }

    public static class DriveRouteBuilder {

        private String id;

        private String userId;

        private DriveRouteStatus routeStatus = DriveRouteStatus.OPEN;

        private GeoPoint driverLocation;

        private GeoShapeLine route;

        private int passengersCount;

        private int maxPassengersCount;

        public DriveRouteBuilder id(String id) {
            this.id = id;
            return this;
        }

        public DriveRouteBuilder userId(String userId) {
            this.userId = userId;
            return this;
        }

        public DriveRouteBuilder routeStatus(DriveRouteStatus routeStatus) {
            this.routeStatus = routeStatus;
            return this;
        }

        public DriveRouteBuilder driverLocation(GeoPoint driverLocation) {
            this.driverLocation = driverLocation;
            return this;
        }

        public DriveRouteBuilder route(LineString lineString) {
            this.route = GeoUtils.fromJTSLineString(lineString);
            return this;
        }

        public DriveRouteBuilder passengersCount(int passengersCount) {
            this.passengersCount = passengersCount;
            return this;
        }

        public DriveRouteBuilder maxPassengersCount(int maxPassengersCount) {
            this.maxPassengersCount = maxPassengersCount;
            return this;
        }

        public DriveRoute build() {
            DriveRoute driveRoute = new DriveRoute();
            driveRoute.setId(this.id);
            driveRoute.setUserId(this.userId);
            driveRoute.setRouteStatus(this.routeStatus);
            driveRoute.setDriverLocation(this.driverLocation);
            driveRoute.setRoute(this.route);
            driveRoute.setPassengersCount(this.passengersCount);
            driveRoute.setMaxPassengersCount(this.maxPassengersCount);
            return driveRoute;
        }
    }
}
