package com.asme.core.elasticsearch.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author Vladyslav Mykhalets
 * @since 03.04.18
 */
@ConfigurationProperties("elasticsearch")
public class ElasticSearchProperties {

    private String host;

    private int port;

    private String clusterName;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }
    
}

