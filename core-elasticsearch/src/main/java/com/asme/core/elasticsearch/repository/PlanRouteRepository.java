package com.asme.core.elasticsearch.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.asme.core.elasticsearch.entity.PlanRoute;

/**
 * @author Vladyslav Mykhalets
 */
public interface PlanRouteRepository extends ElasticsearchRepository<PlanRoute, String> {
}
