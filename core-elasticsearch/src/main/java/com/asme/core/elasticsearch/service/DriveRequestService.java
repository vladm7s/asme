package com.asme.core.elasticsearch.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.asme.common.dto.request.DriveRequestSearchParameters;
import com.asme.common.geo.GeoShapeLine;
import com.asme.core.elasticsearch.entity.DriveRequest;

/**
 * @author Vladyslav Mykhalets
 */
public interface DriveRequestService {

    DriveRequest save(DriveRequest driveRequest);

    DriveRequest findOne(String driveRequestId);

    Page<DriveRequest> find(String driveRouteId, Pageable pageable);

    Page<DriveRequest> find(GeoShapeLine route, Pageable pageable);

    Page<DriveRequest> find(DriveRequestSearchParameters searchParameters, Pageable pageable);
}
