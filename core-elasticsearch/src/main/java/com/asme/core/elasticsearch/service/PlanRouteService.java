package com.asme.core.elasticsearch.service;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import com.asme.common.geo.GeoPoint;
import com.asme.core.elasticsearch.entity.PlanRoute;

/**
 * @author Vladyslav Mykhalets
 */
public interface PlanRouteService {

    PlanRoute findOne(String planRouteId);

    PlanRoute route(List<GeoPoint> waypoints);

    CompletableFuture<PlanRoute> routeAsync(List<GeoPoint> waypoints);

}
