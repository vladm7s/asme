package com.asme.core.elasticsearch.service;

import java.util.List;
import java.util.stream.Collectors;

import org.elasticsearch.common.geo.builders.ShapeBuilder;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.GeoShapeQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.asme.common.dto.route.DriveRouteSearchParameters;
import com.asme.common.enums.Direction;
import com.asme.common.geo.GeoShapeCircle;
import com.asme.core.elasticsearch.entity.Constants;
import com.asme.core.elasticsearch.entity.DriveRequest;
import com.asme.core.elasticsearch.entity.DriveRoute;
import com.asme.core.elasticsearch.repository.DriveRouteRepository;
import com.asme.core.utils.GeoUtils;

/**
 * @author Vladyslav Mykhalets
 */
@Service
public class DriveRouteServiceImpl implements DriveRouteService {
    private static final Logger log = LoggerFactory.getLogger(DriveRouteServiceImpl.class);

    @Autowired
    private DriveRouteRepository driveRouteRepository;

    @Autowired
    private DriveRequestService driveRequestService;

    public DriveRoute save(DriveRoute driveRoute) {
        return driveRouteRepository.save(driveRoute);
    }

    public DriveRoute findOne(String driveRouteId) {
        return driveRouteRepository.findOne(driveRouteId);
    }

    public Page<DriveRoute> find(String driveRequestId, Pageable pageable) {
        long start = System.currentTimeMillis();
        QueryBuilder query = buildQuery(driveRequestId);

        DriveRequest driveRequest = driveRequestService.findOne(driveRequestId);
        Page<DriveRoute> biDirectionRoutes = driveRouteRepository.search(query, pageable);

        long intermediate = System.currentTimeMillis();
        log.info("Searching DRIVE ROUTES in ElasticSearch took: " + (intermediate - start) + " ms");

        Page<DriveRoute> forwardRoutes = filterForwardDirection(driveRequest.getStartLocation(), driveRequest.getDestination(),
                biDirectionRoutes);

        long end = System.currentTimeMillis();
        log.info("Filtering DRIVE ROUTES took: " + (end - intermediate) + " ms");

        return forwardRoutes;
    }

    @Override
    public Page<DriveRoute> find(DriveRouteSearchParameters searchParameters, Pageable pageable) {
        long start = System.currentTimeMillis();
        QueryBuilder query = buildQuery(searchParameters.getStartLocation(), searchParameters.getDestination());

        Page<DriveRoute> biDirectionRoutes = driveRouteRepository.search(query, pageable);

        long intermediate = System.currentTimeMillis();
        log.info("Searching DRIVE ROUTES in ElasticSearch took: " + (intermediate - start) + " ms");

        Page<DriveRoute> forwardRoutes = filterForwardDirection(searchParameters.getStartLocation(), searchParameters.getDestination(),
                biDirectionRoutes);

        long end = System.currentTimeMillis();
        log.info("Filtering DRIVE ROUTES took: " + (end - intermediate) + " ms");

        return forwardRoutes;
    }

    private Page<DriveRoute> filterForwardDirection(GeoShapeCircle startLocation, GeoShapeCircle destination, Page<DriveRoute> biDirectionRoutes) {
        List<DriveRoute> routes = biDirectionRoutes.getContent();
        List<DriveRoute> filteredRoutes = routes.stream()
                .filter(driveRoute -> Direction.FORWARD == GeoUtils.resolveDirection(driveRoute.getRoute(), startLocation, destination))
                .collect(Collectors.toList());
        return new PageImpl<>(filteredRoutes);
    }

    private QueryBuilder buildQuery(String driveRequestId) {

        BoolQueryBuilder query = QueryBuilders.boolQuery();

        GeoShapeQueryBuilder geoQueryStartLocation = QueryBuilders.geoIntersectionQuery(DriveRoute.ROUTE_FIELD, driveRequestId,
                DriveRequest.DRIVE_REQUEST_INDEX_TYPE)
                .indexedShapeIndex(Constants.INDEX_NAME)
                .indexedShapePath(DriveRequest.START_LOCATION_FIELD);

        GeoShapeQueryBuilder geoQueryDestination = QueryBuilders.geoIntersectionQuery(DriveRoute.ROUTE_FIELD, driveRequestId,
                DriveRequest.DRIVE_REQUEST_INDEX_TYPE)
                .indexedShapeIndex(Constants.INDEX_NAME)
                .indexedShapePath(DriveRequest.DESTINATION_FIELD);

        query.filter(geoQueryStartLocation).filter(geoQueryDestination);

        if (log.isDebugEnabled()) {
            log.debug("Search request id: {} \nJSON Query built: {}", driveRequestId, query.toString());
        }

        return query;
    }

    private QueryBuilder buildQuery(GeoShapeCircle startLocation, GeoShapeCircle destination) {

        BoolQueryBuilder query = QueryBuilders.boolQuery();

        ShapeBuilder startLocationBuilder = ShapeBuilder.newCircleBuilder()
                .center(startLocation.getCoordinates()[0].doubleValue(), startLocation.getCoordinates()[1].doubleValue())
                .radius(startLocation.getRadiusInMeters(), DistanceUnit.METERS);

        ShapeBuilder destinationBuilder = ShapeBuilder.newCircleBuilder()
                .center(destination.getCoordinates()[0].doubleValue(), destination.getCoordinates()[1].doubleValue())
                .radius(destination.getRadiusInMeters(), DistanceUnit.METERS);

        GeoShapeQueryBuilder geoQueryStartLocation = QueryBuilders.geoIntersectionQuery(DriveRoute.ROUTE_FIELD, startLocationBuilder);
        GeoShapeQueryBuilder geoQueryDestination = QueryBuilders.geoIntersectionQuery(DriveRoute.ROUTE_FIELD, destinationBuilder);

        query.filter(geoQueryStartLocation).filter(geoQueryDestination);

        if (log.isDebugEnabled()) {
            log.debug("Search start location: {} \ndestination: {} \nJSON Query built: {}", startLocation, destination, query.toString());
        }

        return query;
    }
}
