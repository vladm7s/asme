package com.asme.common.enums;

/**
 * @author Vladyslav Mykhalets
 */
public enum Direction {
    FORWARD,
    REVERSE
}
