package com.asme.common.enums;

/**
 * @author Vladyslav Mykhalets
 * @since 22.11.17
 */
public enum DriveRouteStatus {
    OPEN,
    FULL,
    CLOSED
}
