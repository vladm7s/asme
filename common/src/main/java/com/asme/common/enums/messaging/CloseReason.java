package com.asme.common.enums.messaging;

/**
 * @author Vladyslav Mykhalets
 * @since 15.04.18
 */
public enum CloseReason {

    TOKEN_REVOKED("Access token is revoked"),
    TOKEN_EXPIRED("Access token is expired");

    String message;

    CloseReason(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
