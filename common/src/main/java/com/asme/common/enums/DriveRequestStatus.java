package com.asme.common.enums;

/**
 * @author Vladyslav Mykhalets
 * @since 22.11.17
 */
public enum DriveRequestStatus {
    OPEN,
    ACCEPTED,
    CLOSED
}
