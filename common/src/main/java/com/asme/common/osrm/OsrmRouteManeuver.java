package com.asme.common.osrm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Vladyslav Mykhalets
 */
public class OsrmRouteManeuver {

    private List<BigDecimal> location = new ArrayList<>();

    public List<BigDecimal> getLocation() {
        return location;
    }

    public void setLocation(List<BigDecimal> location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "OsrmRouteManeuver{" +
                "\n location=" + location +
                '}';
    }
}
