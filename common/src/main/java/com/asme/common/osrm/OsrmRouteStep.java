package com.asme.common.osrm;

import java.math.BigDecimal;

/**
 * @author Vladyslav Mykhalets
 */
public class OsrmRouteStep {

    private String name;

    private BigDecimal duration;

    private BigDecimal distance;

    private String geometry;

    private OsrmRouteManeuver maneuver;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getDuration() {
        return duration;
    }

    public void setDuration(BigDecimal duration) {
        this.duration = duration;
    }

    public BigDecimal getDistance() {
        return distance;
    }

    public void setDistance(BigDecimal distance) {
        this.distance = distance;
    }

    public String getGeometry() {
        return geometry;
    }

    public void setGeometry(String geometry) {
        this.geometry = geometry;
    }

    public OsrmRouteManeuver getManeuver() {
        return maneuver;
    }

    public void setManeuver(OsrmRouteManeuver maneuver) {
        this.maneuver = maneuver;
    }

    @Override
    public String toString() {
        return "OsrmRouteStep{" +
                "\n name='" + name + '\'' +
                ",\n duration=" + duration +
                ",\n distance=" + distance +
                ",\n geometry=" + geometry +
                ",\n maneuver=" + maneuver +
                '}';
    }
}
