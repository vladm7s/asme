package com.asme.common.osrm;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Vladyslav Mykhalets
 */
public class OsrmRouteResponse {

    private String code;

    private List<OsrmRoute> routes = new ArrayList<>();

    private List<OsrmWaypoint> waypoints = new ArrayList<>();

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<OsrmRoute> getRoutes() {
        return routes;
    }

    public void setRoutes(List<OsrmRoute> routes) {
        this.routes = routes;
    }

    public List<OsrmWaypoint> getWaypoints() {
        return waypoints;
    }

    public void setWaypoints(List<OsrmWaypoint> waypoints) {
        this.waypoints = waypoints;
    }

    @Override
    public String toString() {
        return "OsrmRouteResponse{" +
                "\n code='" + code + '\'' +
                ",\n routes=" + routes +
                ",\n waypoints=" + waypoints +
                '}';
    }
}
