package com.asme.common.osrm;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Vladyslav Mykhalets
 */
public class OsrmWaypoint {

    private String hint;

    private String name;

    private List<BigDecimal> location;

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<BigDecimal> getLocation() {
        return location;
    }

    public void setLocation(List<BigDecimal> location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "OsrmWaypoint{" +
                "\n hint='" + hint + '\'' +
                ",\n name='" + name + '\'' +
                ",\n location=" + location +
                '}';
    }
}
