package com.asme.common.osrm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Vladyslav Mykhalets
 */
public class OsrmRoute {

    private String geometry;

    private List<OsrmRouteLeg> legs = new ArrayList<>();

    private BigDecimal distance;

    private BigDecimal duration;

    public String getGeometry() {
        return geometry;
    }

    public void setGeometry(String geometry) {
        this.geometry = geometry;
    }

    public List<OsrmRouteLeg> getLegs() {
        return legs;
    }

    public void setLegs(List<OsrmRouteLeg> legs) {
        this.legs = legs;
    }

    public BigDecimal getDistance() {
        return distance;
    }

    public void setDistance(BigDecimal distance) {
        this.distance = distance;
    }

    public BigDecimal getDuration() {
        return duration;
    }

    public void setDuration(BigDecimal duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "OsrmRoute{" +
                "\n geometry='" + geometry + '\'' +
                ",\n legs=" + legs +
                ",\n distance=" + distance +
                ",\n duration=" + duration +
                '}';
    }
}
