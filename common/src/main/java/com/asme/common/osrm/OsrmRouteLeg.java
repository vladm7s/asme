package com.asme.common.osrm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Vladyslav Mykhalets
 */
public class OsrmRouteLeg {

    private String summary;

    private BigDecimal distance;

    private BigDecimal duration;

    private List<OsrmRouteStep> steps = new ArrayList<>();

    public List<OsrmRouteStep> getSteps() {
        return steps;
    }

    public void setSteps(List<OsrmRouteStep> steps) {
        this.steps = steps;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public BigDecimal getDistance() {
        return distance;
    }

    public void setDistance(BigDecimal distance) {
        this.distance = distance;
    }

    public BigDecimal getDuration() {
        return duration;
    }

    public void setDuration(BigDecimal duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "OsrmRouteLeg{" +
                "\n summary='" + summary + '\'' +
                ",\n distance=" + distance +
                ",\n duration=" + duration +
                ",\n steps=" + steps +
                '}';
    }
}
