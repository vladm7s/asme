package com.asme.common.functional;

/**
 * @author Vladyslav Mykhalets
 * @since 10.04.18
 */
public interface Consumer<T> {

    void accept(T t);
}
