package com.asme.common.functional;

/**
 * @author Vladyslav Mykhalets
 * @since 10.04.18
 */
public interface BiConsumer<T, U> {

    void accept(T t, U u);
}
