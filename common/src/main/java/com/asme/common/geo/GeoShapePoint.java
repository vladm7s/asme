package com.asme.common.geo;

import java.math.BigDecimal;
import java.util.Arrays;

/**
 * @author Vladyslav Mykhalets
 */
public class GeoShapePoint {

    private String type = "point";

    private BigDecimal[] coordinates = new BigDecimal[2]; // lon, lat

    public GeoShapePoint() {
    }

    public GeoShapePoint(String lonLat) {
        this.coordinates = new BigDecimal[]{
                new BigDecimal(lonLat.split(",")[0].trim()),
                new BigDecimal(lonLat.split(",")[1].trim())
        };
    }

    public GeoShapePoint(BigDecimal lon, BigDecimal lat) {
        this.coordinates = new BigDecimal[]{lon, lat};
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal[] getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(BigDecimal[] coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    public String toString() {
        return "GeoShapePoint{" +
                "type='" + type + '\'' +
                ", coordinates=" + Arrays.toString(coordinates) +
                '}';
    }
}
