package com.asme.common.geo;

import java.math.BigDecimal;
import java.util.Arrays;

/**
 * @author Vladyslav Mykhalets
 */
public class GeoShapeCircle {

    private String type = "circle";

    private BigDecimal[] coordinates = new BigDecimal[2]; // lon, lat

    private String radius;

    public GeoShapeCircle() {
    }

    public GeoShapeCircle(String lonLat, String radius) {
        this.coordinates = new BigDecimal[]{
                new BigDecimal(lonLat.split(",")[0].trim()),
                new BigDecimal(lonLat.split(",")[1].trim())
        };
        this.radius = radius;
    }

    public GeoShapeCircle(BigDecimal lon, BigDecimal lat, String radius) {
        this.coordinates = new BigDecimal[]{lon, lat};
        this.radius = radius;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal[] getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(BigDecimal[] coordinates) {
        this.coordinates = coordinates;
    }

    public String getRadius() {
        return radius;
    }

    public Long getRadiusInMeters() {
        return Long.parseLong(radius);
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }

    @Override
    public String toString() {
        return "GeoShapeCircle{" +
                "type='" + type + '\'' +
                ", coordinates=" + Arrays.toString(coordinates) +
                ", radius='" + radius + '\'' +
                '}';
    }
}
