package com.asme.common.geo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Vladyslav Mykhalets
 */
public class GeoShapeLine {

    private String type = "linestring";

    private List<BigDecimal[]> coordinates = new ArrayList<>(); // lon, lat

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<BigDecimal[]> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<BigDecimal[]> coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    public String toString() {

        StringBuilder coordinatesString = new StringBuilder();
        coordinatesString.append('[');
        for (BigDecimal[] coordinate : coordinates) {
            coordinatesString.append("\n").append(Arrays.toString(coordinate));
        }
        coordinatesString.append(']');

        return "GeoShapeLine{" +
                "type='" + type + '\'' +
                ", coordinates=" + coordinatesString +
                "]}";
    }
}
