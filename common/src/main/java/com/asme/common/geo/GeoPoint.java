package com.asme.common.geo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Vladyslav Mykhalets
 */
public class GeoPoint implements Serializable {
    private static final long serialVersionUID = 1L;

    private BigDecimal lon;

    private BigDecimal lat;

    public GeoPoint() {
    }

    public GeoPoint(BigDecimal lon, BigDecimal lat) {
        this.lon = lon;
        this.lat = lat;
    }

    public GeoPoint(String lon, String lat) {
        this.lon = new BigDecimal(lon);
        this.lat = new BigDecimal(lat);
    }

    public BigDecimal getLon() {
        return lon;
    }

    public void setLon(BigDecimal lon) {
        this.lon = lon;
    }

    public BigDecimal getLat() {
        return lat;
    }

    public void setLat(BigDecimal lat) {
        this.lat = lat;
    }

    @Override
    public String toString() {
        return "GeoPoint{" +
                "lat='" + lat + '\'' +
                ", lon='" + lon + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GeoPoint geoPoint = (GeoPoint) o;

        if (!lon.equals(geoPoint.lon)) return false;
        return lat.equals(geoPoint.lat);
    }

    @Override
    public int hashCode() {
        int result = lon.hashCode();
        result = 31 * result + lat.hashCode();
        return result;
    }
}
