package com.asme.common.dto.messaging;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * @author Vladyslav Mykhalets
 * @since 07.04.18
 */
@JsonTypeName("COMMUNICATE")
public class CommunicateMessage extends WebSocketMessage {

    private Long toUser;

    private String message;

    @Override
    public String getType() {
        return MessageType.COMMUNICATE.name();
    }

    public Long getToUser() {
        return toUser;
    }

    public void setToUser(Long toUser) {
        this.toUser = toUser;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
