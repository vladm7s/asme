package com.asme.common.dto.authorization.token;

import java.util.Date;

/**
 * @author Vladyslav Mykhalets
 * @since 10.02.18
 */
public class AccessTokenDto {

    public static final String REFRESH_TOKEN_ID = "refreshToken.tokenId";

    private String tokenId;

    private String refreshTokenId;

    private String username;

    private Date expiryDate;

    private boolean revoked;

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public String getRefreshTokenId() {
        return refreshTokenId;
    }

    public void setRefreshTokenId(String refreshTokenId) {
        this.refreshTokenId = refreshTokenId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public boolean isRevoked() {
        return revoked;
    }

    public void setRevoked(boolean revoked) {
        this.revoked = revoked;
    }
}
