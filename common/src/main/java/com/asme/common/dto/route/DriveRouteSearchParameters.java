package com.asme.common.dto.route;

import com.asme.common.geo.GeoShapeCircle;

/**
 * @author Vladyslav Mykhalets
 */
public class DriveRouteSearchParameters {

    private GeoShapeCircle startLocation;

    private GeoShapeCircle destination;

    public DriveRouteSearchParameters() {
    }

    public DriveRouteSearchParameters(GeoShapeCircle startLocation, GeoShapeCircle destination) {
        this.startLocation = startLocation;
        this.destination = destination;
    }

    public GeoShapeCircle getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(GeoShapeCircle startLocation) {
        this.startLocation = startLocation;
    }

    public GeoShapeCircle getDestination() {
        return destination;
    }

    public void setDestination(GeoShapeCircle destination) {
        this.destination = destination;
    }

    @Override
    public String toString() {
        return "DriveRouteSearchParameters{" +
                "startLocation=" + startLocation +
                ", destination=" + destination +
                '}';
    }
}
