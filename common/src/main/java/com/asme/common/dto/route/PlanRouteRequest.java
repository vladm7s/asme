package com.asme.common.dto.route;

import java.util.ArrayList;
import java.util.List;

import com.asme.common.geo.GeoPoint;

/**
 * @author Vladyslav Mykhalets
 * @since 06.04.18
 */
public class PlanRouteRequest {

    private List<GeoPoint> waypoints = new ArrayList<>();

    public void addWaypoint(GeoPoint geoPoint) {
        waypoints.add(geoPoint);
    }

    public List<GeoPoint> getWaypoints() {
        return waypoints;
    }

    public void setWaypoints(List<GeoPoint> waypoints) {
        this.waypoints = waypoints;
    }
}
