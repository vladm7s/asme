package com.asme.common.dto.messaging;

import com.asme.common.dto.messaging.notification.MatchedRequestNotification;
import com.asme.common.dto.messaging.notification.MatchedRouteNotification;
import com.asme.common.dto.messaging.reply.ConnectReply;
import com.asme.common.dto.messaging.reply.StartRequestReply;
import com.asme.common.dto.messaging.reply.StartRouteReply;
import com.asme.common.dto.messaging.request.StartRequestMessage;
import com.asme.common.dto.messaging.request.StartRouteMessage;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * @author Vladyslav Mykhalets
 * @since 07.04.18
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = ConnectMessage.class, name = "CONNECT"),
        @JsonSubTypes.Type(value = ConnectReply.class, name = "CONNECT_REPLY"),

        @JsonSubTypes.Type(value = DisconnectMessage.class, name = "DISCONNECT"),
        @JsonSubTypes.Type(value = CommunicateMessage.class, name = "COMMUNICATE"),

        @JsonSubTypes.Type(value = StartRequestMessage.class, name = "START_REQUEST"),
        @JsonSubTypes.Type(value = StartRequestReply.class, name = "START_REQUEST_REPLY"),

        @JsonSubTypes.Type(value = StartRouteMessage.class, name = "START_ROUTE"),
        @JsonSubTypes.Type(value = StartRouteReply.class, name = "START_ROUTE_REPLY"),

        @JsonSubTypes.Type(value = MatchedRequestNotification.class, name = "MATCHED_REQUEST_NOTIFICATION"),
        @JsonSubTypes.Type(value = MatchedRouteNotification.class, name = "MATCHED_ROUTE_NOTIFICATION"),
})
public abstract class WebSocketMessage {

    protected String type;

    public abstract String getType();

    public void setType(String type) {
        this.type = type;
    }
}
