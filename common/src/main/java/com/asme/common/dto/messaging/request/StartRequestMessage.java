package com.asme.common.dto.messaging.request;

import com.asme.common.dto.messaging.MessageType;
import com.asme.common.dto.messaging.WebSocketMessage;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * @author Vladyslav Mykhalets
 * @since 09.04.18
 */
@JsonTypeName("START_REQUEST")
public class StartRequestMessage extends WebSocketMessage {

    private Long planRequestId;

    @Override
    public String getType() {
        return MessageType.START_REQUEST.name();
    }

    public Long getPlanRequestId() {
        return planRequestId;
    }

    public void setPlanRequestId(Long planRequestId) {
        this.planRequestId = planRequestId;
    }
}
