package com.asme.common.dto.request;

import com.asme.common.geo.GeoShapePoint;

/**
 * @author Vladyslav Mykhalets
 */
public class DriveRequestSearchParameters {

    private GeoShapePoint startLocation;

    private GeoShapePoint destination;

    public GeoShapePoint getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(GeoShapePoint startLocation) {
        this.startLocation = startLocation;
    }

    public GeoShapePoint getDestination() {
        return destination;
    }

    public void setDestination(GeoShapePoint destination) {
        this.destination = destination;
    }

    @Override
    public String toString() {
        return "DriveRequestSearchParameters{" +
                "startLocation=" + startLocation +
                ", destination=" + destination +
                '}';
    }

    public static class DriveRequestSearchParametersBuilder {

        private GeoShapePoint startLocation;

        private GeoShapePoint destination;

        public DriveRequestSearchParametersBuilder startLocation(GeoShapePoint startLocation) {
            this.startLocation = startLocation;
            return this;
        }

        public DriveRequestSearchParametersBuilder destination(GeoShapePoint destination) {
            this.destination = destination;
            return this;
        }

        public DriveRequestSearchParameters build() {
            DriveRequestSearchParameters searchParameters = new DriveRequestSearchParameters();
            searchParameters.setStartLocation(this.startLocation);
            searchParameters.setDestination(this.destination);
            return searchParameters;
        }
    }
}
