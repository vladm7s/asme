package com.asme.common.dto.route;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.asme.common.dto.StatusDto;
import com.asme.common.enums.DriveRouteStatus;
import com.asme.common.geo.GeoPoint;

/**
 * @author Vladyslav Mykhalets
 * @since 16.10.17
 */
public class DriveRouteDto extends StatusDto {

    private String id;

    private Long userId;

    private List<GeoPoint> waypoints;

    private String geometry;

    private BigDecimal distance;

    private DriveRouteStatus routeStatus = DriveRouteStatus.OPEN;

    private int passengersCount;

    private int maxPassengersCount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public List<GeoPoint> getWaypoints() {
        return waypoints;
    }

    public void setWaypoints(List<GeoPoint> waypoints) {
        this.waypoints = waypoints;
    }

    public String getGeometry() {
        return geometry;
    }

    public void setGeometry(String geometry) {
        this.geometry = geometry;
    }

    public BigDecimal getDistance() {
        return distance;
    }

    public void setDistance(BigDecimal distance) {
        this.distance = distance;
    }

    public DriveRouteStatus getRouteStatus() {
        return routeStatus;
    }

    public void setRouteStatus(DriveRouteStatus routeStatus) {
        this.routeStatus = routeStatus;
    }

    public int getPassengersCount() {
        return passengersCount;
    }

    public void setPassengersCount(int passengersCount) {
        this.passengersCount = passengersCount;
    }

    public int getMaxPassengersCount() {
        return maxPassengersCount;
    }

    public void setMaxPassengersCount(int maxPassengersCount) {
        this.maxPassengersCount = maxPassengersCount;
    }

    public static class Builder {

        private String id;

        private Long userId;

        private List<GeoPoint> waypoints = new ArrayList<>();

        private String geometry;

        private BigDecimal distance;

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder userId(Long userId) {
            this.userId = userId;
            return this;
        }

        public Builder waypoints(List<GeoPoint> waypoints) {
            this.waypoints = new ArrayList<>();
            this.waypoints.addAll(waypoints);
            return this;
        }

        public Builder nextWaypoint(GeoPoint waypoint) {
            this.waypoints.add(waypoint);
            return this;
        }

        public Builder geometry(String geometry) {
            this.geometry = geometry;
            return this;
        }

        public Builder distance(BigDecimal distance) {
            this.distance = distance;
            return this;
        }

        public DriveRouteDto build() {
            DriveRouteDto routeDto = new DriveRouteDto();
            routeDto.setId(this.id);
            routeDto.setUserId(this.userId);
            routeDto.setWaypoints(this.waypoints);
            routeDto.setGeometry(this.geometry);
            routeDto.setDistance(this.distance);
            return routeDto;
        }
    }
}
