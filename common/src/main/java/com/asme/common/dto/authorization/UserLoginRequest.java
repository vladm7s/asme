package com.asme.common.dto.authorization;

/**
 * @author Vladyslav Mykhalets
 * @since 27.01.18
 */
public class UserLoginRequest {

    private String username;

    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
