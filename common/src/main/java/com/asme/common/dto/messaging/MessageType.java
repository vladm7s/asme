package com.asme.common.dto.messaging;

/**
 * @author Vladyslav Mykhalets
 * @since 07.04.18
 */
public enum MessageType {
    CONNECT,
    CONNECT_REPLY,

    DISCONNECT,
    COMMUNICATE,

    START_REQUEST,
    START_ROUTE,

    START_REQUEST_REPLY,
    START_ROUTE_REPLY,

    MATCHED_ROUTE_NOTIFICATION,
    MATCHED_REQUEST_NOTIFICATION
}
