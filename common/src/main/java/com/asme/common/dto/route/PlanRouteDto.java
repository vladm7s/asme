package com.asme.common.dto.route;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.asme.common.dto.StatusDto;
import com.asme.common.dto.request.DriveRequestDto;
import com.asme.common.geo.GeoPoint;
import com.asme.common.utils.CommonGeoUtils;

/**
 * @author Vladyslav Mykhalets
 */
public class PlanRouteDto extends StatusDto {

    private String id;

    private List<GeoPoint> planWaypoints;

    private List<DriveRequestDto> driveRequests;

    private String planGeometry;

    private BigDecimal planDistance;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<GeoPoint> getPlanWaypoints() {
        return planWaypoints;
    }

    public void setPlanWaypoints(List<GeoPoint> planWaypoints) {
        this.planWaypoints = planWaypoints;
    }

    public List<DriveRequestDto> getDriveRequests() {
        return driveRequests;
    }

    public void setDriveRequests(List<DriveRequestDto> driveRequests) {
        this.driveRequests = driveRequests;
    }

    public String getPlanGeometry() {
        return planGeometry;
    }

    public void setPlanGeometry(String planGeometry) {
        this.planGeometry = planGeometry;
    }

    public BigDecimal getPlanDistance() {
        return planDistance;
    }

    public void setPlanDistance(BigDecimal planDistance) {
        this.planDistance = planDistance;
    }

    @Override
    public String toString() {
        return "PlanRouteDto{" +
                "id='" + id + '\'' +
                ", planWaypoints=" + CommonGeoUtils.geoPointsToString(planWaypoints) +
                ", driveRequests=" + driveRequests +
                ", planDistance=" + planDistance +
                '}';
    }

    public static class Builder {

        private String id;

        private List<GeoPoint> planWaypoints = new ArrayList<>();

        private List<DriveRequestDto> driveRequests = new ArrayList<>();

        private String planGeometry;

        private BigDecimal planDistance;

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder planWaypoints(List<GeoPoint> waypoints) {
            this.planWaypoints = new ArrayList<>();
            this.planWaypoints.addAll(waypoints);
            return this;
        }

        public Builder driveRequests(List<DriveRequestDto> driveRequests) {
            this.driveRequests = new ArrayList<>();
            this.driveRequests.addAll(driveRequests);
            return this;
        }

        public Builder nextPlanWaypoint(GeoPoint waypoint) {
            this.planWaypoints.add(waypoint);
            return this;
        }

        public Builder planGeometry(String geometry) {
            this.planGeometry = geometry;
            return this;
        }

        public Builder planDistance(BigDecimal planDistance) {
            this.planDistance = planDistance;
            return this;
        }

        public PlanRouteDto build() {
            PlanRouteDto planRoute = new PlanRouteDto();
            planRoute.setId(this.id);
            planRoute.setPlanWaypoints(this.planWaypoints);
            planRoute.setDriveRequests(this.driveRequests);
            planRoute.setPlanGeometry(this.planGeometry);
            planRoute.setPlanDistance(this.planDistance);
            return planRoute;
        }
    }
}
