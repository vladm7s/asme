package com.asme.common.dto.authorization;

import com.asme.common.dto.StatusDto;

/**
 * @author Vladyslav Mykhalets
 * @since 09.02.18
 */
public class UserInfoResponse extends StatusDto {

    private Long userId;

    private String username;

    private String email;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
