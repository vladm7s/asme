package com.asme.common.dto.messaging.reply;

import com.asme.common.dto.messaging.MessageType;
import com.asme.common.dto.messaging.WebSocketMessage;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * @author Vladyslav Mykhalets
 * @since 09.04.18
 */
@JsonTypeName("START_ROUTE_REPLY")
public class StartRouteReply extends WebSocketMessage {

    private Long driveRouteId;

    @Override
    public String getType() {
        return MessageType.START_ROUTE_REPLY.name();
    }

    public Long getDriveRouteId() {
        return driveRouteId;
    }

    public void setDriveRouteId(Long driveRouteId) {
        this.driveRouteId = driveRouteId;
    }
}
