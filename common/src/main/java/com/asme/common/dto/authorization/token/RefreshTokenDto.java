package com.asme.common.dto.authorization.token;

import java.util.Date;

/**
 * @author Vladyslav Mykhalets
 * @since 10.02.18
 */
public class RefreshTokenDto {

    public static final String TOKEN_ID = "tokenId";

    public static final String USERNAME = "user.username";

    public static final String REVOKED = "revoked";

    private String tokenId;

    private String username;

    private Date issueDate;

    private Boolean revoked;

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public Boolean isRevoked() {
        return revoked;
    }

    public void setRevoked(Boolean revoked) {
        this.revoked = revoked;
    }
}
