package com.asme.common.dto.request;

import com.asme.common.dto.StatusDto;
import com.asme.common.geo.GeoPoint;

/**
 * @author Vladyslav Mykhalets
 */
public class DriveRequestDto extends StatusDto {

    private String id;

    private Long userId;

    private GeoPoint startLocation;

    private Integer startLocationRadius;

    private String startLocationName;

    private GeoPoint destination;

    private Integer destinationRadius;

    private String destinationName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getStartLocationName() {
        return startLocationName;
    }

    public void setStartLocationName(String startLocationName) {
        this.startLocationName = startLocationName;
    }

    public GeoPoint getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(GeoPoint startLocation) {
        this.startLocation = startLocation;
    }

    public String getDestinationName() {
        return destinationName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    public GeoPoint getDestination() {
        return destination;
    }

    public void setDestination(GeoPoint destination) {
        this.destination = destination;
    }

    public Integer getStartLocationRadius() {
        return startLocationRadius;
    }

    public void setStartLocationRadius(Integer startLocationRadius) {
        this.startLocationRadius = startLocationRadius;
    }

    public Integer getDestinationRadius() {
        return destinationRadius;
    }

    public void setDestinationRadius(Integer destinationRadius) {
        this.destinationRadius = destinationRadius;
    }

    @Override
    public String toString() {
        return "DriveRequestDto{" +
                "id='" + id + '\'' +
                ", userId='" + userId + '\'' +
                ", startLocation=" + startLocation +
                ", startLocationRadius=" + startLocationRadius +
                ", startLocationName='" + startLocationName + '\'' +
                ", destination=" + destination +
                ", destinationRadius=" + destinationRadius +
                ", destinationName='" + destinationName + '\'' +
                '}';
    }

    public static class Builder {

        private String id;

        private Long userId;

        private GeoPoint startLocation;

        private GeoPoint destination;

        private String startLocationName;

        private String destinationName;

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder userId(Long userId) {
            this.userId = userId;
            return this;
        }

        public Builder startLocation(GeoPoint startLocation) {
            this.startLocation = startLocation;
            return this;
        }

        public Builder destination(GeoPoint destination) {
            this.destination = destination;
            return this;
        }

        public Builder startLocationName(String startLocationName) {
            this.startLocationName = startLocationName;
            return this;
        }

        public Builder destinationName(String destinationName) {
            this.destinationName = destinationName;
            return this;
        }

        public DriveRequestDto build() {
            DriveRequestDto driveRequest = new DriveRequestDto();
            driveRequest.setId(this.id);
            driveRequest.setUserId(this.userId);
            driveRequest.setStartLocation(this.startLocation);
            driveRequest.setDestination(this.destination);
            driveRequest.setStartLocationName(this.startLocationName);
            driveRequest.setDestinationName(this.destinationName);
            return driveRequest;
        }
    }
}
