package com.asme.common.dto.messaging.reply;

import com.asme.common.dto.messaging.MessageType;
import com.asme.common.dto.messaging.WebSocketMessage;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * @author Vladyslav Mykhalets
 * @since 09.04.18
 */
@JsonTypeName("START_REQUEST_REPLY")
public class StartRequestReply extends WebSocketMessage {

    private Long driveRequestId;

    @Override
    public String getType() {
        return MessageType.START_REQUEST_REPLY.name();
    }

    public Long getDriveRequestId() {
        return driveRequestId;
    }

    public void setDriveRequestId(Long driveRequestId) {
        this.driveRequestId = driveRequestId;
    }
}
