package com.asme.common.dto;

import java.io.Serializable;

/**
 * @author Vladyslav Mykhalets
 * @since 09.11.17
 */
public class StatusDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private boolean error;

    private String message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
