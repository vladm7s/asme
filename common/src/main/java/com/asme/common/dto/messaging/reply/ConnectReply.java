package com.asme.common.dto.messaging.reply;

import com.asme.common.dto.messaging.MessageType;
import com.asme.common.dto.messaging.WebSocketMessage;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * @author Vladyslav Mykhalets
 * @since 13.04.18
 */
@JsonTypeName("CONNECT_REPLY")
public class ConnectReply extends WebSocketMessage {

    private boolean connected;

    @Override
    public String getType() {
        return MessageType.CONNECT_REPLY.name();
    }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }
}
