package com.asme.common.dto.request;

import java.util.ArrayList;
import java.util.List;

import com.asme.common.dto.route.DriveRouteDto;
import com.asme.common.dto.StatusDto;
import com.asme.common.geo.GeoPoint;

/**
 * @author Vladyslav Mykhalets
 * @since 07.01.18
 */
public class PlanRequestDto extends StatusDto {

    private String id;

    private GeoPoint startLocation;

    private Integer startLocationRadius;

    private String startLocationName;

    private GeoPoint destination;

    private Integer destinationRadius;

    private String destinationName;

    private List<DriveRouteDto> driveRoutes;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public GeoPoint getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(GeoPoint startLocation) {
        this.startLocation = startLocation;
    }

    public Integer getStartLocationRadius() {
        return startLocationRadius;
    }

    public void setStartLocationRadius(Integer startLocationRadius) {
        this.startLocationRadius = startLocationRadius;
    }

    public String getStartLocationName() {
        return startLocationName;
    }

    public void setStartLocationName(String startLocationName) {
        this.startLocationName = startLocationName;
    }

    public GeoPoint getDestination() {
        return destination;
    }

    public void setDestination(GeoPoint destination) {
        this.destination = destination;
    }

    public Integer getDestinationRadius() {
        return destinationRadius;
    }

    public void setDestinationRadius(Integer destinationRadius) {
        this.destinationRadius = destinationRadius;
    }

    public String getDestinationName() {
        return destinationName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    public List<DriveRouteDto> getDriveRoutes() {
        return driveRoutes;
    }

    public void setDriveRoutes(List<DriveRouteDto> driveRoutes) {
        this.driveRoutes = driveRoutes;
    }

    @Override
    public String toString() {
        return "PlanRequestDto{" +
                "id='" + id + '\'' +
                ", startLocation=" + startLocation +
                ", startLocationRadius=" + startLocationRadius +
                ", startLocationName='" + startLocationName + '\'' +
                ", destination=" + destination +
                ", destinationRadius=" + destinationRadius +
                ", destinationName='" + destinationName + '\'' +
                '}';
    }

    public static class Builder {

        private String id;

        private GeoPoint startLocation;

        private Integer startLocationRadius;

        private String startLocationName;

        private GeoPoint destination;

        private Integer destinationRadius;

        private String destinationName;

        private List<DriveRouteDto> driveRoutes = new ArrayList<>();

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder startLocation(GeoPoint startLocation) {
            this.startLocation = startLocation;
            return this;
        }

        public Builder startLocationRadius(int radius) {
            this.startLocationRadius = radius;
            return this;
        }

        public Builder startLocationName(String startLocationName) {
            this.startLocationName = startLocationName;
            return this;
        }

        public Builder destination(GeoPoint destination) {
            this.destination = destination;
            return this;
        }

        public Builder destinationRadius(int radius) {
            this.destinationRadius = radius;
            return this;
        }

        public Builder destinationName(String destinationName) {
            this.destinationName = destinationName;
            return this;
        }

        public Builder driveRoutes(List<DriveRouteDto> driveRoutes) {
            this.driveRoutes = new ArrayList<>();
            this.driveRoutes.addAll(driveRoutes);
            return this;
        }

        public PlanRequestDto build() {
            PlanRequestDto planRequest = new PlanRequestDto();
            planRequest.setId(this.id);

            planRequest.setStartLocation(this.startLocation);
            planRequest.setStartLocationName(this.startLocationName);
            planRequest.setStartLocationRadius(this.startLocationRadius);

            planRequest.setDestination(this.destination);
            planRequest.setDestinationName(this.destinationName);
            planRequest.setDestinationRadius(this.destinationRadius);

            planRequest.setDriveRoutes(this.driveRoutes);
            return planRequest;
        }
    }
}
