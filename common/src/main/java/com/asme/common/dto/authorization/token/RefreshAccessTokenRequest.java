package com.asme.common.dto.authorization.token;

/**
 * @author Vladyslav Mykhalets
 * @since 16.04.18
 */
public class RefreshAccessTokenRequest {

    private String refreshTokenContent;

    public String getRefreshTokenContent() {
        return refreshTokenContent;
    }

    public void setRefreshTokenContent(String refreshTokenContent) {
        this.refreshTokenContent = refreshTokenContent;
    }
}
