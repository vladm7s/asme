package com.asme.common.dto.messaging;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * @author Vladyslav Mykhalets
 * @since 07.04.18
 */
@JsonTypeName("DISCONNECT")
public class DisconnectMessage extends WebSocketMessage {

    @Override
    public String getType() {
        return MessageType.DISCONNECT.name();
    }
}
