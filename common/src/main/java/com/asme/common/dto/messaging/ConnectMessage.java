package com.asme.common.dto.messaging;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * @author Vladyslav Mykhalets
 * @since 07.04.18
 */
@JsonTypeName("CONNECT")
public class ConnectMessage extends WebSocketMessage {

    @Override
    public String getType() {
        return MessageType.CONNECT.name();
    }
}
