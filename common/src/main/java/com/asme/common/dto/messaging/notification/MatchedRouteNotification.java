package com.asme.common.dto.messaging.notification;

import com.asme.common.dto.messaging.MessageType;
import com.asme.common.dto.messaging.WebSocketMessage;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * @author Vladyslav Mykhalets
 * @since 10.04.18
 */
@JsonTypeName("MATCHED_ROUTE_NOTIFICATION")
public class MatchedRouteNotification extends WebSocketMessage {

    private Long driveRouteId;

    private Long userId;

    private String username;

    private String geometry;

    @Override
    public String getType() {
        return MessageType.MATCHED_ROUTE_NOTIFICATION.name();
    }

    public Long getDriveRouteId() {
        return driveRouteId;
    }

    public void setDriveRouteId(Long driveRouteId) {
        this.driveRouteId = driveRouteId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getGeometry() {
        return geometry;
    }

    public void setGeometry(String geometry) {
        this.geometry = geometry;
    }
}
