package com.asme.common.dto.authorization;

import java.util.Date;

import com.asme.common.dto.StatusDto;

/**
 * @author Vladyslav Mykhalets
 * @since 07.02.18
 */
public class JwtTokenResponse extends StatusDto {

    private String accessToken;

    private String refreshToken;

    private Date accessTokenExpiryDate;

    private UserInfoResponse userInfo;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public Date getAccessTokenExpiryDate() {
        return accessTokenExpiryDate;
    }

    public void setAccessTokenExpiryDate(Date accessTokenExpiryDate) {
        this.accessTokenExpiryDate = accessTokenExpiryDate;
    }

    public UserInfoResponse getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfoResponse userInfo) {
        this.userInfo = userInfo;
    }
}
