package com.asme.common.dto.messaging.notification;

import com.asme.common.dto.messaging.MessageType;
import com.asme.common.dto.messaging.WebSocketMessage;
import com.asme.common.geo.GeoPoint;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * @author Vladyslav Mykhalets
 * @since 10.04.18
 */
@JsonTypeName("MATCHED_REQUEST_NOTIFICATION")
public class MatchedRequestNotification extends WebSocketMessage {

    private Long driveRequestId;

    private Long userId;

    private String username;

    private GeoPoint startLocation;

    private GeoPoint destination;

    @Override
    public String getType() {
        return MessageType.MATCHED_REQUEST_NOTIFICATION.name();
    }

    public Long getDriveRequestId() {
        return driveRequestId;
    }

    public void setDriveRequestId(Long driveRequestId) {
        this.driveRequestId = driveRequestId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public GeoPoint getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(GeoPoint startLocation) {
        this.startLocation = startLocation;
    }

    public GeoPoint getDestination() {
        return destination;
    }

    public void setDestination(GeoPoint destination) {
        this.destination = destination;
    }
}
