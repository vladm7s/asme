package com.asme.common.dto.messaging.request;

import com.asme.common.dto.messaging.MessageType;
import com.asme.common.dto.messaging.WebSocketMessage;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * @author Vladyslav Mykhalets
 * @since 09.04.18
 */
@JsonTypeName("START_ROUTE")
public class StartRouteMessage extends WebSocketMessage {

    private Long planRouteId;

    @Override
    public String getType() {
        return MessageType.START_ROUTE.name();
    }

    public Long getPlanRouteId() {
        return planRouteId;
    }

    public void setPlanRouteId(Long planRouteId) {
        this.planRouteId = planRouteId;
    }
}
