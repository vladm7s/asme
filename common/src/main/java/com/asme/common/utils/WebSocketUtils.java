package com.asme.common.utils;

import com.asme.common.functional.BiConsumer;
import com.asme.common.functional.Consumer;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;

/**
 * @author Vladyslav Mykhalets
 * @since 10.04.18
 */
public class WebSocketUtils {

    private WebSocketUtils() {
    }

    public static WebSocket openWebSocket(OkHttpClient client, String url, String accessToken,
                                          final Consumer<Response> onOpenConsumer,
                                          final Consumer<String> onMessageConsumer,
                                          final BiConsumer<Throwable, Response> onErrorConsumer,
                                          final Consumer<String> onCloseConsumer) {

        Request.Builder requestBuilder = new Request.Builder();
        if (accessToken != null) {
            requestBuilder = requestBuilder.header("Authorization", "Bearer " + accessToken);
        }
        Request request = requestBuilder.url(url).build();

        return client.newWebSocket(request, new WebSocketListener() {

            @Override
            public void onOpen(WebSocket webSocket, Response response) {
                System.out.println("\nOpened web-socket: " + response.toString() + "\n");

                onOpenConsumer.accept(response);
            }

            @Override
            public void onMessage(WebSocket webSocket, String text) {
                System.out.println("\nMessage received: " + text + "\n");

                onMessageConsumer.accept(text);
            }

            @Override
            public void onFailure(WebSocket webSocket, Throwable t, Response response) {
                System.out.println("\nFailure: " + t.getMessage() + "\n");

                onErrorConsumer.accept(t, response);
            }

            @Override
            public void onClosing(WebSocket webSocket, int code, String reason) {
                super.onClosing(webSocket, code, reason);

                System.out.println("\nClosing: " + code + ", " + reason + "\n");

                onCloseConsumer.accept(reason);
            }
        });
    }

}
