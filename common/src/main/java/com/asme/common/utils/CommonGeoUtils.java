package com.asme.common.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.asme.common.geo.GeoPoint;
import com.asme.common.geo.GeoShapeLine;

/**
 * @author Vladyslav Mykhalets
 */
public class CommonGeoUtils {

    private CommonGeoUtils() {
    }

    public static double getEarthCircumference() {
        return 4.007501668557849E7D / 1.0D;
    }

    public static double getEarthRadius() {
        return 6378137.0D / 1.0D;
    }

    public static double getDistancePerDegree() {
        return 4.007501668557849E7D / (360.0D * 1.0D);
    }

    public static String geoPointsToString(List<GeoPoint> points) {
        StringBuilder coordinatesString = new StringBuilder();

        coordinatesString.append('[');
        for (GeoPoint geoPoint : points) {
            coordinatesString.append("\n").append(geoPoint);
        }
        coordinatesString.append(']');

        return coordinatesString.toString();
    }

    public static String geoPointsToString2(List<BigDecimal[]> points) {
        StringBuilder coordinatesString = new StringBuilder();

        coordinatesString.append('[');
        for (BigDecimal[] geoPoint : points) {
            coordinatesString.append("\n").append(" lon=").append(geoPoint[0]).append(" lat=").append(geoPoint[1]);
        }
        coordinatesString.append(']');

        return coordinatesString.toString();
    }

    /**
     * Decodes an encoded path geometry string into a GeoShapeLine.
     *
     * @param geometry {@link String} representing a path string.
     * @param precision   OSRMv4 uses 6, OSRMv5 and Google uses 5.
     * @return GeoShapeLine - the line to draw on the map.
     * @see <a href="https://github.com/mapbox/polyline/blob/master/src/polyline.js">Part of algorithm came from this source</a>
     * @see <a href="https://github.com/googlemaps/android-maps-utils/blob/master/library/src/com/google/maps/android/PolyUtil.java">Part of algorithm came from this source.</a>
     */
    public static GeoShapeLine fromGeometry(final String geometry, int precision) {
        int len = geometry.length();

        // OSRM uses precision=6, the default Polyline spec divides by 1E5, capping at precision=5
        BigDecimal factor = new BigDecimal("10.0").pow(precision).setScale(6, BigDecimal.ROUND_HALF_UP);

        final List<BigDecimal[]> coordinates = new ArrayList<>();

        // For speed we preallocate to an upper bound on the final length, then
        // truncate the array before returning.
        int index = 0;
        int lat = 0;
        int lng = 0;

        while (index < len) {
            int result = 1;
            int shift = 0;
            int b;
            do {
                b = geometry.charAt(index++) - 63 - 1;
                result += b << shift;
                shift += 5;
            }
            while (b >= 0x1f);
            lat += (result & 1) != 0 ? ~(result >> 1) : (result >> 1);

            result = 1;
            shift = 0;
            do {
                b = geometry.charAt(index++) - 63 - 1;
                result += b << shift;
                shift += 5;
            }
            while (b >= 0x1f);
            lng += (result & 1) != 0 ? ~(result >> 1) : (result >> 1);

            coordinates.add(new BigDecimal[]{
                    new BigDecimal(lng).setScale(6, BigDecimal.ROUND_HALF_UP).divide(factor, BigDecimal.ROUND_HALF_UP),
                    new BigDecimal(lat).setScale(6, BigDecimal.ROUND_HALF_UP).divide(factor, BigDecimal.ROUND_HALF_UP)
            });
        }

        GeoShapeLine geoShapeLine = new GeoShapeLine();
        geoShapeLine.setCoordinates(coordinates);

        return geoShapeLine;
    }
}
