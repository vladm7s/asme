package com.asme.common.exception;

/**
 * @author Vladyslav Mykhalets
 * @since 09.11.17
 */
public class AsmeException extends Exception {

    private int statusCode;

    public AsmeException(int statusCode, String message) {
        super(message);
        this.statusCode = statusCode;
    }

    public AsmeException(String message, Throwable cause) {
        super(message, cause);
    }

    public AsmeException(Throwable cause) {
        super(cause);
    }

    public int getStatusCode() {
        return statusCode;
    }
}
