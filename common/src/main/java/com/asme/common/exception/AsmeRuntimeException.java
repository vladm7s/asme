package com.asme.common.exception;

/**
 * @author Vladyslav Mykhalets
 * @since 19.11.17
 */
public class AsmeRuntimeException extends RuntimeException {

    public AsmeRuntimeException(String message) {
        super(message);
    }

    public AsmeRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }
}
