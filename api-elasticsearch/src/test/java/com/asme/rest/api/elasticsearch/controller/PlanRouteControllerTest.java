package com.asme.rest.api.elasticsearch.controller;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import com.asme.rest.api.elasticsearch.controller.PlanRouteControllerTest.MocksConfig;
import com.asme.rest.api.elasticsearch.controller.PlanRouteControllerTest.TestWebConfig;
import com.asme.rest.api.elasticsearch.transformer.DriveRequestTransformer;
import com.asme.rest.api.elasticsearch.transformer.PlanRouteTransformer;
import com.asme.common.geo.GeoPoint;
import com.asme.core.configuration.AsyncConfiguration;
import com.asme.core.elasticsearch.entity.PlanRoute;
import com.asme.core.elasticsearch.service.DriveRequestService;
import com.asme.core.elasticsearch.service.PlanRouteService;
import com.asme.core.service.osrm.OsrmService;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.reset;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * @author Vladyslav Mykhalets
 * @since 16.10.17
 */
@Ignore
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
        PlanRouteController.class,
        AsyncConfiguration.class,
        PlanRouteTransformer.class,
        DriveRequestTransformer.class,
        TestWebConfig.class,
        MocksConfig.class
})
@WebAppConfiguration
public class PlanRouteControllerTest {

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private PlanRouteService planRouteService;

    @Autowired
    private DriveRequestService driveRequestService;

    private MockMvc mockMvc;

    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.stream(converters)
                .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
                .findAny()
                .orElse(null);

        assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void createPost() throws Exception {

        String lonStart = "30.387588";
        String latStart = "50.452806";

        String lonDestination = "30.448539";
        String latDestination = "50.517392";

        List<GeoPoint> geoPoints = Arrays.asList(new GeoPoint(lonStart, latStart), new GeoPoint(lonDestination, latDestination));

        PlanRoute planRoute = new PlanRoute();
        Pageable pageable = new PageRequest(0, 5);

        reset(planRouteService);
        expect(planRouteService.route(eq(geoPoints))).andReturn(planRoute);
        replay(planRouteService);

        reset(driveRequestService);
        expect(driveRequestService.find(eq(planRoute.getPlanRoute()), eq(pageable))).andReturn(new PageImpl<>(new ArrayList<>()));
        replay(driveRequestService);

        mockMvc.perform(post("/plan/route/").content(json(geoPoints)).contentType(contentType))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.planWaypoints", hasSize(0)));
    }

    @Test
    public void createGet() throws Exception {

        String lonStart = "30.387588";
        String latStart = "50.452806";

        String lonDestination = "30.448539";
        String latDestination = "50.517392";

        List<GeoPoint> geoPoints = Arrays.asList(new GeoPoint(lonStart, latStart), new GeoPoint(lonDestination, latDestination));

        PlanRoute planRoute = new PlanRoute();
        Pageable pageable = new PageRequest(0, 5);

        reset(planRouteService);
        expect(planRouteService.route(eq(geoPoints))).andReturn(planRoute);
        replay(planRouteService);

        reset(driveRequestService);
        expect(driveRequestService.find(eq(planRoute.getPlanRoute()), eq(pageable))).andReturn(new PageImpl<>(new ArrayList<>()));
        replay(driveRequestService);

        mockMvc.perform(get("/plan/route/" + latStart + "/" + lonStart + "/" + latDestination + "/" + lonDestination + "/").contentType(contentType))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.planWaypoints", hasSize(0)));
//                .andExpect(jsonPath("$[0].id", is(this.bookmarkList.get(0).getId().intValue())))
//                .andExpect(jsonPath("$[0].uri", is("http://bookmark.com/1/" + userName)))
//                .andExpect(jsonPath("$[0].description", is("A description")))
//                .andExpect(jsonPath("$[1].id", is(this.bookmarkList.get(1).getId().intValue())))
//                .andExpect(jsonPath("$[1].uri", is("http://bookmark.com/2/" + userName)))
//                .andExpect(jsonPath("$[1].description", is("A description")));

    }

    @Test
    public void createGetAsync() throws Exception {

        String lonStart = "30.387588";
        String latStart = "50.452806";

        String lonDestination = "30.448539";
        String latDestination = "50.517392";

        List<GeoPoint> geoPoints = Arrays.asList(new GeoPoint(lonStart, latStart), new GeoPoint(lonDestination, latDestination));

        PlanRoute planRoute = new PlanRoute();
        planRoute.setPlanWaypoints(Arrays.asList(new GeoPoint("10.0", "20.0")));

        Pageable pageable = new PageRequest(0, 5);

        CompletableFuture<PlanRoute> completableFuture = CompletableFuture.completedFuture(planRoute);

        reset(planRouteService);
        expect(planRouteService.routeAsync(eq(geoPoints))).andReturn(completableFuture);
        replay(planRouteService);

        reset(driveRequestService);
        expect(driveRequestService.find(eq(planRoute.getPlanRoute()), eq(pageable))).andReturn(new PageImpl<>(new ArrayList<>()));
        replay(driveRequestService);

        MvcResult result = mockMvc.perform(get("/plan/route/async/" + latStart + "/" + lonStart + "/" + latDestination + "/" + lonDestination + "/")
                .contentType(contentType)).andReturn();

        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.planWaypoints", hasSize(1)));
//                .andExpect(jsonPath("$[0].id", is(this.bookmarkList.get(0).getId().intValue())))
//                .andExpect(jsonPath("$[0].uri", is("http://bookmark.com/1/" + userName)))
//                .andExpect(jsonPath("$[0].description", is("A description")))
//                .andExpect(jsonPath("$[1].id", is(this.bookmarkList.get(1).getId().intValue())))
//                .andExpect(jsonPath("$[1].uri", is("http://bookmark.com/2/" + userName)))
//                .andExpect(jsonPath("$[1].description", is("A description")));

    }

    private String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        mappingJackson2HttpMessageConverter.write(o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }

    @Configuration
    public static class MocksConfig {

        @Bean
        public PlanRouteService planRouteService() {
            return createMock(PlanRouteService.class);
        }

        @Bean
        public DriveRequestService driveRequestService() {
            return createMock(DriveRequestService.class);
        }

        @Bean
        public OsrmService osrmService() {
            return createMock(OsrmService.class);
        }
    }

    @Configuration
    public static class TestWebConfig extends WebMvcConfigurationSupport {

        @Bean
        public MappingJackson2HttpMessageConverter customJackson2HttpMessageConverter() {
            MappingJackson2HttpMessageConverter jsonConverter = new MappingJackson2HttpMessageConverter();
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            jsonConverter.setObjectMapper(objectMapper);
            return jsonConverter;
        }

        @Override
        public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
            converters.add(customJackson2HttpMessageConverter());
            super.addDefaultHttpMessageConverters(converters);
        }
    }
}
