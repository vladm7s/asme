package com.asme.rest.api.elasticsearch.controller;

import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.asme.common.dto.request.DriveRequestSearchParameters;
import com.asme.core.elasticsearch.entity.DriveRequest;
import com.asme.core.elasticsearch.service.DriveRequestService;

/**
 * @author Vladyslav Mykhalets
 */
@RestController
@RequestMapping(value = "/drive/request", produces = MediaType.APPLICATION_JSON_VALUE)
public class DriveRequestController {

    @Autowired
    private DriveRequestService driveRequestService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public String create(@RequestBody @Valid DriveRequest driveRequest) {
        String uuid = UUID.randomUUID().toString();
        driveRequest.setId(uuid);
        driveRequestService.save(driveRequest);
        return uuid;
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestBody @Valid DriveRequest driveRequest) {
        driveRequestService.save(driveRequest);
    }

    @RequestMapping(value = "/search/{driveRouteId}", method = RequestMethod.GET)
    @ResponseBody
    public Page<DriveRequest> search(@PathVariable String driveRouteId, @PageableDefault Pageable pageable) {
        return driveRequestService.find(driveRouteId, pageable);
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ResponseBody
    public Page<DriveRequest> search(@RequestBody @Valid DriveRequestSearchParameters searchParameters, @PageableDefault Pageable pageable) {
        return driveRequestService.find(searchParameters, pageable);
    }
}
