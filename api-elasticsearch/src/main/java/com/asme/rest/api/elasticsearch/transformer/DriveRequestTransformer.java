package com.asme.rest.api.elasticsearch.transformer;

import org.springframework.stereotype.Component;

import com.asme.common.dto.request.DriveRequestDto;
import com.asme.common.geo.GeoPoint;
import com.asme.core.elasticsearch.entity.DriveRequest;

/**
 * @author Vladyslav Mykhalets
 * @since 03.04.18
 */
@Component
public class DriveRequestTransformer {

    public DriveRequestDto transform(DriveRequest driveRequest) {
        return new DriveRequestDto.Builder()
                .id(driveRequest.getId())
                .userId(driveRequest.getUserId())
                .startLocation(new GeoPoint(driveRequest.getStartLocation().getCoordinates()[0], driveRequest.getStartLocation().getCoordinates()[1]))
                .destination(new GeoPoint(driveRequest.getDestination().getCoordinates()[0], driveRequest.getDestination().getCoordinates()[1]))
                .startLocationName(driveRequest.getStartLocationName())
                .destinationName(driveRequest.getDestinationName())
                .build();
    }

}
