package com.asme.rest.api.elasticsearch.transformer;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.asme.common.dto.request.DriveRequestDto;
import com.asme.common.dto.route.PlanRouteDto;
import com.asme.core.elasticsearch.entity.DriveRequest;
import com.asme.core.elasticsearch.entity.PlanRoute;

/**
 * @author Vladyslav Mykhalets
 * @since 03.04.18
 */
@Component
public class PlanRouteTransformer {

    @Autowired
    private DriveRequestTransformer driveRequestTransformer;

    public PlanRouteDto transform(PlanRoute source, List<DriveRequest> driveRequests) {

        List<DriveRequestDto> driveRequestsDto = driveRequests
                .stream()
                .map(driveRequestTransformer::transform)
                .collect(Collectors.toList());

        return new PlanRouteDto.Builder()
                .id(source.getId())
                .planWaypoints(source.getPlanWaypoints())
                .planGeometry(source.getPlanGeometry())
                .planDistance(source.getPlanDistance())
                .driveRequests(driveRequestsDto)
                .build();
    }
}
