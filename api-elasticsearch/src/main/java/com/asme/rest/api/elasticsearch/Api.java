package com.asme.rest.api.elasticsearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.asme.core.CoreConfiguration;

/**
 * @author Vladyslav Mykhalets
 */
@SpringBootApplication(scanBasePackages = {
        "com.asme.rest.api.elasticsearch"
})
@Import({CoreConfiguration.class})
public class Api {

    public static void main(String[] args) {
        SpringApplication.run(Api.class, args);
    }
}
