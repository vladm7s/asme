package com.asme.rest.api.elasticsearch.controller;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.asme.rest.api.elasticsearch.transformer.PlanRouteTransformer;
import com.asme.common.dto.route.PlanRouteDto;
import com.asme.common.geo.GeoPoint;
import com.asme.core.elasticsearch.entity.DriveRequest;
import com.asme.core.elasticsearch.entity.PlanRoute;
import com.asme.core.elasticsearch.service.DriveRequestService;
import com.asme.core.elasticsearch.service.PlanRouteService;

/**
 * @author Vladyslav Mykhalets
 */
@RestController
@RequestMapping(value = "/plan/route", produces = MediaType.APPLICATION_JSON_VALUE)
public class PlanRouteController {
    private static final Logger log = LoggerFactory.getLogger(PlanRouteController.class);

    @Autowired
    private PlanRouteService planRouteService;

    @Autowired
    private DriveRequestService driveRequestService;

    @Autowired
    private PlanRouteTransformer planRouteTransformer;

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public PlanRouteDto create(@RequestBody List<GeoPoint> waypoints) {
        PlanRoute planRoute = planRouteService.route(waypoints);
        Page<DriveRequest> driveRequests = driveRequestService.find(planRoute.getPlanRoute(), new PageRequest(0, 5));
        return planRouteTransformer.transform(planRoute, driveRequests.getContent());
    }

    @RequestMapping(value = "/{latStart}/{lonStart}/{latDestination}/{lonDestination}/", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public PlanRouteDto create(@PathVariable String latStart, @PathVariable String lonStart,
                               @PathVariable String latDestination, @PathVariable String lonDestination) {

        PlanRoute planRoute = planRouteService.route(Arrays.asList(new GeoPoint(lonStart, latStart), new GeoPoint(lonDestination, latDestination)));
        Page<DriveRequest> driveRequests = driveRequestService.find(planRoute.getPlanRoute(), new PageRequest(0, 5));
        return planRouteTransformer.transform(planRoute, driveRequests.getContent());
    }

    @RequestMapping(value = "/async/{latStart}/{lonStart}/{latDestination}/{lonDestination}/", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public CompletableFuture<PlanRouteDto> createAsync(@PathVariable String latStart, @PathVariable String lonStart,
                                            @PathVariable String latDestination, @PathVariable String lonDestination) {
        log.info("Entering PlanRouteController::createAsync");

        CompletableFuture<PlanRoute> planRouteCompletableFuture = planRouteService.routeAsync(Arrays.asList(new GeoPoint(lonStart, latStart), new GeoPoint(lonDestination, latDestination)));

        CompletableFuture<PlanRouteDto> planRouteDto = planRouteCompletableFuture.thenApply(planRoute -> {
            Page<DriveRequest> driveRequests = driveRequestService.find(planRoute.getPlanRoute(), new PageRequest(0, 5));
            return planRouteTransformer.transform(planRoute, driveRequests.getContent());
        });

        log.info("Leaving PlanRouteController::createAsync");

        return planRouteDto;
    }
}
