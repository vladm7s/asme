package com.asme.rest.api.elasticsearch.controller;

import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.asme.common.dto.route.DriveRouteSearchParameters;
import com.asme.core.elasticsearch.entity.DriveRoute;
import com.asme.core.elasticsearch.service.DriveRouteService;

/**
 * @author Vladyslav Mykhalets
 */
@RestController
@RequestMapping(value = "/drive/route", produces = MediaType.APPLICATION_JSON_VALUE)
public class DriveRouteController {

    @Autowired
    private DriveRouteService driveRouteService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public String create(@RequestBody @Valid DriveRoute driveRoute) {
        String uuid = UUID.randomUUID().toString();
        driveRoute.setId(uuid);
        driveRouteService.save(driveRoute);
        return uuid;
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestBody @Valid DriveRoute driveRoute) {
        driveRouteService.save(driveRoute);
    }

    @RequestMapping(value = "/search/{driveRequestId}", method = RequestMethod.GET)
    @ResponseBody
    public Page<DriveRoute> search(@PathVariable String driveRequestId, @PageableDefault Pageable pageable) {
        return driveRouteService.find(driveRequestId, pageable);
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ResponseBody
    public Page<DriveRoute> search(@RequestBody @Valid DriveRouteSearchParameters searchParameters, @PageableDefault Pageable pageable) {
        return driveRouteService.find(searchParameters, pageable);
    }
}
