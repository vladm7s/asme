package com.asme.authorization.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.asme.authorization.entity.User;
import com.asme.common.dto.authorization.UserSignUpRequest;
import com.asme.common.exception.AsmeException;

/**
 * @author Vladyslav Mykhalets
 * @since 07.02.18
 */
public interface UserService extends UserDetailsService {

    User create(UserSignUpRequest signUpRequest) throws AsmeException;

    User findByUsername(String username);
}
