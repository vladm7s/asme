package com.asme.authorization.service;

import java.util.Date;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.asme.authclient.domain.JwtTokenAuthentication;
import com.asme.authorization.entity.AccessToken;
import com.asme.authorization.entity.RefreshToken;
import com.asme.authorization.entity.Role;
import com.asme.authorization.entity.User;
import com.asme.authorization.repository.AccessTokenRepository;
import com.asme.authorization.repository.RefreshTokenRepository;
import com.asme.common.dto.authorization.JwtTokenResponse;
import com.asme.common.dto.authorization.UserInfoResponse;
import com.asme.common.dto.authorization.UserLoginRequest;
import com.asme.common.dto.authorization.UserSignUpRequest;
import com.asme.common.dto.authorization.token.AccessTokenDto;
import com.asme.common.dto.authorization.token.RefreshTokenDto;
import com.asme.common.exception.AsmeException;
import com.asme.messaging.dto.messaging.token.TokenType;
import com.asme.messaging.service.messaging.token.TokenMessagingService;

import static com.asme.authclient.domain.JwtTokenAuthentication.EMAIL_CLAIM;
import static com.asme.authclient.domain.JwtTokenAuthentication.ROLES_CLAIM;
import static com.asme.authclient.domain.JwtTokenAuthentication.USER_ID_CLAIM;
import static com.google.common.collect.Maps.newHashMap;
import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.exact;
import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.startsWith;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * @author Vladyslav Mykhalets
 * @since 06.02.18
 */
@Service
public class JwtTokenService {

    public static final long EXPIRATION_TIME = 1_800_000; // 30 minutes

    @Autowired
    private UserService userService;

    @Autowired
    private AccessTokenRepository accessTokenRepository;

    @Autowired
    private RefreshTokenRepository refreshTokenRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private TokenMessagingService tokenMessagingService;

    @Transactional
    public JwtTokenResponse refreshAccessToken(String refreshTokenId) {
        RefreshToken refreshToken = refreshTokenRepository.findRefreshTokenByTokenId(refreshTokenId);
        AccessToken accessToken = buildAccessToken(refreshToken.getUser(), refreshToken);

        accessTokenRepository.save(accessToken);

        JwtTokenResponse tokenResponse = new JwtTokenResponse();
        tokenResponse.setAccessToken(accessToken.getTokenContent());
        tokenResponse.setRefreshToken(refreshToken.getTokenContent());
        tokenResponse.setAccessTokenExpiryDate(accessToken.getExpiryDate());
        tokenResponse.setUserInfo(buildUserInfo(refreshToken.getUser()));

        return tokenResponse;
    }

    @Transactional
    public void revokeRefreshToken(String tokenId) {
        refreshTokenRepository.revokeToken(tokenId);
        tokenMessagingService.revokeToken(TokenType.REFRESH_TOKEN, tokenId);

        try (Stream<AccessToken> stream = accessTokenRepository.findAllByRefreshTokenTokenId(tokenId)) {
            stream.forEach(accessToken -> revokeAccessToken(accessToken.getTokenId()));
        }
    }

    @Transactional
    public void revokeAccessToken(String tokenId) {
        accessTokenRepository.revokeToken(tokenId);
        tokenMessagingService.revokeToken(TokenType.ACCESS_TOKEN, tokenId);
    }

    @Transactional
    public void revokeRefreshTokenByAccessToken(String accessTokenId) {
        AccessToken accessToken = accessTokenRepository.findAccessTokenByTokenId(accessTokenId);
        revokeRefreshToken(accessToken.getRefreshToken().getTokenId());
    }

    public Page<String> findRevokedAccessTokens(Pageable pageable) {
        return accessTokenRepository.findAccessTokenTokenIdByRevokedTrueAndExpiryDateIsAfterCurrentDate(pageable);
    }

    public Page<AccessToken> findAccessTokensByCriteria(String refreshTokenId, Map<String, String> searchCriteria, Pageable pageable) {

        ExampleMatcher matcher = ExampleMatcher.matching().withIgnoreNullValues();
        matcher = matcher.withMatcher(AccessTokenDto.REFRESH_TOKEN_ID, exact());

        AccessToken accessToken = new AccessToken();
        RefreshToken refreshToken = new RefreshToken();

        refreshToken.setTokenId(refreshTokenId);
        accessToken.setRefreshToken(refreshToken);

        Example<AccessToken> example = Example.of(accessToken, matcher);
        return accessTokenRepository.findAll(example, pageable);
    }

    public Page<RefreshToken> findRefreshTokensByCriteria(Map<String, String> searchCriteria, Pageable pageable) {

        // https://easyjava.ru/data/jpa/jpa-criteria/
        // https://easyjava.ru/spring/spring-data-project/ispolzovanie-jpa-criteria-v-spring-data-jpa/

        if (searchCriteria.isEmpty()) {
            return refreshTokenRepository.findAll(pageable);
        } else {
            ExampleMatcher matcher = ExampleMatcher.matching().withIgnoreNullValues();
            RefreshToken refreshToken = new RefreshToken();

            if (searchCriteria.containsKey(RefreshTokenDto.USERNAME)) {

                User user = new User();
                user.setUsername(searchCriteria.get(RefreshTokenDto.USERNAME));
                refreshToken.setUser(user);

                matcher = matcher.withMatcher(RefreshTokenDto.USERNAME, startsWith().ignoreCase());
            }

            if (searchCriteria.containsKey(RefreshTokenDto.TOKEN_ID)) {

                refreshToken.setTokenId(searchCriteria.get(RefreshTokenDto.TOKEN_ID));
                matcher = matcher.withMatcher(RefreshTokenDto.TOKEN_ID, startsWith().ignoreCase());
            }

            if (searchCriteria.containsKey(RefreshTokenDto.REVOKED)) {

                refreshToken.setRevoked(Boolean.parseBoolean(searchCriteria.get(RefreshTokenDto.REVOKED)));
                matcher = matcher.withMatcher(RefreshTokenDto.REVOKED, exact());
            }

            Example<RefreshToken> example = Example.of(refreshToken, matcher);
            return refreshTokenRepository.findAll(example, pageable);
        }
    }

    @Transactional
    public JwtTokenResponse login(UserLoginRequest loginRequest) {

        String username = loginRequest.getUsername();

        User user = userService.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User with username '" + username + "' not found");
        }

        boolean passwordMatched = passwordEncoder.matches(loginRequest.getPassword(), user.getPassword());
        if (!passwordMatched) {
            throw new BadCredentialsException("The provided password is incorrect");
        }

        RefreshToken refreshToken = buildRefreshToken(user);
        refreshTokenRepository.save(refreshToken);

        AccessToken accessToken = buildAccessToken(user, refreshToken);
        accessTokenRepository.save(accessToken);

        JwtTokenResponse tokenResponse = new JwtTokenResponse();
        tokenResponse.setAccessToken(accessToken.getTokenContent());
        tokenResponse.setRefreshToken(refreshToken.getTokenContent());
        tokenResponse.setAccessTokenExpiryDate(accessToken.getExpiryDate());
        tokenResponse.setUserInfo(buildUserInfo(user));

        return tokenResponse;
    }

    @Transactional
    public JwtTokenResponse signUp(UserSignUpRequest signUpRequest) throws AsmeException {

        User user = userService.create(signUpRequest);

        RefreshToken refreshToken = buildRefreshToken(user);
        refreshTokenRepository.save(refreshToken);

        AccessToken accessToken = buildAccessToken(user, refreshToken);
        accessTokenRepository.save(accessToken);

        JwtTokenResponse tokenResponse = new JwtTokenResponse();
        tokenResponse.setAccessToken(accessToken.getTokenContent());
        tokenResponse.setRefreshToken(refreshToken.getTokenContent());
        tokenResponse.setAccessTokenExpiryDate(accessToken.getExpiryDate());
        tokenResponse.setUserInfo(buildUserInfo(user));

        return tokenResponse;
    }

    public String getTokenIdFromRefreshTokenContent(String refreshTokenContent) {

        Claims claims = Jwts.parser()
                .setSigningKey(JwtTokenAuthentication.SECRET)
                .parseClaimsJws(refreshTokenContent)
                .getBody();

        return claims.getId();
    }

    private AccessToken buildAccessToken(User user, RefreshToken refreshToken) {

        Map<String, Object> claims = newHashMap();

        claims.put(USER_ID_CLAIM, user.getId().toString());
        claims.put(EMAIL_CLAIM, user.getEmail());
        claims.put(ROLES_CLAIM, user.getRoles().stream().map(Role::getRole).toArray());

        String tokenId = UUID.randomUUID().toString();
        Date expiryDate = new Date(System.currentTimeMillis() + EXPIRATION_TIME);
        String tokenContent = Jwts.builder()
                .setId(tokenId)
                .setSubject(user.getUsername())
                .setExpiration(expiryDate)
                .addClaims(claims)
                .signWith(SignatureAlgorithm.HS512, JwtTokenAuthentication.SECRET)
                .compact();

        AccessToken accessToken = new AccessToken();
        accessToken.setTokenId(tokenId);
        accessToken.setTokenContent(tokenContent);
        accessToken.setExpiryDate(expiryDate);
        accessToken.setRevoked(false);
        accessToken.setUser(user);
        accessToken.setRefreshToken(refreshToken);

        return accessToken;
    }

    private RefreshToken buildRefreshToken(User user) {

        String tokenId = UUID.randomUUID().toString();
        String tokenContent = Jwts.builder()
                .setId(tokenId)
                .setSubject(user.getUsername())
                .signWith(SignatureAlgorithm.HS512, JwtTokenAuthentication.SECRET)
                .compact();

        RefreshToken refreshToken = new RefreshToken();
        refreshToken.setTokenId(tokenId);
        refreshToken.setTokenContent(tokenContent);
        refreshToken.setUser(user);
        refreshToken.setRevoked(false);

        return refreshToken;
    }

    private UserInfoResponse buildUserInfo(User user) {
        UserInfoResponse infoResponse = new UserInfoResponse();
        infoResponse.setUserId(user.getId());
        infoResponse.setUsername(user.getUsername());
        infoResponse.setEmail(user.getEmail());
        return infoResponse;
    }
}
