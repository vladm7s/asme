package com.asme.authorization.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.asme.authorization.entity.Role;
import com.asme.authorization.entity.User;
import com.asme.authorization.repository.RoleRepository;
import com.asme.authorization.repository.UserRepository;
import com.asme.common.dto.authorization.UserSignUpRequest;
import com.asme.common.exception.AsmeException;

import static org.hibernate.validator.internal.util.CollectionHelper.newArrayList;

/**
 * @author Vladyslav Mykhalets
 * @since 30.01.18
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public User create(UserSignUpRequest signUpRequest) throws AsmeException {

        User user = userRepository.findByUsername(signUpRequest.getUsername());
        if (user != null) {
            throw new AsmeException(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                    "User with username '" + signUpRequest.getUsername() + "' already exists");
        }

        user = new User();
        user.setUsername(signUpRequest.getUsername());
        user.setPassword(passwordEncoder.encode(signUpRequest.getPassword()));
        user.setEmail(signUpRequest.getEmail());
        user.setBlacklisted(false);

        List<Role> defaultAssignableRoles = roleRepository.findByDefaultAssignableIsTrue();
        defaultAssignableRoles.forEach(user::addRole);

        return userRepository.save(user);
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User with username '" + username + "' not found");
        }

        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), newArrayList());
    }
}
