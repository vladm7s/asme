package com.asme.authorization.provider;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import com.asme.authclient.provider.RevokedTokenListProvider;
import com.asme.authorization.repository.AccessTokenRepository;

/**
 * @author Vladyslav Mykhalets
 * @since 18.02.18
 */
@Component
@Scope("prototype")
public class RevokedTokenListProviderJpaImpl implements RevokedTokenListProvider {

    private static final int SIZE = 100;

    @Autowired
    private AccessTokenRepository accessTokenRepository;

    private Page<String> currentPage;

    @Override
    public boolean hasNext() {
        int page = (currentPage == null) ? 0 : currentPage.getNumber() + 1;
        currentPage = accessTokenRepository
                .findAccessTokenTokenIdByRevokedTrueAndExpiryDateIsAfterCurrentDate(new PageRequest(page, SIZE));
        return currentPage.getContent().size() > 0;
    }

    @Override
    public List<String> next() {
        if (currentPage == null) {
            throw new IllegalStateException();
        }
        return currentPage.getContent();
    }
}
