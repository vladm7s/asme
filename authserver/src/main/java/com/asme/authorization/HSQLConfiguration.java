package com.asme.authorization;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * @author Vladyslav Mykhalets
 * @since 30.01.18
 */
@Configuration
@EnableJpaAuditing
@EntityScan(basePackages = "com.asme.authorization.entity")
@EnableJpaRepositories(basePackages = "com.asme.authorization.repository")
public class HSQLConfiguration {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @PostConstruct
    public void startDBManager() {

        try {
            jdbcTemplate.execute("INSERT INTO role (id, role, default_assignable) VALUES (1, 'ROLE_USER', true);");
        } catch (Exception e) {

        }

        //hsqldb
        //DatabaseManagerSwing.main(new String[] { "--url", "jdbc:hsqldb:mem:asme", "--user", "asme", "--password", "asme" });

        //derby
        //DatabaseManagerSwing.main(new String[] { "--url", "jdbc:derby:memory:testdb", "--user", "", "--password", "" });

        //h2
        //DatabaseManagerSwing.main(new String[] { "--url", "jdbc:h2:mem:testdb", "--user", "sa", "--password", "" });

    }

//    @Bean
//    public DataSource dataSource() {
//
//        // no need shutdown, EmbeddedDatabaseFactoryBean will take care of this
//        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
//        EmbeddedDatabase db = builder
//                .setType(EmbeddedDatabaseType.HSQL) //.H2 or .DERBY
//                .build();
//        return db;
//    }
}
