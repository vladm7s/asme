package com.asme.authorization;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.asme.authclient.service.RevokedTokenService;
import com.asme.authclient.service.RevokedTokenServiceImpl;
import com.asme.messaging.configuration.MessagingConfiguration;

/**
 * @author Vladyslav Mykhalets
 * @since 30.01.18
 */
@Configuration
@ComponentScan(
        basePackages = {
                "com.asme.authorization.controller",
                "com.asme.authorization.service",
                "com.asme.authorization.provider",
                "com.asme.authorization.transformer"
        }
)
@Import({
        HSQLConfiguration.class, MessagingConfiguration.class
})
public class AuthorizationConfiguration {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public RevokedTokenService revokedTokenService() {
        return new RevokedTokenServiceImpl();
    }
}
