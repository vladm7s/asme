package com.asme.authorization.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.asme.authorization.entity.Role;

/**
 * @author Vladyslav Mykhalets
 * @since 20.03.18
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findByRole(String role);

    List<Role> findByDefaultAssignableIsTrue();
}
