package com.asme.authorization.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.asme.authorization.entity.IdentityProvider;

/**
 * @author Vladyslav Mykhalets
 * @since 06.02.18
 */
@Repository
public interface IdentityProviderRepository extends JpaRepository<IdentityProvider, Long> {
}
