package com.asme.authorization.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.asme.authorization.entity.IdentityProviderAccessInfo;

/**
 * @author Vladyslav Mykhalets
 * @since 06.02.18
 */
public interface IdentityProviderAccessInfoRepository extends JpaRepository<IdentityProviderAccessInfo, Long> {
}
