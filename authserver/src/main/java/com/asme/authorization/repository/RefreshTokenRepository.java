package com.asme.authorization.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.asme.authorization.entity.RefreshToken;

/**
 * @author Vladyslav Mykhalets
 * @since 01.02.18
 */
@Repository
public interface RefreshTokenRepository extends JpaRepository<RefreshToken, Long> {

    @Modifying
    @Query("update RefreshToken token set token.revoked = TRUE where token.tokenId = :tokenId")
    void revokeToken(@Param("tokenId") String tokenId);

    RefreshToken findRefreshTokenByTokenId(String tokenId);
}
