package com.asme.authorization.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.asme.authorization.entity.User;

/**
 * @author Vladyslav Mykhalets
 * @since 30.01.18
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);
}
