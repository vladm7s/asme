package com.asme.authorization.repository;

import java.util.stream.Stream;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.asme.authorization.entity.AccessToken;

/**
 * @author Vladyslav Mykhalets
 * @since 01.02.18
 */
@Repository
public interface AccessTokenRepository extends JpaRepository<AccessToken, Long> {

    @Modifying
    @Query("update AccessToken token set token.revoked = TRUE where token.tokenId = :tokenId")
    void revokeToken(@Param("tokenId") String tokenId);

    Stream<AccessToken> findAllByRefreshTokenTokenId(String tokenId);

    AccessToken findAccessTokenByTokenId(String tokenId);

    @Query(value = "SELECT accessToken.tokenId from AccessToken accessToken WHERE " +
            "accessToken.revoked = TRUE AND accessToken.expiryDate > CURRENT_TIMESTAMP")
    Page<String> findAccessTokenTokenIdByRevokedTrueAndExpiryDateIsAfterCurrentDate(Pageable pageable);
}
