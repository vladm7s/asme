package com.asme.authorization.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * @author Vladyslav Mykhalets
 * @since 20.03.18
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "role", indexes = {
        @Index(name = "role", columnList="role", unique = true)
})
public class Role {

    @Id
    @Column(name = "id")
    @GeneratedValue
    private Long id;

    @Column(name = "role", nullable = false)
    private String role;

    @Column(name = "default_assignable")
    private boolean defaultAssignable;

    public Long getId() {
        return id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public boolean isDefaultAssignable() {
        return defaultAssignable;
    }

    public void setDefaultAssignable(boolean defaultAssignable) {
        this.defaultAssignable = defaultAssignable;
    }
}
