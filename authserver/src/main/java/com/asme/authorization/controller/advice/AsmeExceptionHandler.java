package com.asme.authorization.controller.advice;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.asme.common.dto.StatusDto;
import com.asme.common.exception.AsmeException;
import com.asme.common.exception.AsmeRuntimeException;

/**
 * @author Vladyslav Mykhalets
 * @since 09.11.17
 */
@ControllerAdvice
public class AsmeExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(AuthenticationException.class)
    @ResponseBody
    public ResponseEntity<StatusDto> handleException(AuthenticationException exception) {

        StatusDto status = new StatusDto();
        status.setError(true);
        status.setMessage(exception.getMessage());

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        HttpStatus httpStatus = HttpStatus.FORBIDDEN;
        return new ResponseEntity<>(status, headers, httpStatus);
    }

    @ExceptionHandler(AsmeException.class)
    @ResponseBody
    public ResponseEntity<StatusDto> handleException(AsmeException exception) {
        StatusDto status = new StatusDto();
        status.setError(true);
        status.setMessage(exception.getMessage());

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();

        HttpStatus httpStatus;
        if (exception.getStatusCode() > 0) {
            httpStatus = HttpStatus.valueOf(exception.getStatusCode());
        } else {
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return new ResponseEntity<>(status, headers, httpStatus);
    }

    @ExceptionHandler(AsmeRuntimeException.class)
    @ResponseBody
    public ResponseEntity<StatusDto> handleException(AsmeRuntimeException exception) {
        StatusDto status = new StatusDto();
        status.setError(true);
        status.setMessage(exception.getMessage());

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();

        return new ResponseEntity<>(status, headers, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Override
    protected ResponseEntity handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return handleException(new AsmeException(ex));
    }
}
