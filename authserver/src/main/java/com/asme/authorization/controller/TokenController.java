package com.asme.authorization.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.asme.authorization.entity.AccessToken;
import com.asme.authorization.entity.RefreshToken;
import com.asme.authorization.service.JwtTokenService;
import com.asme.authorization.transformer.AccessTokenTransformer;
import com.asme.authorization.transformer.RefreshTokenTransformer;
import com.asme.common.dto.authorization.JwtTokenResponse;
import com.asme.common.dto.authorization.token.AccessTokenDto;
import com.asme.common.dto.authorization.token.RefreshAccessTokenRequest;
import com.asme.common.dto.authorization.token.RefreshTokenDto;
import com.google.common.collect.Maps;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @author Vladyslav Mykhalets
 * @since 10.02.18
 */
@RestController
@RequestMapping(value = "/token", produces = {APPLICATION_JSON_VALUE})
public class TokenController {

    @Autowired
    private JwtTokenService jwtTokenService;

    @Autowired
    private RefreshTokenTransformer refreshTokenTransformer;

    @Autowired
    private AccessTokenTransformer accessTokenTransformer;

    @GetMapping
    public Page<RefreshTokenDto> getTokens(@RequestParam Map<String, String> searchParameters, @PageableDefault Pageable pageable) {
        Map<String, String> searchCriteria = searchCriteriaFromParameters(searchParameters);
        Page<RefreshToken> refreshTokenPage = jwtTokenService.findRefreshTokensByCriteria(searchCriteria, pageable);
        return refreshTokenTransformer.transform(refreshTokenPage, pageable);
    }

    @GetMapping(value = "/revoked")
    public Page<String> getRevokedTokens(@PageableDefault Pageable pageable) {
        Page<String> refreshTokenPage = jwtTokenService.findRevokedAccessTokens(pageable);
        return refreshTokenPage;
    }

    @GetMapping(value = "/{refreshTokenId}/access_token")
    public Page<AccessTokenDto> getTokens(@PathVariable String refreshTokenId,
                                          @RequestParam Map<String, String> searchParameters,
                                          @PageableDefault Pageable pageable) {
        Map<String, String> searchCriteria = searchCriteriaFromParameters(searchParameters);
        Page<AccessToken> accessTokenPage = jwtTokenService.findAccessTokensByCriteria(refreshTokenId, searchCriteria, pageable);
        return accessTokenTransformer.transform(accessTokenPage, pageable);
    }

    @PostMapping(value = "/refresh")
    @ResponseStatus(OK)
    public JwtTokenResponse refreshAccessToken(@RequestBody RefreshAccessTokenRequest refreshAccessTokenRequest) {
        String refreshTokenId = jwtTokenService.getTokenIdFromRefreshTokenContent(refreshAccessTokenRequest.getRefreshTokenContent());
        return jwtTokenService.refreshAccessToken(refreshTokenId);
    }

    @PutMapping(value = "/{refreshTokenId}/revoke")
    @ResponseStatus(OK)
    public void revokeRefreshToken(@PathVariable String refreshTokenId) {
        jwtTokenService.revokeRefreshToken(refreshTokenId);
    }

    @PutMapping(value = "/{refreshTokenId}/access_token/{accessTokenId}/revoke")
    @ResponseStatus(OK)
    public void revokeAccessToken(@PathVariable String refreshTokenId, @PathVariable String accessTokenId) {
        jwtTokenService.revokeAccessToken(accessTokenId);
    }

    private Map<String, String> searchCriteriaFromParameters(Map<String, String> searchParameters) {
        return Maps.filterKeys(searchParameters, paramName ->
                !"page".equals(paramName)
                        && !"pageSize".equals(paramName)
                        && !"sort".equals(paramName));
    }
}
