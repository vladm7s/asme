package com.asme.authorization.controller;

import java.security.Principal;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.asme.authclient.domain.JwtTokenAuthentication;
import com.asme.authorization.service.JwtTokenService;
import com.asme.common.dto.authorization.JwtTokenResponse;
import com.asme.common.dto.authorization.UserInfoResponse;
import com.asme.common.dto.authorization.UserLoginRequest;
import com.asme.common.dto.authorization.UserSignUpRequest;
import com.asme.common.exception.AsmeException;

import static com.asme.common.dto.authorization.UserSignUpRequest.EMAIL_PARAM;
import static com.asme.common.dto.authorization.UserSignUpRequest.PASS_PARAM;
import static com.asme.common.dto.authorization.UserSignUpRequest.USERNAME_PARAM;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED_VALUE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @author Vladyslav Mykhalets
 * @since 30.01.18
 */
@RestController
@RequestMapping(value = "/user", produces = {APPLICATION_JSON_VALUE})
public class UserController {

    @Autowired
    private JwtTokenService jwtTokenService;

    @RequestMapping({"/me"})
    public UserInfoResponse user(Principal principal) {
        JwtTokenAuthentication authentication = (JwtTokenAuthentication) principal;

        UserInfoResponse infoResponse = new UserInfoResponse();
        infoResponse.setUserId(authentication.getUserId());
        infoResponse.setUsername(authentication.getName());
        infoResponse.setEmail(authentication.getEmail());
        return infoResponse;
    }

    @PostMapping("/login")
    public JwtTokenResponse login(@RequestBody UserLoginRequest loginRequest) {
        return jwtTokenService.login(loginRequest);
    }

    @PostMapping("/logout")
    public void logout(Principal principal) {
        JwtTokenAuthentication authentication = (JwtTokenAuthentication) principal;
        jwtTokenService.revokeRefreshTokenByAccessToken(authentication.getTokenId());
    }

    @PostMapping(value = "/login", consumes = APPLICATION_FORM_URLENCODED_VALUE)
    public JwtTokenResponse login(@RequestParam Map<String, String> body) throws AsmeException {
        UserLoginRequest loginRequest = new UserLoginRequest();

        loginRequest.setUsername(body.get(USERNAME_PARAM));
        loginRequest.setPassword(body.get(PASS_PARAM));

        return jwtTokenService.login(loginRequest);
    }

    @PostMapping(value = "/sign-up", consumes = APPLICATION_JSON_VALUE)
    public JwtTokenResponse signUp(@RequestBody UserSignUpRequest signUpRequest) throws AsmeException {
        return jwtTokenService.signUp(signUpRequest);
    }

    @PostMapping(value = "/sign-up", consumes = APPLICATION_FORM_URLENCODED_VALUE)
    public JwtTokenResponse signUp(@RequestParam Map<String, String> body) throws AsmeException {
        UserSignUpRequest signUpRequest = new UserSignUpRequest();

        signUpRequest.setUsername(body.get(USERNAME_PARAM));
        signUpRequest.setPassword(body.get(PASS_PARAM));
        signUpRequest.setEmail(body.get(EMAIL_PARAM));

        return jwtTokenService.signUp(signUpRequest);
    }
}
