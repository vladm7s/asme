package com.asme.authorization.transformer;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.asme.authorization.entity.RefreshToken;
import com.asme.common.dto.authorization.token.RefreshTokenDto;

/**
 * @author Vladyslav Mykhalets
 * @since 11.02.18
 */
@Component
public class RefreshTokenTransformer {

    public RefreshTokenDto transform(RefreshToken refreshToken) {
        RefreshTokenDto refreshTokenDto = new RefreshTokenDto();

        refreshTokenDto.setTokenId(refreshToken.getTokenId());
        refreshTokenDto.setUsername(refreshToken.getUser().getUsername());
        refreshTokenDto.setIssueDate(refreshToken.getIssueDate());
        refreshTokenDto.setRevoked(refreshToken.isRevoked());

        return refreshTokenDto;
    }

    public Page<RefreshTokenDto> transform(Page<RefreshToken> refreshTokens, Pageable pageable) {

        List<RefreshTokenDto> refreshTokenDtoList = refreshTokens.getContent()
                .stream()
                .map(this::transform)
                .collect(Collectors.toList());

        return new PageImpl<>(refreshTokenDtoList, pageable, refreshTokens.getTotalElements());
    }
}
