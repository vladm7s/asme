package com.asme.authorization.transformer;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.asme.authorization.entity.AccessToken;
import com.asme.common.dto.authorization.token.AccessTokenDto;

/**
 * @author Vladyslav Mykhalets
 * @since 11.02.18
 */
@Component
public class AccessTokenTransformer {

    public AccessTokenDto transform(AccessToken accessToken) {
        AccessTokenDto accessTokenDto = new AccessTokenDto();

        accessTokenDto.setTokenId(accessToken.getTokenId());
        accessTokenDto.setRefreshTokenId(accessToken.getRefreshToken().getTokenId());
        accessTokenDto.setUsername(accessToken.getUser().getUsername());
        accessTokenDto.setExpiryDate(accessToken.getExpiryDate());
        accessTokenDto.setRevoked(accessToken.isRevoked());

        return accessTokenDto;
    }

    public Page<AccessTokenDto> transform(Page<AccessToken> accessTokens, Pageable pageable) {

        List<AccessTokenDto> accessTokenDtoList = accessTokens.getContent()
                .stream()
                .map(this::transform)
                .collect(Collectors.toList());

        return new PageImpl<>(accessTokenDtoList, pageable, accessTokens.getTotalElements());
    }
}
