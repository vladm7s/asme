appServices.service('GlobalService', function () {

    this.transformErrorMessage = function transformErrorMessage(response) {
        var alert = {};

        var errorData;
        if (response.error) {
            errorData = response.error.data;
        } else {
            errorData = response.data;
        }

        alert.title = errorData.title;
        alert.status = errorData.status;
        if (errorData.errors) {
            alert.errorMessages = [];
            angular.forEach(errorData.errors, function (error, key) {
                var errorMessage = error.field + ': ' + error.message + '; ';
                alert.errorMessages.push(errorMessage);
            });
        }
        alert.detail = errorData.detail;
        return alert;
    };

    this.insertPageIntoQuery = function insertPageIntoQuery(query, page, pageSize) {
        var pagination = '?page=' + page + '&size=' + pageSize;

        if (query && query.length > 0) {
            return pagination + '&' + query;
        } else {
            return pagination;
        }
    }
});