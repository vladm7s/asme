appDirectives.directive('asmeFilterSort', function ($document, $window, $timeout) {
    return {
        restrict: 'E',
        scope: {
            br: "=",
            sort: "=",
            filters: '=',
            dateRange: '=',
            dateTimeRange: '=',
            numericRange: '=',
            multiSelectOptions: "="
        },
        templateUrl: 'view/directive/filter-sort.html',

        link: function (scope, element, attrs) {
            scope.field = attrs.field;
            scope.multiSelectOptionsLabel = attrs.multiSelectOptionsLabel ? attrs.multiSelectOptionsLabel : 'label';

            scope.useFilterSelectOptions = attrs.useFilterSelectOptions ? true : false;
            scope.filterSelectOptions = [{alias: "NULL"}, {alias: "EXISTS"}];

            scope.refreshFilterSelectOptionSearch = function ($select) {
                var search = $select.search;
                var list = angular.copy(scope.filterSelectOptions);

                if (!search) {
                    $select.items = list;
                } else {
                    $select.items = [search].concat(list);
                    $select.selected = search;
                }
            };

            scope.clearSelectedFilterValue = function ($event, $select) {
                $event.stopPropagation();
                $select.selected = undefined;
                $select.search = undefined;
                $select.activate();
            };

            if (scope.numericRange) {
                scope.filters[scope.field] = {};

                if (scope.numericRange.min != null) {
                    scope.filters[scope.field].numericRange = {};
                    scope.filters[scope.field].numericRange.min = Number.NaN;
                }
                if (scope.numericRange.max != null) {
                    if (!scope.filters[scope.field].numericRange) {
                        scope.filters[scope.field].numericRange = {};
                    }
                    scope.filters[scope.field].numericRange.max = Number.NaN;
                }
            } else
            if (scope.dateRange && !scope.dateRange.disabled) {
                scope.filters[scope.field] = {};
                scope.filters[scope.field].dateRange = {
                    startDate: null,
                    endDate: null
                };

                scope.opts = {locale: {format: 'YYYY-MM-DD'}, autoUpdateInput: false};
                if (scope.dateRange.ranges) {
                    scope.opts.ranges = scope.dateRange.ranges;
                } else {
                    scope.opts.ranges = {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 days': [moment().subtract(7, 'days'), moment()],
                        'Last 30 days': [moment().subtract(30, 'days'), moment()],
                        'This month': [moment().startOf('month'), moment().endOf('month')],
                        'All': [moment('2000-01-01'), moment('2100-01-01')]
                    }
                }
            } else
            if (scope.dateTimeRange && !scope.dateTimeRange.disabled) {
                scope.filters[scope.field] = {};
                scope.filters[scope.field].dateTimeRange = {
                    startDate: null,
                    endDate: null
                };

                scope.opts = {locale: {format: 'YYYY-MM-DD'}, autoUpdateInput: false};
                if (scope.dateTimeRange.ranges) {
                    scope.opts.ranges = scope.dateTimeRange.ranges;
                } else {
                    scope.opts.ranges = {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 days': [moment().subtract(7, 'days'), moment()],
                        'Last 30 days': [moment().subtract(30, 'days'), moment()],
                        'This month': [moment().startOf('month'), moment().endOf('month')],
                        'All': [moment('2000-01-01'), moment('2100-01-01')]
                    }
                }
            }
        }
    };
});