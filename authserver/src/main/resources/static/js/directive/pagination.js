appDirectives.directive('asmePagination', function ($document, $window, $timeout, GlobalService) {
    return {
        restrict: 'E',
        scope: {
            data: '=',
            model: '=',
            query: '=',
            transformer: '=',
            refreshRegistration: '=',
            beforeRefresh: '=',
            afterRefresh: '=',
            operationResult: '='
        },
        templateUrl: 'view/directive/pagination.html',

        link: function (scope, element, attrs) {
            scope.ready = false;
            
            scope.perPages = [5, 10, 20, 50, 100, 200, 500];

            scope.info = {
                chosenPage: 1,
                totalPages: 0,
                totalElements: 0,
                query: scope.query,
                perPage: 5,
                go2page: false,
                isPaginated: false,
                style: {
                    leftArrowStyle: 'previous disabled',
                    rightArrowStyle: 'next'
                }
            };

            scope.changePagination = function changePagination() {
                scope.refresh();
            };

            scope.doRefresh = function doRefresh() {
                var info = scope.info;

                return scope.model.find(info.query, info.chosenPage - 1, info.perPage)
                    .then(function (promise) {
                        var data = promise.data;

                        if (data.totalPages < info.chosenPage && data.totalPages > 0) {
                            info.totalPages = data.totalPages;
                            info.totalElements = data.totalElements;

                            scope.goToPage(1);
                        } else {
                            info.totalPages = data.totalPages;
                            info.totalElements = data.totalElements;

                            var oldData = angular.copy(scope.data);

                            scope.data.length = 0;
                            angular.forEach(data.content, function (item) {
                                if (scope.transformer) {
                                    scope.data.push(scope.transformer(item));
                                } else {
                                    scope.data.push(item);
                                }
                            });

                            scope.ready = true;
                            info.isPaginated = info.perPage < info.totalElements ? true : false;
                            _set_arrows_style(info);

                            if (scope.afterRefresh) {
                                scope.afterRefresh(oldData, scope.data);
                            }
                        }
                    }, function (error) {
                        if (scope.operationResult) {
                            scope.operationResult.error = GlobalService.transformErrorMessage(error);
                        }
                    });
            };

            scope.refresh = function refresh() {
                if (scope.beforeRefresh) {
                    scope.beforeRefresh().then(function () {
                        scope.doRefresh();
                    });
                } else {
                    scope.doRefresh();
                }
            };

            scope.refreshRegistration(scope.refresh);

            scope.nextPage = function nextPage() {
                var info = scope.info;

                if (_check_next_limit(info)) {
                    info.chosenPage += 1;
                    scope.refresh();
                }
            };

            scope.previousPage = function previousPage() {
                var info = scope.info;

                if (_check_previous_limit(info)) {
                    info.chosenPage -= 1;
                    scope.refresh();
                }
            };

            scope.goToPage = function goToPage(page) {
                var info = scope.info;

                if (_check_page(info, page)) {
                    info.chosenPage = parseInt(page);
                    scope.refresh();
                }
            };

            function _set_arrows_style(info) {
                if (info.chosenPage == 1) {
                    info.style = {leftArrowStyle: 'previous disabled', rightArrowStyle: 'next'}
                }
                else if (info.chosenPage == info.totalPages) {
                    info.style = {leftArrowStyle: 'previous', rightArrowStyle: 'next disabled'}
                }
                else {
                    info.style = {leftArrowStyle: 'previous', rightArrowStyle: 'next'}
                }
            }

            function _check_page(info, page) {
                if (isNaN(page)) {
                    return false;
                }
                return parseInt(page) > 0 && parseInt(page) <= info.totalPages;
            }

            function _check_previous_limit(info) {
                return info.chosenPage > 1;
            }

            function _check_next_limit(info) {
                return info.chosenPage < info.totalPages
            }
        }
    };
});