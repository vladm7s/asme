angular.module('AsmeApp', [
    'Asme.services',
    'Asme.filters',
    'Asme.directives',
    'Asme.controllers'
]);

var appDirectives = angular.module('Asme.directives', []);
var appControllers = angular.module('Asme.controllers', []);
var appServices = angular.module('Asme.services', []);
var appFilters = angular.module('Asme.filters', []);