appFilters.filter('toDate', function () {
    return function (timestamp) {
        return timestamp ? moment.unix(timestamp / 1000).format("YYYY-MM-DD HH:mm:ss") : '';
    }
});
