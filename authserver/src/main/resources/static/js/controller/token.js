appControllers.controller('TokenController', function ($scope, $http, RefreshTokenModel, AccessTokenModel) {

    $scope.tokens = [];
    $scope.accessTokens = [];
    $scope.filters = {};
    $scope.accessTokenFilters = {};
    $scope.dataModel = RefreshTokenModel;

    $scope.refreshToken = null;
    $scope.accessTokenModel = {
        find: function (search, page, pageSize) {
            return AccessTokenModel.find($scope.refreshToken.tokenId, search, page, pageSize);
        }
    };

    $scope.searchQuery = {
        query: ''
    };
    $scope.accessTokenSearchQuery = {
        query: ''
    };

    $scope.init = function () {
    };

    $scope.refreshRegistration = function (refresh) {
        if (refresh) {
            delete $scope.refreshRegistration;
            $scope.refresh = refresh;
            $scope.refresh();
        }
    };

    $scope.accessTokenRefreshRegistration = function (refresh) {
        if (refresh) {
            delete $scope.accessTokenRefreshRegistration;
            $scope.accessTokenSearchRefresh = refresh;
        }
    };

    $scope.toToken = function (refreshToken) {
        $scope.refreshToken = refreshToken;
        $scope.accessTokenFilters = {};
        $scope.accessTokenSearchRefresh();
    };

    $scope.revokeRefreshToken = function(refreshToken) {
        RefreshTokenModel.revoke(refreshToken.tokenId).then(function () {
            refreshToken.revoked = true;
        });
    };

    $scope.revokeAccessToken = function(accessToken) {
        AccessTokenModel.revoke(accessToken.refreshTokenId, accessToken.tokenId).then(function () {
            accessToken.revoked = true;
        });
    };

    $scope.$watch('filters', function (newValue, oldValue) {
        if (newValue !== oldValue) {

            var query = '';
            $scope.accessTokens = [];

            angular.forEach(newValue, function (value, key) {
                if (value.value && value.value !== '') {
                    if (query) {
                        query = query + '&' + key + '=' + value.value;
                    } else {
                        query = key + '=' + value.value;
                    }
                }
                if (value.sort) {
                    if (query) {
                        query = query + '&sort=' + key + ',' + value.sort;
                    } else {
                        query = 'sort=' + key + ',' + value.sort;
                    }
                }
            });
            $scope.searchQuery.query = query;
            $scope.refresh();
        }
    }, true);

    $scope.$watch('accessTokenFilters', function (newValue, oldValue) {
        if (newValue !== oldValue) {
            var query = '';
            angular.forEach(newValue, function (value, key) {
                if (value.value && value.value !== '') {
                    if (query) {
                        query = query + '&' + key + '=' + value.value;
                    } else {
                        query = key + '=' + value.value;
                    }
                }
                if (value.sort) {
                    if (query) {
                        query = query + '&sort=' + key + ',' + value.sort;
                    } else {
                        query = 'sort=' + key + ',' + value.sort;
                    }
                }
            });
            $scope.accessTokenSearchQuery.query = query;
            $scope.accessTokenSearchRefresh();
        }
    }, true);
});