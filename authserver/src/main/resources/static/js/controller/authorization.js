appControllers.controller('AuthorizationController', function ($scope, $http) {

    $scope.credentials = {};

    $scope.init = function () {

        if ($scope.accessToken) {
            var request = {
                method: 'GET',
                url: '/user/me',
                headers: {
                    'Authorization': 'Bearer ' + $scope.accessToken
                }
            };

            $http(request).then(function (response) {
                if (response.data.username) {
                    $scope.user = response.data.username;
                }
            }, function (error) {
                $scope.error = error.data.message;
                $scope.user = null;
            });
        }
    };

    $scope.login = function () {

        $http.post('/user/login', $scope.credentials).then(function(response) {

            if (response.status === 200) {
                $scope.accessToken = response.data.accessToken;
                $scope.refreshToken = response.data.refreshToken;
            }

            $scope.init();
            $scope.credentials = {};
            $scope.error = null;

        }, function (error) {
            $scope.error = error.data.message;
            $scope.credentials = {};
        });
    };

    $scope.logout = function () {
        $http.post('/user/logout').then(function(response) {

            $scope.init();

        }, function (error) {
            $scope.error = error.data.message;
        });
    };

});