appServices.service('AccessTokenModel', function ($rootScope, $http, GlobalService) {

    var basePath = '/token/{refreshTokenId}/access_token';

    this.findByRefreshTokenId = function (refreshTokenId) {
        return {
            find: function (search, page, pageSize) {
                return _find(refreshTokenId, search, page, pageSize);
            }
        }
    };

    function path(refreshTokenId) {
        return basePath.replace('{refreshTokenId}', refreshTokenId);
    }

    this.find = function (refreshTokenId, search, page, pageSize) {
        var searchQuery = GlobalService.insertPageIntoQuery(search.query, page, pageSize);

        return $http.get(path(refreshTokenId) + searchQuery).then(function (promise) {
            return promise;
        });
    };

    this.revoke = function(refreshTokenId, accessTokenId) {
        return $http.put(path(refreshTokenId) + "/" + accessTokenId + "/revoke").then(function (promise) {
            return promise;
        });
    };
});