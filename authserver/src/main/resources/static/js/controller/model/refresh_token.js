appServices.service('RefreshTokenModel', function ($rootScope, $http, GlobalService) {

    var basePath = '/token';

    this.find = function(search, page, pageSize) {
        var searchQuery = GlobalService.insertPageIntoQuery(search.query, page, pageSize);

        return $http.get(basePath + searchQuery).then(function (promise) {
            return promise;
        });
    };

    this.revoke = function(tokenId) {
        return $http.put(basePath + "/" + tokenId + "/revoke").then(function (promise) {
            return promise;
        });
    };
});