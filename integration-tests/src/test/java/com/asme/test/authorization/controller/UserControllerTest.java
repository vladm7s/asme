package com.asme.test.authorization.controller;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.asme.common.dto.authorization.JwtTokenResponse;
import com.asme.common.dto.authorization.UserInfoResponse;
import com.asme.test.core.configuration.TestDockerServiceConfiguration;
import com.asme.test.core.properties.DockerServiceProperties;
import com.fasterxml.jackson.databind.ObjectMapper;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * @author Vladyslav Mykhalets
 * @since 09.02.18
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {
        TestDockerServiceConfiguration.class
})
@EnableConfigurationProperties(value = {DockerServiceProperties.class})
@PropertySource("authorization/userController.properties")
public class UserControllerTest {

    private static ObjectMapper mapper = new ObjectMapper();

    private static final String BASE_URL = "http://authserver:8084/user";

    //private static final String BASE_URL = "http://localhost:8080/user";

    @Test
    public void userMeRestrictedAccessTest() {

        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(30,  TimeUnit.SECONDS)
                .build();

        String userInfoUrl = BASE_URL + "/me";

        Request userInfoRequest = new Request.Builder()
                .url(userInfoUrl)
                .header("Content-Type", "application/json; charset=utf-8")
                .get()
                .build();

        try (Response response = client.newCall(userInfoRequest).execute()) {
            System.err.println("\n" + response.body().string() + "\n");

            assertThat(response.code(), is(401));
        } catch (IOException io) {
            io.printStackTrace(System.err);
        }
    }

    @Test
    public void loginUserNotFoundTest() {

        String username = "user-" + UUID.randomUUID().toString();
        String password = "password-" + UUID.randomUUID().toString();

        // Login non-existing user
        JwtTokenResponse tokenResponse = login(username, password);

        assertThat(tokenResponse, notNullValue());
        assertThat(tokenResponse.isError(), is(true));
        assertThat(tokenResponse.getMessage(), is("User with username '" + username + "' not found"));
    }

    @Test
    public void loginUserIncorrectPassword() {

        String username = "user-" + UUID.randomUUID().toString();
        String password = "password-" + UUID.randomUUID().toString();
        String email = String.format("email-%s@test.com", UUID.randomUUID().toString());

        // Sign-up user
        JwtTokenResponse tokenResponse = signUp(username, email, password);

        assertThat(tokenResponse, notNullValue());
        assertThat(tokenResponse.isError(), is(false));

        // Login user with correct password
        tokenResponse = login(username, password);

        assertThat(tokenResponse, notNullValue());
        assertThat(tokenResponse.isError(), is(false));

        // Login user with wrong password
        tokenResponse = login(username, UUID.randomUUID().toString());

        assertThat(tokenResponse, notNullValue());
        assertThat(tokenResponse.isError(), is(true));
        assertThat(tokenResponse.getMessage(), is("The provided password is incorrect"));
    }

    @Test
    public void signUpUserAlreadyExistsTest() {

        String username = "user-" + UUID.randomUUID().toString();
        String password = "password-" + UUID.randomUUID().toString();
        String email = String.format("email-%s@test.com", UUID.randomUUID().toString());

        // Sign-up user
        JwtTokenResponse tokenResponse = signUp(username, email, password);

        assertThat(tokenResponse, notNullValue());
        assertThat(tokenResponse.isError(), is(false));

        // Sign-up user again
        tokenResponse = signUp(username, email, password);

        assertThat(tokenResponse, notNullValue());
        assertThat(tokenResponse.isError(), is(true));
        assertThat(tokenResponse.getMessage(), is("User with username '" + username + "' already exists"));
    }

    @Test
    public void signUpTest() {

        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(30,  TimeUnit.SECONDS)
                .build();

        // 1. Sign-up user
        String username = "user-" + UUID.randomUUID().toString();
        String password = "password-" + UUID.randomUUID().toString();
        String email = String.format("email-%s@test.com", UUID.randomUUID().toString());

        JwtTokenResponse tokenResponse = signUp(username, email, password);

        assertThat(tokenResponse, notNullValue());
        assertThat(tokenResponse.getAccessToken(), notNullValue());
        assertThat(tokenResponse.getRefreshToken(), notNullValue());

        // 2. Get info about user
        UserInfoResponse infoResponse = aboutMe(tokenResponse.getAccessToken());

        assertThat(infoResponse, notNullValue());
        assertThat(infoResponse.getUsername(), is(username));
    }

    public static JwtTokenResponse signUp(String username, String email, String password) {

        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(30,  TimeUnit.SECONDS)
                .build();

        RequestBody body = new FormBody.Builder()
                .add("username", username)
                .add("password", password)
                .add("email", email)
                .build();

        String signUpUrl = BASE_URL + "/sign-up";
        Request signUpRequest = new Request.Builder()
                .url(signUpUrl)
                .post(body)
                .build();

        JwtTokenResponse tokenResponse = null;
        try (Response response = client.newCall(signUpRequest).execute()) {

            String responseBody = response.body().string();
            System.out.println("\nResponse received:\n" + responseBody + "\n");

            tokenResponse = mapper.readValue(responseBody, JwtTokenResponse.class);

        } catch (IOException io) {
            io.printStackTrace(System.err);
        }

        return tokenResponse;
    }

    public static UserInfoResponse aboutMe(String accessToken) {

        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(30,  TimeUnit.SECONDS)
                .build();

        String userInfoUrl = BASE_URL + "/me";
        Request userInfoRequest = new Request.Builder()
                .url(userInfoUrl)
                .header("Content-Type", "application/json; charset=utf-8")
                .header("Authorization", "Bearer " + accessToken)
                .get()
                .build();

        UserInfoResponse infoResponse = null;
        try (Response response = client.newCall(userInfoRequest).execute()) {
            if (response.code() == 200) {

                infoResponse = mapper.readValue(response.body().string(), UserInfoResponse.class);
            } else {
                System.err.println("\n" + response.body().string() + "\n");
            }
        } catch (IOException io) {
            io.printStackTrace(System.err);
        }

        return infoResponse;
    }

    public static JwtTokenResponse login(String username, String password) {

        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(30,  TimeUnit.SECONDS)
                .build();

        RequestBody body = new FormBody.Builder()
                .add("username", username)
                .add("password", password)
                .build();

        String loginUrl = BASE_URL + "/login";
        Request loginRequest = new Request.Builder()
                .url(loginUrl)
                .post(body)
                .build();

        JwtTokenResponse tokenResponse = null;
        try (Response response = client.newCall(loginRequest).execute()) {

            String responseBody = response.body().string();
            System.out.println("\nResponse received:\n" + responseBody + "\n");

            tokenResponse = mapper.readValue(responseBody, JwtTokenResponse.class);

        } catch (IOException io) {
            io.printStackTrace(System.err);
        }

        return tokenResponse;
    }
}
