package com.asme.test.authorization.controller;

import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Page;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.asme.authclient.domain.PageWrapper;
import com.asme.common.dto.authorization.JwtTokenResponse;
import com.asme.common.dto.authorization.token.AccessTokenDto;
import com.asme.common.dto.authorization.token.RefreshTokenDto;
import com.asme.test.core.configuration.TestDockerServiceConfiguration;
import com.asme.test.core.properties.DockerServiceProperties;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * @author Vladyslav Mykhalets
 * @since 22.02.18
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {
        TestDockerServiceConfiguration.class
})
@EnableConfigurationProperties(value = {DockerServiceProperties.class})
@PropertySource("authorization/tokenController.properties")
public class TokenControllerTest {

    private static final String BASE_URL = "http://authserver:8084/token";

    private static ObjectMapper mapper = new ObjectMapper();

    //private static final String BASE_URL = "http://localhost:8080/token";

    @Test
    public void revokeTokenTest() {

        // 1. Sign-up user
        String username = "user-" + UUID.randomUUID().toString();
        String password = "password-" + UUID.randomUUID().toString();
        String email = String.format("email-%s@test.com", UUID.randomUUID().toString());

        JwtTokenResponse tokenResponse = UserControllerTest.signUp(username, email, password);

        assertThat(tokenResponse, notNullValue());
        assertThat(tokenResponse.getAccessToken(), notNullValue());
        assertThat(tokenResponse.getRefreshToken(), notNullValue());

        // 2. Find active refresh tokens
        List<RefreshTokenDto> tokenList = findRefreshTokens(username, false);

        assertThat(tokenList, notNullValue());
        assertThat(tokenList.isEmpty(), is(false));
        assertThat(tokenList.size(), is(1));
        assertThat(tokenList.get(0).getUsername(), equalTo(username));
        assertThat(tokenList.get(0).isRevoked(), is(false));

        // 2. Find active access tokens
        String refreshTokenId = tokenList.get(0).getTokenId();
        List<AccessTokenDto> accessTokenList = findAccessTokens(refreshTokenId);

        assertThat(accessTokenList, notNullValue());
        assertThat(accessTokenList.isEmpty(), is(false));

        accessTokenList.forEach(token -> {
            assertThat(token.getUsername(), equalTo(username));
            assertThat(token.isRevoked(), is(false));
        });

        // 3. Revoke refresh token
        boolean result = revokeRefreshToken(refreshTokenId);
        assertThat(result, is(true));

        // 4. Find revoked refresh tokens
        tokenList = findRefreshTokens(username, true);

        assertThat(tokenList, notNullValue());
        assertThat(tokenList.isEmpty(), is(false));
        assertThat(tokenList.size(), is(1));
        assertThat(tokenList.get(0).getUsername(), equalTo(username));
        assertThat(tokenList.get(0).isRevoked(), is(true));

        // 5. Find revoked access tokens
        accessTokenList = findAccessTokens(refreshTokenId);

        assertThat(accessTokenList, notNullValue());
        assertThat(accessTokenList.isEmpty(), is(false));

        accessTokenList.forEach(token -> {
            assertThat(token.getUsername(), equalTo(username));
            assertThat(token.isRevoked(), is(true));
        });

        // 6. Get all revoked access token ids
        List<String> revokedTokenIds = getRevokedTokenIds();

        assertThat(revokedTokenIds, notNullValue());
        accessTokenList.forEach(token -> {
            assertThat(revokedTokenIds.contains(token.getTokenId()), is(true));
        });
    }

    public static List<RefreshTokenDto> findRefreshTokens(String username, boolean revoked) {

        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(30,  TimeUnit.SECONDS)
                .build();

        String url = BASE_URL + String.format("?user.username=%s&revoked=%s", username, revoked);
        Request request = new Request.Builder()
                .header("Content-Type", "application/json; charset=utf-8")
                .url(url)
                .get()
                .build();

        Page<RefreshTokenDto> page = null;
        try (Response response = client.newCall(request).execute()) {
            if (response.code() == 200) {
                page = mapper.readValue(response.body().string(), new TypeReference<PageWrapper<RefreshTokenDto>>(){});
            } else {
                System.err.println("\n" + response.body().string() + "\n");
            }
        } catch (IOException io) {
            io.printStackTrace(System.err);
        }

        return page != null ? page.getContent() : null;
    }

    public static List<AccessTokenDto> findAccessTokens(String refreshTokenId) {

        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(30,  TimeUnit.SECONDS)
                .build();

        String url = BASE_URL + String.format("/%s/access_token/", refreshTokenId);
        Request request = new Request.Builder()
                .header("Content-Type", "application/json; charset=utf-8")
                .url(url)
                .get()
                .build();

        Page<AccessTokenDto> page = null;
        try (Response response = client.newCall(request).execute()) {
            if (response.code() == 200) {
                page = mapper.readValue(response.body().string(), new TypeReference<PageWrapper<AccessTokenDto>>(){});
            } else {
                System.err.println("\n" + response.body().string() + "\n");
            }
        } catch (IOException io) {
            io.printStackTrace(System.err);
        }

        return page != null ? page.getContent() : null;
    }

    public static boolean revokeRefreshToken(String tokenId) {

        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(30,  TimeUnit.SECONDS)
                .build();

        String url = BASE_URL + String.format("/%s/revoke", tokenId);
        Request request = new Request.Builder()
                .header("Content-Type", "application/json; charset=utf-8")
                .url(url)
                .put(emptyBody())
                .build();

        try (Response response = client.newCall(request).execute()) {
            if (response.code() == 200) {
                return true;
            } else {
                System.err.println("\n" + response.body().string() + "\n");
                return false;
            }
        } catch (IOException io) {
            io.printStackTrace(System.err);
            return false;
        }
    }

    public static List<String> getRevokedTokenIds() {

        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(30,  TimeUnit.SECONDS)
                .build();

        String url = BASE_URL + "/revoked?page=0&size=100";
        Request request = new Request.Builder()
                .header("Content-Type", "application/json; charset=utf-8")
                .url(url)
                .get()
                .build();

        Page<String> page = null;
        try (Response response = client.newCall(request).execute()) {
            if (response.code() == 200) {
                page = mapper.readValue(response.body().string(), new TypeReference<PageWrapper<String>>(){});
            } else {
                System.err.println("\n" + response.body().string() + "\n");
            }
        } catch (IOException io) {
            io.printStackTrace(System.err);
        }

        return page != null ? page.getContent() : null;
    }

    private static RequestBody emptyBody() {
        return RequestBody.create(MediaType.parse("application/json; charset=utf-8"), "");
    }
}
