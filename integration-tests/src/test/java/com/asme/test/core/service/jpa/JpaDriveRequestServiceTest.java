package com.asme.test.core.service.jpa;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.asme.common.enums.DriveRouteStatus;
import com.asme.common.geo.GeoPoint;
import com.asme.core.configuration.AsmeConfiguration;
import com.asme.core.configuration.AsyncConfiguration;
import com.asme.core.configuration.OsrmConfiguration;
import com.asme.core.entity.jpa.JpaDriveRequest;
import com.asme.core.entity.jpa.JpaDriveRoute;
import com.asme.core.service.jpa.JpaDriveRequestService;
import com.asme.core.service.jpa.JpaDriveRequestServiceImpl;
import com.asme.core.service.jpa.JpaDriveRouteService;
import com.asme.core.service.jpa.JpaDriveRouteServiceImpl;
import com.asme.core.service.jpa.JpaPlanRequestServiceImpl;
import com.asme.core.service.jpa.JpaPlanRouteServiceImpl;
import com.asme.core.utils.GeoUtils;
import com.asme.test.core.configuration.TestPostgisConfiguration;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * @author Vladyslav Mykhalets
 * @since 03.12.17
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {
        JpaDriveRequestServiceImpl.class,
        JpaDriveRouteServiceImpl.class,
        JpaPlanRequestServiceImpl.class,
        JpaPlanRouteServiceImpl.class,

        AsmeConfiguration.class,
        AsyncConfiguration.class,
        OsrmConfiguration.class,
        TestPostgisConfiguration.class
})
public class JpaDriveRequestServiceTest {

    @Autowired
    private JpaDriveRequestService driveRequestService;

    @Autowired
    private JpaDriveRouteService driveRouteService;

    @Test
    public void findByRoute() throws ParseException {

        // http://osrm:5000/route/v1/driving/30.387588,50.452806;30.448539,50.517392?steps=true
        // http://osrm:5000/tile/v1/driving/tile(2390,1387,12).mvt

        // request-1-forward-direction
        JpaDriveRequest driveRequest = new JpaDriveRequest.Builder()
                .userId(1L)
                .username("user-1")
                .startLocation(new GeoPoint("30.379211", "50.454985"), new BigDecimal("50"))
                .destination(new GeoPoint("30.402875", "50.458311"), new BigDecimal("50"))
                .build();

        // request-2-reverse-direction
        JpaDriveRequest driveRequestReverseDirection = new JpaDriveRequest.Builder()
                .userId(2L)
                .username("user-2")
                .startLocation(new GeoPoint("30.402875", "50.458311"), new BigDecimal("50"))
                .destination(new GeoPoint("30.379211", "50.454985"), new BigDecimal("50"))
                .build();

        // request-3-not-on-route
        JpaDriveRequest driveRequestNotOnRoute = new JpaDriveRequest.Builder()
                .userId(3L)
                .username("user-3")
                .startLocation(new GeoPoint("30.375942", "50.454766"), new BigDecimal("50"))
                .destination(new GeoPoint("30.402875", "50.458311"), new BigDecimal("50"))
                .build();

        // request-4-forward-direction-within-single-step
        JpaDriveRequest driveRequestWithinSingleStep = new JpaDriveRequest.Builder()
                .userId(4L)
                .username("user-4")
                .startLocation(new GeoPoint("30.379211", "50.454985"), new BigDecimal("20"))
                .destination(new GeoPoint("30.378957", "50.456623"), new BigDecimal("20"))
                .build();

        // request-5-reverse-direction-within-single-step
        JpaDriveRequest driveRequestWithinSingleStepReverseDirection = new JpaDriveRequest.Builder()
                .userId(5L)
                .username("user-5")
                .startLocation(new GeoPoint("30.378957", "50.456623"), new BigDecimal("20"))
                .destination(new GeoPoint("30.379211", "50.454985"), new BigDecimal("20"))
                .build();

        driveRequestService.save(driveRequest);
        driveRequestService.save(driveRequestReverseDirection);
        driveRequestService.save(driveRequestNotOnRoute);
        driveRequestService.save(driveRequestWithinSingleStep);
        driveRequestService.save(driveRequestWithinSingleStepReverseDirection);

        WKTReader wkt = new WKTReader(GeoUtils.defaultGeometryFactory);

        LineString route = (LineString) wkt.read("LINESTRING ( " +
                "30.387605 50.452807, " +
                "30.387599 50.452859, " +
                "30.379666 50.452316, " +
                "30.378968 50.456644, " +
                "30.400804 50.458253, " +
                "30.406282 50.45851, " +
                "30.406132 50.45925 " +
                ")");

        LineString waypoints = (LineString) wkt.read("LINESTRING ( " +
                "30.387605 50.452807, " +
                "30.406132 50.45925 " +
                ")");

        JpaDriveRoute driveRoute = new JpaDriveRoute.Builder()
                .userId(6L)
                .username("user-driver-6")
                .route(route)
                .routeStatus(DriveRouteStatus.OPEN)
                .geometry("")
                .distance(new BigDecimal("100.00"))
                .waypoints(waypoints)
                .build();

        JpaDriveRoute savedRoute = driveRouteService.save(driveRoute);

        // test find by route id
        PageRequest pageRequest = new PageRequest(0, 10);
        Page<JpaDriveRequest> result = driveRequestService.findByDriveRoute(savedRoute.getId(), pageRequest);

        assertThat(result, is(notNullValue()));
        assertThat(result.getTotalElements(), is(2L));
        assertThat("First drive request found must be 'request-1-forward-direction'", result.getContent().get(0).getUsername(),
                is("user-1"));
        assertThat("Second drive request found must be 'request-4-forward-direction-within-single-step'", result.getContent().get(1).getUsername(),
                is("user-4"));

        // test find by route id as stream
        List<JpaDriveRequest> listResult = new ArrayList<>();
        driveRequestService.findByDriveRoute(savedRoute.getId(), listResult::add);

        assertThat(listResult, is(notNullValue()));
        assertThat(listResult.size(), is(2));
        assertThat("First drive request found must be 'request-1-forward-direction'", listResult.get(0).getUsername(),
                is("user-1"));
        assertThat("Second drive request found must be 'request-4-forward-direction-within-single-step'", listResult.get(1).getUsername(),
                is("user-4"));

        // test find by route line
        result = driveRequestService.findByRouteLine(savedRoute.getUserId(), GeoUtils.fromJTSLineString(route), pageRequest);

        assertThat(result, is(notNullValue()));
        assertThat(result.getTotalElements(), is(2L));
        assertThat("First drive request found must be 'request-1-forward-direction'", result.getContent().get(0).getUsername(),
                is("user-1"));
        assertThat("Second drive request found must be 'request-4-forward-direction-within-single-step'", result.getContent().get(1).getUsername(),
                is("user-4"));

        // test find by route line as stream
        List<JpaDriveRequest> listResult2 = new ArrayList<>();
        driveRequestService.findByRouteLine(savedRoute.getUserId(), route, listResult2::add);

        assertThat(listResult2, is(notNullValue()));
        assertThat(listResult2.size(), is(2));
        assertThat("First drive request found must be 'request-1-forward-direction'", listResult2.get(0).getUsername(),
                is("user-1"));
        assertThat("Second drive request found must be 'request-4-forward-direction-within-single-step'", listResult2.get(1).getUsername(),
                is("user-4"));

        driveRequestService.deleteAll();
        driveRouteService.deleteAll();
    }
}
