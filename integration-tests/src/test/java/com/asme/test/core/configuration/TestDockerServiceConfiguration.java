package com.asme.test.core.configuration;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.asme.test.core.properties.DockerService;
import com.asme.test.core.properties.DockerServiceProperties;
import com.google.common.collect.Lists;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

/**
 * @author Vladyslav Mykhalets
 */
@Configuration
public class TestDockerServiceConfiguration {
    private static final Logger log = LoggerFactory.getLogger(TestDockerServiceConfiguration.class);

    private final RestTemplate template = new RestTemplate();

    @Autowired
    private DockerServiceProperties dockerServiceProperties;

    @PostConstruct
    public void start() {
        dockerServiceProperties.getServices().forEach(this::start);
    }

    @PreDestroy
    public void close() {
        stop();
    }

    public void start(String serviceName, DockerService dockerService) {

        String command = String.format("/vagrant/virtual/bin/run.sh -d -s %s -f /vagrant/virtual/bin/docker-compose-test.yml", serviceName);
        executeSshCommand(command);

        if (dockerService.getWaitForUrl() != null) {

            int statusCode = 200;
            if (dockerService.getWaitForStatus() != null) {
                statusCode = dockerService.getWaitForStatus();
            }

            while (true) {

                ResponseEntity<String> responseEntity;
                try {
                    responseEntity = template.getForEntity(dockerService.getWaitForUrl(), String.class);
                } catch (Exception e) {
                    log.info("Received exception '{}' when waiting for service '{}'. Continue waiting.. ",
                            e.getMessage(), dockerService.getId());

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ie) {
                        throw new RuntimeException(ie);
                    }

                    continue;
                }

                if (responseEntity.getStatusCodeValue() == statusCode) {
                    log.info("Received successful code '{}' when waiting for service '{}'. Continue bootstrap.. ",
                            responseEntity.getStatusCodeValue(), dockerService.getId());
                    break;
                } else {
                    log.info("Received not-acceptable code '{}' when waiting for service '{}'. Continue waiting.. ",
                            responseEntity.getStatusCodeValue(), dockerService.getId());

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }
    }

    public static void start(String... services) {
        Lists.newArrayList(services).forEach(service -> {
            String command = String.format("/vagrant/virtual/bin/run.sh -d -s %s -f /vagrant/virtual/bin/docker-compose-test.yml", service);
            executeSshCommand(command);
        });
    }

    public static void stop() {
        String command = "/vagrant/virtual/bin/stop.sh -f /vagrant/virtual/bin/docker-compose-test.yml";
        executeSshCommand(command);

        command = "/vagrant/virtual/bin/remove.sh -f /vagrant/virtual/bin/docker-compose-test.yml";
        executeSshCommand(command);
    }

    public static void executeSshCommand(String command) {

        // http://www.jcraft.com/jsch/examples

        final String host = "192.168.33.10";
        final String user = "ubuntu";
        final int port = 22;

        int exitCode;
        Session session;
        try {
            JSch jsch = new JSch();
            jsch.addIdentity("../.vagrant/machines/server/virtualbox/private_key");

            session = jsch.getSession(user, host, port);

            Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");

            session.setConfig(config);
        } catch (JSchException e) {
            throw new RuntimeException("Failed to create Jsch Session object.", e);
        }

        try {
            session.connect();

            Channel channel = session.openChannel("exec");
            ((ChannelExec) channel).setCommand(command);
            ((ChannelExec) channel).setPty(false);

            InputStream in = channel.getInputStream();
            channel.connect();

            byte[] tmp = new byte[1024];
            while (true) {
                while (in.available() > 0) {
                    int i = in.read(tmp, 0, 1024);
                    if (i < 0) break;
                    System.out.print(new String(tmp, 0, i));
                }
                if (channel.isClosed()) {
                    exitCode = channel.getExitStatus();
                    System.out.println("\nExit code: " + exitCode + "\n");
                    break;
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    System.err.println("Sleep was interrupted");
                }
            }

            channel.disconnect();
            session.disconnect();
        } catch (JSchException e) {
            throw new RuntimeException("Error during SSH command execution. Command: " + command);
        } catch (IOException ioe) {
            throw new RuntimeException("Error during input channel init. Command: " + command);
        }

        if (exitCode != 0) {
            throw new RuntimeException("Remote command exited with code: " + exitCode + ". " +
                    "\n Command: " + command);
        }
    }
}
