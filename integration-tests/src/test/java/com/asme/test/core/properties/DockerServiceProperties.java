package com.asme.test.core.properties;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author Vladyslav Mykhalets
 */
@Component
@ConfigurationProperties("docker")
public class DockerServiceProperties {

    private Map<String, DockerService> services = new HashMap<>();

    public Map<String, DockerService> getServices() {
        return services;
    }

    public void setServices(Map<String, DockerService> services) {
        this.services = services;
    }
}
