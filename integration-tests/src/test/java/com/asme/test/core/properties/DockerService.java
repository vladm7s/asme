package com.asme.test.core.properties;

/**
 * @author Vladyslav Mykhalets
 * @since 15.03.18
 */
public class DockerService {

    private String id;

    private String waitForUrl;

    private Integer waitForStatus;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWaitForUrl() {
        return waitForUrl;
    }

    public void setWaitForUrl(String waitForUrl) {
        this.waitForUrl = waitForUrl;
    }

    public Integer getWaitForStatus() {
        return waitForStatus;
    }

    public void setWaitForStatus(Integer waitForStatus) {
        this.waitForStatus = waitForStatus;
    }
}
