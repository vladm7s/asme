package com.asme.test.core.service.elasticsearch;

import org.elasticsearch.common.geo.builders.ShapeBuilder;
import org.elasticsearch.common.unit.DistanceUnit;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import com.asme.common.dto.request.DriveRequestSearchParameters;
import com.asme.common.geo.GeoPoint;
import com.asme.common.geo.GeoShapeCircle;
import com.asme.common.geo.GeoShapePoint;
import com.asme.core.elasticsearch.entity.DriveRequest;
import com.asme.core.elasticsearch.entity.DriveRoute;
import com.asme.core.elasticsearch.service.DriveRequestService;
import com.asme.core.elasticsearch.service.DriveRouteService;
import com.asme.core.utils.GeoUtils;
import com.spatial4j.core.context.jts.JtsSpatialContext;
import com.spatial4j.core.shape.Circle;
import com.spatial4j.core.shape.Shape;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * @author Vladyslav Mykhalets
 */
//@RunWith(SpringRunner.class)
//@ContextConfiguration(classes = {
//        TestElasticSearchConfiguration.class,
//        DriveRequestServiceImpl.class,
//        DriveRouteServiceImpl.class
//})
@Ignore
public class DriveRequestServiceTest {

    @Autowired
    private DriveRequestService driveRequestService;

    @Autowired
    private DriveRouteService driveRouteService;

    @Test
    public void findByStartLocationAndDestinationTest() {

        DriveRequest driveRequest = new DriveRequest.DriveRequestBuilder()
                .id("request-1")
                .userId(1L)
                .startLocation(new GeoShapeCircle("11.450894, 48.145363", "200"))
                .destination(new GeoShapeCircle("11.478792, 48.153735", "200"))
                .build();

        driveRequestService.save(driveRequest);

        DriveRequestSearchParameters searchParameters = new DriveRequestSearchParameters.DriveRequestSearchParametersBuilder()
                .startLocation(new GeoShapePoint("11.449916, 48.145232"))
                .destination(new GeoShapePoint("11.477019, 48.153861"))
                .build();

        PageRequest pageRequest = new PageRequest(0, 10);
        Page<DriveRequest> result = driveRequestService.find(searchParameters, pageRequest);

        assertThat(result, is(notNullValue()));
        assertThat(result.getTotalElements(), is(1L));

        // http://osrm:5000/route/v1/driving/30.387588,50.452806;30.448539,50.517392?steps=true

        // http://osrm:5000/tile/v1/driving/tile(2390,1387,12).mvt
    }

    @Test
    public void jtsIntersectionTest() {

        Coordinate routeStepStart = new Coordinate(30.379666, 50.452316);
        Coordinate routeStepEnd = new Coordinate(30.378968, 50.456644);

        Shape routeStep = ShapeBuilder.newLineString().points(routeStepStart, routeStepEnd).build();

        Circle startLocation = ShapeBuilder.newCircleBuilder()
                .center(new Coordinate(30.379211, 50.454985))
                .radius(50, DistanceUnit.METERS)
                .build();

        Circle destination = ShapeBuilder.newCircleBuilder()
                .center(new Coordinate(30.402875, 50.458311))
                .radius(50, DistanceUnit.METERS)
                .build();

        Geometry startLocationGeometry = JtsSpatialContext.GEO.getGeometryFrom(startLocation);
        Geometry destinationGeometry = JtsSpatialContext.GEO.getGeometryFrom(destination);
        Geometry routeStepGeometry = JtsSpatialContext.GEO.getGeometryFrom(routeStep);

        boolean startLocationIntersects = routeStepGeometry.intersects(startLocationGeometry);
        boolean destinationIntersects = routeStepGeometry.intersects(destinationGeometry);

        assertThat(startLocationIntersects, is(true));
        assertThat(destinationIntersects, is(false));
    }

    @Test
    public void findByRoute() throws ParseException {

        DriveRequest driveRequest = new DriveRequest.DriveRequestBuilder()
                .id("request-1-forward-direction")
                .userId(1L)
                .startLocation(new GeoShapeCircle("30.379211, 50.454985", "50"))
                .destination(new GeoShapeCircle("30.402875, 50.458311", "50"))
                .build();

        DriveRequest driveRequestReverseDirection = new DriveRequest.DriveRequestBuilder()
                .id("request-2-reverse-direction")
                .userId(2L)
                .startLocation(new GeoShapeCircle("30.402875, 50.458311", "50"))
                .destination(new GeoShapeCircle("30.379211, 50.454985", "50"))
                .build();

        DriveRequest driveRequestNotOnRoute = new DriveRequest.DriveRequestBuilder()
                .id("request-3-not-on-route")
                .userId(3L)
                .startLocation(new GeoShapeCircle("30.375942, 50.454766", "50"))
                .destination(new GeoShapeCircle("30.402875, 50.458311", "50"))
                .build();

        DriveRequest driveRequestWithinSingleStep = new DriveRequest.DriveRequestBuilder()
                .id("request-4-forward-direction-within-single-step")
                .userId(4L)
                .startLocation(new GeoShapeCircle("30.379211, 50.454985", "20"))
                .destination(new GeoShapeCircle("30.378957, 50.456623", "20"))
                .build();

        DriveRequest driveRequestWithinSingleStepReverseDirection = new DriveRequest.DriveRequestBuilder()
                .id("request-5-reverse-direction-within-single-step")
                .userId(5L)
                .startLocation(new GeoShapeCircle("30.378957, 50.456623", "20"))
                .destination(new GeoShapeCircle("30.379211, 50.454985", "20"))
                .build();

        driveRequestService.save(driveRequest);
        driveRequestService.save(driveRequestReverseDirection);
        driveRequestService.save(driveRequestNotOnRoute);
        driveRequestService.save(driveRequestWithinSingleStep);
        driveRequestService.save(driveRequestWithinSingleStepReverseDirection);

        WKTReader wkt = new WKTReader();
        LineString lineString = (LineString) wkt.read("LINESTRING ( " +
                "30.387605 50.452807, " +
                "30.387599 50.452859, " +
                "30.379666 50.452316, " +
                "30.378968 50.456644, " +
                "30.400804 50.458253, " +
                "30.406282 50.45851, " +
                "30.406132 50.45925 " +
                ")");

        DriveRoute driveRoute = new DriveRoute.DriveRouteBuilder()
                .id("route-1")
                .userId("user-driver-1")
                .driverLocation(new GeoPoint("50.4528073", "30.38760"))
                .route(lineString)
                .build();

        driveRouteService.save(driveRoute);

        // test find by route id
        PageRequest pageRequest = new PageRequest(0, 10);
        Page<DriveRequest> result = driveRequestService.find("route-1", pageRequest);

        assertThat(result, is(notNullValue()));
        assertThat(result.getTotalElements(), is(2L));
        assertThat("First drive request found must be 'request-1-forward-direction'", result.getContent().get(0).getId(),
                is("request-1-forward-direction"));
        assertThat("Second drive request found must be 'request-4-forward-direction-within-single-step'", result.getContent().get(1).getId(),
                is("request-4-forward-direction-within-single-step"));

        // test find by route
        result = driveRequestService.find(GeoUtils.fromJTSLineString(lineString), pageRequest);

        assertThat(result, is(notNullValue()));
        assertThat(result.getTotalElements(), is(2L));
        assertThat("First drive request found must be 'request-1-forward-direction'", result.getContent().get(0).getId(),
                is("request-1-forward-direction"));
        assertThat("Second drive request found must be 'request-4-forward-direction-within-single-step'", result.getContent().get(1).getId(),
                is("request-4-forward-direction-within-single-step"));
    }
}
