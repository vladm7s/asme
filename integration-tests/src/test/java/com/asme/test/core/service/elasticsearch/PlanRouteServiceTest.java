package com.asme.test.core.service.elasticsearch;

import java.math.BigDecimal;
import java.util.Arrays;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.asme.common.geo.GeoPoint;
import com.asme.core.elasticsearch.entity.PlanRoute;
import com.asme.core.elasticsearch.service.PlanRouteService;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * @author Vladyslav Mykhalets
 */
//@RunWith(SpringRunner.class)
//@ContextConfiguration(classes = {
//        AsmeConfiguration.class,
//        AsyncConfiguration.class,
//        OsrmConfiguration.class,
//        PlanRouteServiceImpl.class,
//        TestElasticSearchConfiguration.class,
//        TestDockerServiceConfiguration.class
//})
//@EnableConfigurationProperties(value = {DockerServiceProperties.class})
//@PropertySource("planRouteServiceTest.properties")
@Ignore
public class PlanRouteServiceTest {

    @Autowired
    private PlanRouteService planRouteService;

    @Test
    public void planRoute() {

        GeoPoint start = new GeoPoint("30.387588", "50.452806");
        GeoPoint destination = new GeoPoint("30.448539", "50.517392");

        PlanRoute planRoute = planRouteService.route(Arrays.asList(start, destination));

        PlanRoute savedPlanRoute = planRouteService.findOne(planRoute.getId());

        assertThat(savedPlanRoute, is(notNullValue()));
        assertThat(savedPlanRoute.getPlanRoute(), is(notNullValue()));

        assertThat(savedPlanRoute.getPlanRoute().getCoordinates().size(), is(342));

        assertThat(savedPlanRoute.getPlanRoute().getCoordinates().get(0), is(new BigDecimal[]{
                new BigDecimal("30.387610"),
                new BigDecimal("50.452810")
        }));

        assertThat(savedPlanRoute.getPlanRoute().getCoordinates().get(4), is(new BigDecimal[]{
                new BigDecimal("30.386200"),
                new BigDecimal("50.452750")
        }));

        assertThat(savedPlanRoute.getPlanRoute().getCoordinates().get(341), is(new BigDecimal[]{
                new BigDecimal("30.448540"),
                new BigDecimal("50.517380")
        }));
    }
}
