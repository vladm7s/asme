package com.asme.test.core.configuration;

import javax.annotation.PreDestroy;
import javax.sql.DataSource;

import org.springframework.context.annotation.Configuration;

import com.asme.core.configuration.PostgisConfiguration;

/**
 * @author Vladyslav Mykhalets
 * @since 07.11.17
 */
@Configuration
public class TestPostgisConfiguration extends PostgisConfiguration {

    @Override
    public DataSource dataSource() {

        try {
            TestDockerServiceConfiguration.start("postgis");
            Thread.sleep(10000);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return super.dataSource();
    }

    @PreDestroy
    public void close() {
        TestDockerServiceConfiguration.stop();
    }
}
