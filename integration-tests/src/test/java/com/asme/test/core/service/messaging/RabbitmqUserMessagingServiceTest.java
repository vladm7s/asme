package com.asme.test.core.service.messaging;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.asme.messaging.service.messaging.user.UserMessageListener;
import com.asme.messaging.service.messaging.user.UserMessagingService;
import com.asme.test.core.configuration.TestDockerServiceConfiguration;
import com.asme.test.core.configuration.TestMessagingConfiguration;
import com.asme.test.core.properties.DockerServiceProperties;

import static junit.framework.Assert.fail;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author Vladyslav Mykhalets
 * @since 21.01.18
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {
        TestDockerServiceConfiguration.class,
        TestMessagingConfiguration.class
})
@EnableConfigurationProperties(value = {DockerServiceProperties.class})
@PropertySource("messaging/messaging.properties")
public class RabbitmqUserMessagingServiceTest {

    @Autowired
    private UserMessagingService userMessagingService;

    @Test
    public void subscribeUserTest() throws InterruptedException {

        CountDownLatch countDownLatchUser_1 = new CountDownLatch(3);
        CountDownLatch countDownLatchUser_2 = new CountDownLatch(2);
        CountDownLatch countDownLatchUser_3 = new CountDownLatch(4);

        userMessagingService.subscribeUser(1L, new UserMessageListener() {
            @Override
            public void onMessage(String message) {
                countDownLatchUser_1.countDown();
            }
        });

        userMessagingService.subscribeUser(2L, new UserMessageListener() {
            @Override
            public void onMessage(String message) {
                countDownLatchUser_2.countDown();
            }
        });

        userMessagingService.subscribeUser(3L, new UserMessageListener() {
            @Override
            public void onMessage(String message) {
                countDownLatchUser_3.countDown();
            }
        });

        userMessagingService.sendMessageToUser(2L, "message");
        userMessagingService.sendMessageToUser(3L, "message");
        userMessagingService.sendMessageToUser(3L, "message");
        userMessagingService.sendMessageToUser(1L, "message");
        userMessagingService.sendMessageToUser(1L, "message");
        userMessagingService.sendMessageToUser(2L, "message");
        userMessagingService.sendMessageToUser(1L, "message");
        userMessagingService.sendMessageToUser(3L, "message");
        userMessagingService.sendMessageToUser(3L, "message");

        if (!countDownLatchUser_1.await(10, TimeUnit.SECONDS)) {
            fail("Message for user-1 not received");
        }
        if (!countDownLatchUser_2.await(10, TimeUnit.SECONDS)) {
            fail("Message for user-2 not received");
        }
        if (!countDownLatchUser_3.await(10, TimeUnit.SECONDS)) {
            fail("Message for user-3 not received");
        }

        assertThat(countDownLatchUser_1.getCount(), is(0L));
        assertThat(countDownLatchUser_2.getCount(), is(0L));
        assertThat(countDownLatchUser_3.getCount(), is(0L));
    }
}
