package com.asme.test.core.service.elasticsearch;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import com.asme.common.dto.route.DriveRouteSearchParameters;
import com.asme.common.geo.GeoPoint;
import com.asme.common.geo.GeoShapeCircle;
import com.asme.core.elasticsearch.entity.DriveRequest;
import com.asme.core.elasticsearch.entity.DriveRoute;
import com.asme.core.elasticsearch.service.DriveRequestService;
import com.asme.core.elasticsearch.service.DriveRouteService;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * @author Vladyslav Mykhalets
 */
//@RunWith(SpringRunner.class)
//@ContextConfiguration(classes = {
//        TestElasticSearchConfiguration.class,
//        DriveRequestServiceImpl.class,
//        DriveRouteServiceImpl.class
//})
@Ignore
public class DriveRouteServiceTest {

    @Autowired
    private DriveRequestService driveRequestService;

    @Autowired
    private DriveRouteService driveRouteService;

    @Test
    public void findByRequest() throws ParseException {

        GeoShapeCircle startLocation = new GeoShapeCircle("30.379211, 50.454985", "20");
        GeoShapeCircle destination = new GeoShapeCircle("30.402875, 50.458311", "20");

        DriveRequest driveRequest = new DriveRequest.DriveRequestBuilder()
                .id("request-1")
                .userId(1L)
                .startLocation(startLocation)
                .destination(destination)
                .build();

        driveRequestService.save(driveRequest);

        WKTReader wkt = new WKTReader();
        LineString lineString = (LineString) wkt.read("LINESTRING ( " +
                "30.387605 50.452807, " +
                "30.387599 50.452859, " +
                "30.379666 50.452316, " +
                "30.378968 50.456644, " +
                "30.400804 50.458253, " +
                "30.406282 50.45851, " +
                "30.406132 50.45925 " +
                ")");

        DriveRoute driveRoute = new DriveRoute.DriveRouteBuilder()
                .id("route-1")
                .userId("user-driver-1")
                .driverLocation(new GeoPoint("50.4528073", "30.38760"))
                .route(lineString)
                .build();

        LineString lineStringReverse = (LineString) wkt.read("LINESTRING ( " +
                "30.406132 50.45925, " +
                "30.406282 50.45851, " +
                "30.400804 50.458253, " +
                "30.378968 50.456644, " +
                "30.379666 50.452316, " +
                "30.387599 50.452859, " +
                "30.387605 50.452807 " +
                ")");

        DriveRoute driveRouteReverse = new DriveRoute.DriveRouteBuilder()
                .id("route-2-reverse")
                .userId("user-driver-2")
                .driverLocation(new GeoPoint("50.4528073", "30.38760"))
                .route(lineStringReverse)
                .build();

        LineString lineStringShort = (LineString) wkt.read("LINESTRING ( " +
                "30.387599 50.452859, " +
                "30.379666 50.452316, " +
                "30.378968 50.456644, " +
                "30.400804 50.458253, " +
                "30.406282 50.45851 " +
                ")");

        DriveRoute driveRouteShort = new DriveRoute.DriveRouteBuilder()
                .id("route-3-short")
                .userId("user-driver-3")
                .driverLocation(new GeoPoint("50.4528073", "30.38760"))
                .route(lineStringShort)
                .build();

        driveRouteService.save(driveRoute);
        driveRouteService.save(driveRouteReverse);
        driveRouteService.save(driveRouteShort);

        // test find by request id
        PageRequest pageRequest = new PageRequest(0, 10);
        Page<DriveRoute> result = driveRouteService.find("request-1", pageRequest);

        assertThat(result, is(notNullValue()));
        assertThat(result.getTotalElements(), is(2L));
        assertThat("First drive route found must be 'route-1'", result.getContent().get(0).getId(),
                is("route-1"));
        assertThat("Second drive route found must be 'route-3-short'", result.getContent().get(1).getId(),
                is("route-3-short"));

        // test find by request start location /destination
        result = driveRouteService.find(new DriveRouteSearchParameters(startLocation, destination), pageRequest);

        assertThat(result, is(notNullValue()));
        assertThat(result.getTotalElements(), is(2L));
        assertThat("First drive route found must be 'route-1'", result.getContent().get(0).getId(),
                is("route-1"));
        assertThat("Second drive route found must be 'route-3-short'", result.getContent().get(1).getId(),
                is("route-3-short"));
    }
}
