package com.asme.test.core.repository.elasticsearch;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.asme.core.elasticsearch.entity.PlanRoute;
import com.asme.core.elasticsearch.repository.PlanRouteRepository;
import com.asme.core.utils.JsonUtils;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * @author Vladyslav Mykhalets
 * @since 20.10.17
 */

//@RunWith(SpringRunner.class)
//@ContextConfiguration(classes = {
//        TestElasticSearchConfiguration.class,
//})
@Ignore
public class PlanRouteRepositoryTest {

    @Autowired
    private PlanRouteRepository planRouteRepository;

    @Test
    public void planRoute() {

        PlanRoute planRoute = JsonUtils.fromJsonFile("planRoute.json", PlanRoute.class);

        long start = System.currentTimeMillis();
        PlanRoute savedPlanRoute = planRouteRepository.save(planRoute);
        long stop = System.currentTimeMillis();

        assertThat(savedPlanRoute, is(notNullValue()));

        assertThat(savedPlanRoute.getPlanRoute(), is(notNullValue()));

        assertThat(stop - start, lessThan(5000L));
    }
}
