package com.asme.test.core.configuration;

import java.io.File;

import javax.annotation.PreDestroy;

import org.apache.commons.io.FileUtils;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.node.Node;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

import static org.elasticsearch.node.NodeBuilder.nodeBuilder;

/**
 * @author Vladyslav Mykhalets
 */
//@Configuration
//@EnableElasticsearchRepositories(basePackages = "com.asme.core.repository.elasticsearch")
public class TestElasticSearchConfiguration {

    private static String ES_HOME_PATH = "/tmp/elasticsearch-home";
    private static String ES_DATA_PATH = "/tmp/elasticsearch-data";

    private Node node;

    private Client client;

    private ElasticsearchTemplate elasticsearchTemplate;

    private static Settings.Builder elasticsearchSettings = Settings.settingsBuilder()
            .put("http.enabled", "true")
            .put("path.data", ES_DATA_PATH)
            .put("path.home", ES_HOME_PATH);

    @Bean
    public ElasticsearchOperations elasticsearchTemplate() {
        if (this.node == null && this.client == null && this.elasticsearchTemplate == null) {
            this.node = nodeBuilder()
                    .local(true)
                    .settings(elasticsearchSettings.build())
                    .node();
            this.client = this.node.client();
            this.elasticsearchTemplate = new ElasticsearchTemplate(this.client);
        }
        return this.elasticsearchTemplate;
    }

    @PreDestroy
    public void close() {
        if (node == null || client == null)
            return;

        client.close();
        if (!node.isClosed()) {
            node.close();
        }

        client = null;
        node = null;

        try {
            FileUtils.deleteDirectory(new File(ES_HOME_PATH));
        } catch (Exception e) {
            System.err.println("Could not delete directory: " + ES_HOME_PATH);
        }
        try {
            FileUtils.deleteDirectory(new File(ES_DATA_PATH));
        } catch (Exception e) {
            System.err.println("Could not delete directory: " + ES_DATA_PATH);
        }
    }
}
