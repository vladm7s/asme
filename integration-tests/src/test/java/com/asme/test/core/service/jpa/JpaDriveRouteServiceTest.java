package com.asme.test.core.service.jpa;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.asme.common.dto.route.DriveRouteSearchParameters;
import com.asme.common.enums.DriveRouteStatus;
import com.asme.common.geo.GeoPoint;
import com.asme.common.geo.GeoShapeCircle;
import com.asme.core.configuration.AsmeConfiguration;
import com.asme.core.configuration.AsyncConfiguration;
import com.asme.core.configuration.OsrmConfiguration;
import com.asme.core.entity.jpa.JpaDriveRequest;
import com.asme.core.entity.jpa.JpaDriveRoute;
import com.asme.core.service.jpa.JpaDriveRequestService;
import com.asme.core.service.jpa.JpaDriveRequestServiceImpl;
import com.asme.core.service.jpa.JpaDriveRouteService;
import com.asme.core.service.jpa.JpaDriveRouteServiceImpl;
import com.asme.core.service.jpa.JpaPlanRequestServiceImpl;
import com.asme.core.service.jpa.JpaPlanRouteServiceImpl;
import com.asme.core.utils.GeoUtils;
import com.asme.test.core.configuration.TestPostgisConfiguration;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * @author Vladyslav Mykhalets
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {
        JpaDriveRequestServiceImpl.class,
        JpaDriveRouteServiceImpl.class,
        JpaPlanRequestServiceImpl.class,
        JpaPlanRouteServiceImpl.class,

        AsmeConfiguration.class,
        AsyncConfiguration.class,
        OsrmConfiguration.class,
        TestPostgisConfiguration.class
})
public class JpaDriveRouteServiceTest {

    @Autowired
    private JpaDriveRequestService driveRequestService;

    @Autowired
    private JpaDriveRouteService driveRouteService;

    @Test
    public void findByRequest() throws ParseException {
        WKTReader wkt = new WKTReader(GeoUtils.defaultGeometryFactory);

        GeoPoint startLocation = new GeoPoint("30.379211", "50.454985");
        GeoShapeCircle startLocationCircle = new GeoShapeCircle("30.379211, 50.454985", "20");

        GeoPoint destination = new GeoPoint("30.402875", "50.458311");
        GeoShapeCircle destinationCircle = new GeoShapeCircle("30.402875, 50.458311", "20");

        JpaDriveRequest driveRequest = new JpaDriveRequest.Builder()
                .userId(1L)
                .username("user-1")
                .startLocation(startLocation, new BigDecimal("20"))
                .destination(destination, new BigDecimal("20"))
                .build();

        JpaDriveRequest savedRequest = driveRequestService.save(driveRequest);

        LineString lineString = (LineString) wkt.read("LINESTRING ( " +
                "30.387605 50.452807, " +
                "30.387599 50.452859, " +
                "30.379666 50.452316, " +
                "30.378968 50.456644, " +
                "30.400804 50.458253, " +
                "30.406282 50.45851, " +
                "30.406132 50.45925 " +
                ")");

        LineString waypoints = (LineString) wkt.read("LINESTRING ( " +
                "30.387605 50.452807, " +
                "30.406132 50.45925 " +
                ")");

        JpaDriveRoute driveRoute = new JpaDriveRoute.Builder()
                .userId(10L)
                .username("user-driver-1")
                .route(lineString)
                .waypoints(waypoints)
                .distance(new BigDecimal("100"))
                .geometry("")
                .routeStatus(DriveRouteStatus.OPEN)
                .build();

        LineString lineStringReverse = (LineString) wkt.read("LINESTRING ( " +
                "30.406132 50.45925, " +
                "30.406282 50.45851, " +
                "30.400804 50.458253, " +
                "30.378968 50.456644, " +
                "30.379666 50.452316, " +
                "30.387599 50.452859, " +
                "30.387605 50.452807 " +
                ")");

        LineString waypointsReverse = (LineString) wkt.read("LINESTRING ( " +
                "30.406132 50.45925, " +
                "30.387605 50.452807 " +
                ")");

        JpaDriveRoute driveRouteReverse = new JpaDriveRoute.Builder()
                .userId(20L)
                .username("user-driver-2")
                .route(lineStringReverse)
                .waypoints(waypointsReverse)
                .distance(new BigDecimal("100"))
                .geometry("")
                .routeStatus(DriveRouteStatus.OPEN)
                .build();

        LineString lineStringShort = (LineString) wkt.read("LINESTRING ( " +
                "30.387599 50.452859, " +
                "30.379666 50.452316, " +
                "30.378968 50.456644, " +
                "30.400804 50.458253, " +
                "30.406282 50.45851 " +
                ")");

        LineString waypointsShort = (LineString) wkt.read("LINESTRING ( " +
                "30.387599 50.452859, " +
                "30.406282 50.45851 " +
                ")");

        JpaDriveRoute driveRouteShort = new JpaDriveRoute.Builder()
                .userId(30L)
                .username("user-driver-3")
                .route(lineStringShort)
                .waypoints(waypointsShort)
                .distance(new BigDecimal("100"))
                .geometry("")
                .routeStatus(DriveRouteStatus.OPEN)
                .build();

        driveRouteService.save(driveRoute);
        driveRouteService.save(driveRouteReverse);
        driveRouteService.save(driveRouteShort);

        // test find by request id
        PageRequest pageRequest = new PageRequest(0, 10);
        Page<JpaDriveRoute> result = driveRouteService.findByDriveRequest(savedRequest.getId(), pageRequest);

        assertThat(result, is(notNullValue()));
        assertThat(result.getTotalElements(), is(2L));
        assertThat("First drive route found must be 'route-1'", result.getContent().get(0).getUsername(),
                is("user-driver-1"));
        assertThat("Second drive route found must be 'route-3-short'", result.getContent().get(1).getUsername(),
                is("user-driver-3"));

        // test find by request start location / destination
        result = driveRouteService.findByStartAndDestination(savedRequest.getUserId(), new DriveRouteSearchParameters(startLocationCircle, destinationCircle), pageRequest);

        assertThat(result, is(notNullValue()));
        assertThat(result.getTotalElements(), is(2L));
        assertThat("First drive route found must be 'route-1'", result.getContent().get(0).getUsername(),
                is("user-driver-1"));
        assertThat("Second drive route found must be 'route-3-short'", result.getContent().get(1).getUsername(),
                is("user-driver-3"));

        driveRequestService.deleteAll();
        driveRouteService.deleteAll();
    }
}
