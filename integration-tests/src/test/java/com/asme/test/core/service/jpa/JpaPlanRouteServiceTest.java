package com.asme.test.core.service.jpa;

import java.math.BigDecimal;
import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.asme.common.geo.GeoPoint;
import com.asme.core.configuration.AsmeConfiguration;
import com.asme.core.configuration.AsyncConfiguration;
import com.asme.core.configuration.OsrmConfiguration;
import com.asme.test.core.configuration.TestDockerServiceConfiguration;
import com.asme.test.core.configuration.TestPostgisConfiguration;
import com.asme.core.entity.jpa.JpaPlanRoute;
import com.asme.test.core.properties.DockerServiceProperties;
import com.asme.core.service.jpa.JpaPlanRouteService;
import com.asme.core.service.jpa.JpaPlanRouteServiceImpl;
import com.vividsolutions.jts.geom.Coordinate;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * @author Vladyslav Mykhalets
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {
        AsmeConfiguration.class,
        AsyncConfiguration.class,
        OsrmConfiguration.class,
        JpaPlanRouteServiceImpl.class,
        TestPostgisConfiguration.class,
        TestDockerServiceConfiguration.class
})
@EnableConfigurationProperties(value = {DockerServiceProperties.class})
@PropertySource("jpaPlanRouteServiceTest.properties")
public class JpaPlanRouteServiceTest {

    @Autowired
    private JpaPlanRouteService planRouteService;

    @Test
    public void planRoute() {

        GeoPoint start = new GeoPoint("30.387588", "50.452806");
        GeoPoint destination = new GeoPoint("30.448539", "50.517392");

        JpaPlanRoute planRoute = planRouteService.route(Arrays.asList(start, destination));

        JpaPlanRoute savedPlanRoute = planRouteService.findOne(planRoute.getId());

        assertThat(savedPlanRoute, is(notNullValue()));
        assertThat(savedPlanRoute.getPlanRoute(), is(notNullValue()));
        assertThat(savedPlanRoute.getLastModified(), is(notNullValue()));

        assertThat(savedPlanRoute.getPlanRoute().getCoordinates().length, is(342));

        assertThat(savedPlanRoute.getPlanRoute().getCoordinates()[0], is(new Coordinate(
                new BigDecimal("30.387610").doubleValue(),
                new BigDecimal("50.452810").doubleValue()
        )));

        assertThat(savedPlanRoute.getPlanRoute().getCoordinates()[4], is(new Coordinate(
                new BigDecimal("30.386200").doubleValue(),
                new BigDecimal("50.452750").doubleValue()
        )));

        assertThat(savedPlanRoute.getPlanRoute().getCoordinates()[341], is(new Coordinate(
                new BigDecimal("30.448540").doubleValue(),
                new BigDecimal("50.517380").doubleValue()
        )));

        planRouteService.deleteAll();
    }
}
