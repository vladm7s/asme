package com.asme.test.core.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.asme.messaging.configuration.MessagingConfiguration;

/**
 * @author Vladyslav Mykhalets
 * @since 21.01.18
 */
@Configuration
@PropertySource("messaging/messaging.properties")
public class TestMessagingConfiguration extends MessagingConfiguration {
}
