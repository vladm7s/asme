package com.asme.test.core.repository.jpa;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.asme.test.core.configuration.TestPostgisConfiguration;
import com.asme.core.entity.jpa.JpaPlanRoute;
import com.asme.core.repository.jpa.JpaPlanRouteRepository;
import com.asme.core.utils.GeoUtils;
import com.asme.core.utils.PathUtils;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * @author Vladyslav Mykhalets
 * @since 23.10.17
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {
        TestPostgisConfiguration.class
})
public class JpaPlanRouteRepositoryTest {

    @Autowired
    private JpaPlanRouteRepository jpaPlanRouteRepository;

    @Test
    public void planRoute() throws ParseException {
        WKTReader wkt = new WKTReader(GeoUtils.defaultGeometryFactory);

        LineString planWaypoints = (LineString) wkt.read(PathUtils.readFileToString("planRoute-waypoints.wkt"));
        LineString planRouteLine = (LineString) wkt.read(PathUtils.readFileToString("planRoute-route.wkt"));

        long start = System.currentTimeMillis();

        for (int i = 0; i < 100; i++) {
            JpaPlanRoute planRoute = new JpaPlanRoute();

            planRoute.setPlanWaypoints(planWaypoints);
            planRoute.setPlanRoute(planRouteLine);
            planRoute.setPlanGeometry("");
            planRoute.setPlanDistance(new BigDecimal("1000.00"));

            JpaPlanRoute savedPlanRoute = jpaPlanRouteRepository.save(planRoute);

            assertThat(savedPlanRoute, is(notNullValue()));

            assertThat(savedPlanRoute.getPlanRoute(), is(notNullValue()));
        }
        long stop = System.currentTimeMillis();

        System.out.println("Execution time: " + (stop - start) + " ms");
        assertThat(stop - start, lessThan(20000L));

        jpaPlanRouteRepository.deleteAll();
    }
}
