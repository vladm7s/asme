package com.asme.test.websocket.api.handler;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nullable;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.asme.common.dto.authorization.JwtTokenResponse;
import com.asme.common.dto.authorization.token.RefreshTokenDto;
import com.asme.common.dto.messaging.CommunicateMessage;
import com.asme.common.dto.messaging.ConnectMessage;
import com.asme.common.enums.messaging.CloseReason;
import com.asme.common.utils.WebSocketUtils;
import com.asme.test.authorization.controller.TokenControllerTest;
import com.asme.test.core.configuration.TestDockerServiceConfiguration;
import com.asme.test.core.configuration.TestPostgisConfiguration;
import com.asme.test.core.properties.DockerServiceProperties;
import com.fasterxml.jackson.databind.ObjectMapper;

import static com.asme.test.authorization.controller.UserControllerTest.aboutMe;
import static com.asme.test.authorization.controller.UserControllerTest.signUp;
import static junit.framework.Assert.fail;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;

/**
 * @author Vladyslav Mykhalets
 * @since 21.01.18
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {
        TestPostgisConfiguration.class,
        TestDockerServiceConfiguration.class
})
@EnableConfigurationProperties(value = {DockerServiceProperties.class})
@PropertySource("websocket/handler.properties")
public class ApiRequestHandlerTest {

    private ObjectMapper mapper = new ObjectMapper();

    @Test
    public void restrictedAccessTest() throws Exception {

        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(300,  TimeUnit.SECONDS)
                .build();

        CountDownLatch restrictedAccessCounter = new CountDownLatch(1);

        WebSocketUtils.openWebSocket(client, "http://192.168.33.10:8081/web-socket/api/handler",
                null,
                response -> {},
                text -> {},
                (exception, response) -> { if (response.code() == 401) restrictedAccessCounter.countDown(); },
                closeReason -> {}
                );

        if (!restrictedAccessCounter.await(10, TimeUnit.SECONDS)) {
            fail("Restricted access error not received");
        }

        assertThat(restrictedAccessCounter.getCount(), is(0L));
    }

    @Test
    public void revokeTokenHandlerTest() throws Exception {

        // Sign-up user_1 & user_2
        String username_1 = "user-" + UUID.randomUUID().toString();
        String password_1 = "password-" + UUID.randomUUID().toString();
        String email_1 = String.format("email-%s@test.com", UUID.randomUUID().toString());

        String username_2 = "user-" + UUID.randomUUID().toString();
        String password_2 = "password-" + UUID.randomUUID().toString();
        String email_2 = String.format("email-%s@test.com", UUID.randomUUID().toString());

        JwtTokenResponse tokenResponse_1 = signUp(username_1, email_1, password_1);
        Long userId_1 = aboutMe(tokenResponse_1.getAccessToken()).getUserId();

        JwtTokenResponse tokenResponse_2 = signUp(username_2, email_2, password_2);
        Long userId_2 = aboutMe(tokenResponse_2.getAccessToken()).getUserId();
        //--------------//

        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(300,  TimeUnit.SECONDS)
                .build();

        CountDownLatch counter_1 = new CountDownLatch(2);
        CountDownLatch counter_2 = new CountDownLatch(2);
        CountDownLatch revokeTokenCounter_1 = new CountDownLatch(1);

        // Opening 1st websocket for user 1 to server 1
        WebSocket webSocketUser_1 = WebSocketUtils.openWebSocket(client, "http://192.168.33.10:8081/web-socket/api/handler",
                tokenResponse_1.getAccessToken(),
                response -> {},
                text -> counter_1.countDown() ,
                (exception, response) -> {},
                closeReason -> { if (closeReason.equals(CloseReason.TOKEN_REVOKED.name())) revokeTokenCounter_1.countDown(); });

        // Opening 2nd websocket for user 2 to server 2
        WebSocket webSocketUser_2 = WebSocketUtils.openWebSocket(client, "http://192.168.33.10:8082/web-socket/api/handler",
                tokenResponse_2.getAccessToken(),
                response -> {},
                text -> counter_2.countDown(),
                (exception, response) -> {},
                closeReason -> {});

        ConnectMessage connectMessage_1 = new ConnectMessage();
        webSocketUser_1.send(mapper.writeValueAsString(connectMessage_1));

        ConnectMessage connectMessage_2 = new ConnectMessage();
        webSocketUser_2.send(mapper.writeValueAsString(connectMessage_2));
        //-------------------//

        // wait until users are subscribed
        Thread.sleep(3000);

        // 1 message from user-2 to user-1
        CommunicateMessage message_1_ToUser_1 = new CommunicateMessage();
        message_1_ToUser_1.setToUser(userId_1);
        message_1_ToUser_1.setMessage("Message from user-2");

        webSocketUser_2.send(mapper.writeValueAsString(message_1_ToUser_1));
        //-------------------//

        // 1 message from user-1 to user-2
        CommunicateMessage message_1_ToUser_2 = new CommunicateMessage();
        message_1_ToUser_2.setToUser(userId_2);
        message_1_ToUser_2.setMessage("Message 1 from user-1");

        webSocketUser_1.send(mapper.writeValueAsString(message_1_ToUser_2));
        //-------------------//

        counter_1.await();
        counter_2.await();

        assertThat(counter_1.getCount(), is(0L));
        assertThat(counter_2.getCount(), is(0L));

        List<RefreshTokenDto> tokenList = TokenControllerTest.findRefreshTokens(username_1, false);
        boolean result = TokenControllerTest.revokeRefreshToken(tokenList.get(0).getTokenId());
        assertThat(result, is(true));

        // wait until token is revoked
        Thread.sleep(3000);

        // Another message from user-2 to user-1
        CommunicateMessage message_2_ToUser_1 = new CommunicateMessage();
        message_2_ToUser_1.setToUser(userId_1);
        message_2_ToUser_1.setMessage("Another message from user-2");

        webSocketUser_2.send(mapper.writeValueAsString(message_2_ToUser_1));
        //-------------------//

        revokeTokenCounter_1.await();
        assertThat(revokeTokenCounter_1.getCount(), is(0L));
    }

    @Test
    public void handlerTest() throws Exception {

        // Sign-up user_1
        String username_1 = "user-" + UUID.randomUUID().toString();
        String password_1 = "password-" + UUID.randomUUID().toString();
        String email_1 = String.format("email-%s@test.com", UUID.randomUUID().toString());

        JwtTokenResponse tokenResponse_1 = signUp(username_1, email_1, password_1);
        Long userId_1 = aboutMe(tokenResponse_1.getAccessToken()).getUserId();
        //--------------//

        // Sign-up user_2
        String username_2 = "user-" + UUID.randomUUID().toString();
        String password_2 = "password-" + UUID.randomUUID().toString();
        String email_2 = String.format("email-%s@test.com", UUID.randomUUID().toString());

        JwtTokenResponse tokenResponse_2 = signUp(username_2, email_2, password_2);
        Long userId_2 = aboutMe(tokenResponse_2.getAccessToken()).getUserId();
        //--------------//

        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(300,  TimeUnit.SECONDS)
                .build();

        // Opening 1st websocket for user 1 to server 1
        String serverUrl_1 = "http://192.168.33.10:8081/web-socket/api/handler";
        Request request_1 = new Request.Builder()
                .header("Authorization", "Bearer " + tokenResponse_1.getAccessToken())
                .url(serverUrl_1)
                .build();

        CountDownLatch counter_1 = new CountDownLatch(3);
        WebSocket webSocketUser_1 = client.newWebSocket(request_1, new WebSocketListener() {

            public void onOpen(WebSocket webSocket, Response response) {
                System.out.println("Opened web-socket for user-1 on server 1: " + response.toString());
            }

            public void onMessage(WebSocket webSocket, String text) {
                System.out.println("Message received: " + text);
                counter_1.countDown();
            }

            @Override
            public void onFailure(WebSocket webSocket, Throwable t, @Nullable Response response) {
                System.out.println("Failure: " + t.getMessage());
            }
        });

        ConnectMessage subscribe_1 = new ConnectMessage();
        webSocketUser_1.send(mapper.writeValueAsString(subscribe_1));
        //-------------------//

        // Opening 2nd websocket for user 2 to server 2
        String serverUrl_2 = "http://192.168.33.10:8082/web-socket/api/handler";
        Request request_2 = new Request.Builder()
                .header("Authorization", "Bearer " + tokenResponse_2.getAccessToken())
                .url(serverUrl_2)
                .build();

        CountDownLatch counter_2 = new CountDownLatch(4);
        WebSocket webSocketUser_2 = client.newWebSocket(request_2, new WebSocketListener() {

            public void onOpen(WebSocket webSocket, Response response) {
                System.out.println("Opened web-socket for user-2 on server 2: " + response.toString());
            }

            public void onMessage(WebSocket webSocket, String text) {
                System.out.println("Message received: " + text);
                counter_2.countDown();
            }

            @Override
            public void onFailure(WebSocket webSocket, Throwable t, @Nullable Response response) {
                System.out.println("Failure: " + t.getMessage());
            }
        });

        ConnectMessage subscribe_2 = new ConnectMessage();
        webSocketUser_2.send(mapper.writeValueAsString(subscribe_2));
        //-------------------//

        // wait until users are subscribed
        Thread.sleep(5000);

        // 2 messages from user-2 to user-1
        CommunicateMessage message_1_ToUser_1 = new CommunicateMessage();
        message_1_ToUser_1.setToUser(userId_1);
        message_1_ToUser_1.setMessage("Message 1 from user-2");

        CommunicateMessage message_2_ToUser_1 = new CommunicateMessage();
        message_2_ToUser_1.setToUser(userId_1);
        message_2_ToUser_1.setMessage("Message 2 from user-2");

        webSocketUser_2.send(mapper.writeValueAsString(message_1_ToUser_1));
        webSocketUser_2.send(mapper.writeValueAsString(message_2_ToUser_1));
        //-------------------//

        // 3 messages from user-1 to user-2
        CommunicateMessage message_1_ToUser_2 = new CommunicateMessage();
        message_1_ToUser_2.setToUser(userId_2);
        message_1_ToUser_2.setMessage("Message 1 from user-1");

        CommunicateMessage message_2_ToUser_2 = new CommunicateMessage();
        message_2_ToUser_2.setToUser(userId_2);
        message_2_ToUser_2.setMessage("Message 2 from user-1");

        CommunicateMessage message_3_ToUser_2 = new CommunicateMessage();
        message_3_ToUser_2.setToUser(userId_2);
        message_3_ToUser_2.setMessage("Message 3 from user-1");

        webSocketUser_1.send(mapper.writeValueAsString(message_1_ToUser_2));
        webSocketUser_1.send(mapper.writeValueAsString(message_2_ToUser_2));
        webSocketUser_1.send(mapper.writeValueAsString(message_3_ToUser_2));
        //-------------------//

        counter_1.await();
        counter_2.await();

        assertThat(counter_1.getCount(), is(0L));
        assertThat(counter_2.getCount(), is(0L));

        webSocketUser_1.close(1000, null);
        webSocketUser_2.close(1000, null);
    }

}
