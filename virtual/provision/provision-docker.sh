#!/usr/bin/env bash
#https://docs.docker.com/engine/installation/linux/ubuntu/#install-using-the-repository
apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
apt-add-repository 'deb https://apt.dockerproject.org/repo ubuntu-xenial main'
apt-get update
apt-cache policy docker-engine
apt-get install -y docker-engine
systemctl status docker

groupadd docker
usermod -aG docker ubuntu
usermod -aG sudo ubuntu

curl -L https://github.com/docker/compose/releases/download/1.13.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose


#https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html