#!/usr/bin/env bash

apt-get update
#add-apt-repository ppa:webupd8team/java
#apt-get update
#apt-get install -y oracle-java8-installer
apt-get install -y openjdk-8-jdk-headless
apt-get install -y maven htop

sysctl -w vm.max_map_count=262144

echo "127.0.0.1 rabbitmq osrm postgis authserver" >> /etc/hosts

#grep -q JAVA_HOME /etc/environment || echo 'JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java"' >> /etc/environment
#source /etc/environment && echo $JAVA_HOME
#JAVA_HOME="/usr/lib/jvm/java-8-oracle" >> /etc/environment
