#!/usr/bin/env bash

echo "Adding authentication information for private docker into /home/ubuntu/.docker/config.json"
mkdir -p /home/ubuntu/.docker
cat <<EOF > /home/ubuntu/.docker/config.json
{
    "auths": {
        "${DOCKER_REGISTRY_DOMAIN}:${DOCKER_REGISTRY_PORT}": {
            "auth": "${DOCKER_REGISTRY_BASIC_AUTH}"
        }
    }
}
EOF

echo "Copying CA certificate for private Docker Registry to certificate store at the OS level"
cp /etc/docker/certs.d/"${DOCKER_REGISTRY_DOMAIN}"/ca.crt /usr/local/share/ca-certificates/"${DOCKER_REGISTRY_DOMAIN}"\:"${DOCKER_REGISTRY_PORT}".crt \
   && update-ca-certificates

echo "Checking if CA certificate was registered at the OS level"
ls -l /etc/ssl/certs | grep "${DOCKER_REGISTRY_DOMAIN}"

service docker restart && echo "Restarted the docker daemon"
docker version && echo "Docker has been successfully restarted"
