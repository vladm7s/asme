#!/usr/bin/env bash

echo "Provisioning Docker Registry..."

if [ ! -d /auth ]; then
    mkdir /auth
fi

docker run --entrypoint htpasswd registry:2 -Bbn ${DOCKER_REGISTRY_USERNAME} ${DOCKER_REGISTRY_PASSWORD} > /auth/htpasswd

(docker stop -t=1 registry-srv || true) && (docker rm registry-srv || true)

(docker stop -t=1 registry-web || true) && (docker rm registry-web || true)

# NOTICE!!! GENERATION OF SELF-SIGNED-CERTIFICATE on MacOS DOES NOT PRODUCE WORKABLE RESULTS!!!
#
# Prerequisites:
#
# 1. Edit your /etc/ssl/openssl.cnf on the host where you generate certificates:
#   1.1 Make sure section [req] has: x509_extensions = v3_ca
#   1.2 Add subjectAltName = IP:192.168.33.30 in [v3_ca] section.
#
# 2. openssl req -newkey rsa:4096 -nodes -sha256 -keyout 192.168.33.30/certs/domain.key -x509 -days 3650 -out 192.168.33.30/certs/domain.crt
#
# 3. cp ./192.168.33.30/certs/domain.crt ./192.168.33.30/ca/ca.crt
#
# 4. Examine generated certificate: openssl x509 -text -noout -in domain.crt
#    It should have next:
#       X509v3 extensions:
#            X509v3 Subject Alternative Name:
#                IP Address:192.168.33.30

docker run -d -p ${DOCKER_REGISTRY_PORT}:5000 --restart=always --name=registry-srv \
     -v /auth:/auth \
     -v /certs:/certs \
     -e "REGISTRY_AUTH=htpasswd" \
     -e "REGISTRY_AUTH_HTPASSWD_REALM=Registry Realm" \
     -e REGISTRY_AUTH_HTPASSWD_PATH=/auth/htpasswd \
     -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/${DOCKER_REGISTRY_DOMAIN}/domain.crt \
     -e REGISTRY_HTTP_TLS_KEY=/certs/${DOCKER_REGISTRY_DOMAIN}/domain.key \
     registry:2
#     -e REGISTRY_STORAGE=s3 \
#     -e REGISTRY_STORAGE_S3_ACCESSKEY=${DOCKER_REGISTRY_S3_ACCESS_KEY} \
#     -e REGISTRY_STORAGE_S3_SECRETKEY=${DOCKER_REGISTRY_S3_SECRET_KEY} \
#     -e REGISTRY_STORAGE_S3_REGION=${DOCKER_REGISTRY_S3_REGION} \
#     -e REGISTRY_STORAGE_S3_BUCKET=${DOCKER_REGISTRY_S3_BUCKET} \
#     registry:2

sleep 5

# Probably use Portus? http://port.us.org/
docker run -d -p 8080:8080 --name registry-web --link registry-srv \
           -e REGISTRY_URL=https://registry-srv:${DOCKER_REGISTRY_PORT}/v2 \
           -e REGISTRY_TRUST_ANY_SSL=true \
           -e REGISTRY_BASIC_AUTH="${DOCKER_REGISTRY_BASIC_AUTH}" \
           -e REGISTRY_NAME=${DOCKER_REGISTRY_DOMAIN}:${DOCKER_REGISTRY_PORT} hyper/docker-registry-web