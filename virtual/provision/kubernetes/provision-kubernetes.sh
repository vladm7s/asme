#!/usr/bin/env bash

apt-get update && apt-get install -y apt-transport-https
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb http://apt.kubernetes.io/ kubernetes-xenial main
EOF
apt-get update
apt-get install -y kubelet kubeadm kubectl

docker info | grep -i cgroup
cat /etc/systemd/system/kubelet.service.d/10-kubeadm.conf

systemctl daemon-reload
systemctl restart kubelet

kubeadm init --pod-network-cidr=10.244.0.0/16 --apiserver-advertise-address="${KUBERNETES_MASTER_DOMAIN}"

mkdir -p /home/ubuntu/.kube
cp -i /etc/kubernetes/admin.conf /home/ubuntu/.kube/config
chown ubuntu:ubuntu /home/ubuntu/.kube/config
export KUBECONFIG=/home/ubuntu/.kube/config

kubectl apply -f https://raw.githubusercontent.com/projectcalico/canal/master/k8s-install/1.7/rbac.yaml
kubectl apply -f https://raw.githubusercontent.com/projectcalico/canal/master/k8s-install/1.7/canal.yaml

kubectl get pods --all-namespaces
kubectl taint nodes --all node-role.kubernetes.io/master-

#----------------------- Launch Dashboard and expose as NodePort ---------------------#
#--- https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/ ---#

kubectl create -f https://raw.githubusercontent.com/kubernetes/dashboard/master/src/deploy/recommended/kubernetes-dashboard.yaml
kubectl -n kube-system delete service kubernetes-dashboard
kubectl -n kube-system expose deployment kubernetes-dashboard --type=NodePort
kubectl -n kube-system get service kubernetes-dashboard

#------------------------------ Create admin user for dashboard ----------------------#
#-------- https://github.com/kubernetes/dashboard/wiki/Creating-sample-user ----------#

cat <<EOF >/home/ubuntu/dashboard-admin-user.yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: dashboard-admin-user
  namespace: kube-system
EOF

cat <<EOF >/home/ubuntu/dashboard-admin-user-role-binding.yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: dashboard-admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: dashboard-admin-user
  namespace: kube-system
EOF

kubectl create -f dashboard-admin-user.yaml
kubectl create -f dashboard-admin-user-role-binding.yaml
kubectl -n kube-system describe secret dashboard-admin-user-token > /vagrant/virtual/bin/kubernetes/dashboard-admin-user.token

kubectl create secret docker-registry regcred \
    --docker-server="${DOCKER_REGISTRY_DOMAIN}:${DOCKER_REGISTRY_PORT}" \
    --docker-username="${DOCKER_REGISTRY_USERNAME}" \
    --docker-password="${DOCKER_REGISTRY_PASSWORD}" \
    --docker-email="vladm7s@gmail.com"

#kubectl api-versions
#kubectl -n kube-system get secret
#kubectl -n kube-system describe secret namespace-controller-token