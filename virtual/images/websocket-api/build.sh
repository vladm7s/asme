#!/usr/bin/env bash

echo "Current directory: " $(dirname "$0")

cd "$(dirname "$0")"/../../../

version=$1

cmnd="cp websocket-api/target/websocket-api.jar virtual/images/websocket-api/websocket-api.jar
        && docker build -t asme/websocket-api:${version} virtual/images/websocket-api
        && rm -f virtual/images/websocket-api/websocket-api.jar"

eval ${cmnd}

ret_code=$?

if [ ${ret_code} != 0 ]; then
    printf "Error : [%d] when executing command: '$cmnd' \n" ${ret_code}
    exit ${ret_code}
fi