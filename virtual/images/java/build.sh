#!/usr/bin/env bash

echo "Current directory: " $(dirname "$0")

cd "$(dirname "$0")"/../../../

version=$1

cmnd="docker build -t asme/java:${version} virtual/images/java"

eval ${cmnd}

ret_code=$?

if [ ${ret_code} != 0 ]; then
    printf "Error : [%d] when executing command: '$cmnd' \n" ${ret_code}
    exit ${ret_code}
fi