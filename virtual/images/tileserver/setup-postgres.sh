#!/bin/bash

POSTGRESQL_USER=${POSTGRESQL_USER}
POSTGRESQL_PASS=${POSTGRESQL_PASS}
POSTGRESQL_DB=${POSTGRESQL_DB}

wait_for_postgres() {
    wait_for_postgres_mgs="Waiting for Postgres..."

    until sudo -u postgres psql -c "select 1" postgres > /dev/null 2>&1
    do
        echo ${wait_for_postgres_mgs}
        sleep 5
    done
}

echo "Postgres configuration started..." && \
    /etc/init.d/postgresql start && \
    wait_for_postgres && \
    sudo -u postgres psql -c "CREATE USER $POSTGRESQL_USER SUPERUSER PASSWORD '$POSTGRESQL_PASS';" && \
    sudo -u postgres createdb -E UTF-8 -T template0 ${POSTGRESQL_DB} && \
    sudo -u postgres psql -d ${POSTGRESQL_DB} -c 'CREATE EXTENSION postgis; CREATE EXTENSION hstore;' && \
    /etc/init.d/postgresql stop && \
    echo "Postgres configuration finished!"

ret_code=$?

if [ ${ret_code} != 0 ]; then
    printf "Error : [%d] when configuring Postgres: \n" ${ret_code}
    exit ${ret_code}
fi



