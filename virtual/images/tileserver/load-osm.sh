#!/bin/bash

POSTGRESQL_DB=${POSTGRESQL_DB}
OSM_UKRAINE_VERSION=${OSM_UKRAINE_VERSION}

SKIP_UKRAINE_DOWNLOAD="/usr/share/osm/ukraine-${OSM_UKRAINE_VERSION}.osm.pbf"
if [ ! -f ${SKIP_UKRAINE_DOWNLOAD} ]
then
    cd /usr/share/osm

    wget http://download.geofabrik.de/europe/ukraine-${OSM_UKRAINE_VERSION}.osm.pbf
else
	echo "Skipped download. ukraine-${OSM_UKRAINE_VERSION}.osm.pbf already downloaded."
fi

osm2pgsql -s -C 1024 -S /opt/mapzen/tileserver/vector-datasource/osm2pgsql.style -j /usr/share/osm/ukraine-${OSM_UKRAINE_VERSION}.osm.pbf -d ${POSTGRESQL_DB} -H localhost




