#!/usr/bin/env bash

echo "Current directory: " $(dirname "$0")

cd "$(dirname "$0")"/../../../

version=$1

cmnd="cp api/target/api.jar virtual/images/api/api.jar
        && docker build -t asme/api:${version} virtual/images/api
        && rm -f virtual/images/api/api.jar"

eval ${cmnd}

ret_code=$?

if [ ${ret_code} != 0 ]; then
    printf "Error : [%d] when executing command: '$cmnd' \n" ${ret_code}
    exit ${ret_code}
fi