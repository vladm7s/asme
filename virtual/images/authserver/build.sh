#!/usr/bin/env bash

echo "Current directory: " $(dirname "$0")

cd "$(dirname "$0")"/../../../

version=$1

cmnd="cp authserver/target/authserver.jar virtual/images/authserver/authserver.jar
        && docker build -t asme/authserver:${version} virtual/images/authserver
        && rm -f virtual/images/authserver/authserver.jar"

eval ${cmnd}

ret_code=$?

if [ ${ret_code} != 0 ]; then
    printf "Error : [%d] when executing command: '$cmnd' \n" ${ret_code}
    exit ${ret_code}
fi