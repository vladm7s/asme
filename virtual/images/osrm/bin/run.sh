#!/bin/sh

OSM_UKRAINE_VERSION=${OSM_UKRAINE_VERSION}

SKIP_UKRAINE_DOWNLOAD="/usr/share/osrm/data/ukraine-${OSM_UKRAINE_VERSION}.osm.pbf"

if [ -f ${SKIP_UKRAINE_DOWNLOAD} ]
then
    echo "Skipped download. ukraine-${OSM_UKRAINE_VERSION}.osm.pbf already downloaded."
else
    cd /usr/share/osrm/data

    wget http://download.geofabrik.de/europe/ukraine-${OSM_UKRAINE_VERSION}.osm.pbf
fi

SKIP_UKRAINE_EXTRACT_1="/usr/share/osrm/data/ukraine-${OSM_UKRAINE_VERSION}.osrm"
SKIP_UKRAINE_EXTRACT_2="/usr/share/osrm/data/ukraine-${OSM_UKRAINE_VERSION}.osrm.names"

if [ -f ${SKIP_UKRAINE_EXTRACT_1} ] && [ -f ${SKIP_UKRAINE_EXTRACT_2} ]
then
    echo "Skipped extraction. 'ukraine-${OSM_UKRAINE_VERSION}.osm.pbf' already extracted."
else
    cd /usr/share/osrm/data

    osrm-extract /usr/share/osrm/data/ukraine-${OSM_UKRAINE_VERSION}.osm.pbf -p /opt/car.lua
fi

SKIP_UKRAINE_CONTRACT_1="/usr/share/osrm/data/ukraine-${OSM_UKRAINE_VERSION}.osrm.nodes"
SKIP_UKRAINE_CONTRACT_2="/usr/share/osrm/data/ukraine-${OSM_UKRAINE_VERSION}.osrm.hsgr"

if [ -f ${SKIP_UKRAINE_CONTRACT_1} ] && [ -f ${SKIP_UKRAINE_CONTRACT_2} ]
then
    echo "Skipped contraction. 'ukraine-${OSM_UKRAINE_VERSION}.osm.pbf' already contracted."
else
    cd /usr/share/osrm/data

    osrm-contract /usr/share/osrm/data/ukraine-${OSM_UKRAINE_VERSION}.osrm
fi

osrm-routed /usr/share/osrm/data/ukraine-${OSM_UKRAINE_VERSION}.osrm