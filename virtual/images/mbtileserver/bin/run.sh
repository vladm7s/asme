#!/bin/bash

SKIP_UKRAINE_KYIV_DOWNLOAD="/data/kyiv_ukraine.mbtiles"

if [ -f ${SKIP_UKRAINE_KYIV_DOWNLOAD} ]
then
    echo "Skipped download. kyiv_ukraine.mbtiles already downloaded."
else
    echo "Starting download 'kyiv_ukraine.mbtiles'..."

    cd /data

    wget https://openmaptiles.os.zhdk.cloud.switch.ch/v3.3/extracts/kyiv_ukraine.mbtiles

    echo "Completed download 'kyiv_ukraine.mbtiles'..."
fi

cd /data
cp /ukraine/data/ukraine-170701.mbtiles /data/ukraine-170701.mbtiles

exec /usr/src/app/run.sh /data/ukraine-170701.mbtiles