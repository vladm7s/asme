#!/usr/bin/env bash

echo 'Cleaning not used docker volumes..'
docker volume ls -qf dangling=true | xargs -r docker volume rm

echo 'Cleaning not used docker containers..'
docker rm $(docker ps -qa --no-trunc --filter "status=exited")

echo 'Cleaning not used docker images..'
docker rmi $(docker images | grep "none" | awk '/ / { print $3 }')