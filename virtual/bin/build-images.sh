#!/usr/bin/env bash

cd "$(dirname "$0")"/../../

version=$1

if [ "$1" == "" ]; then
    version="latest"
fi;

cmnd="virtual/images/java/build.sh ${version}
        && virtual/images/postgis/build.sh ${version}
        && virtual/images/rabbitmq/build.sh ${version}
        && virtual/images/api/build.sh ${version}
        && virtual/images/authserver/build.sh ${version}
        && virtual/images/websocket-api/build.sh ${version}
        && virtual/images/elasticsearch/build.sh ${version}
        && virtual/images/osrm/build.sh ${version}
        && virtual/images/mbtileserver/build.sh ${version}"

eval ${cmnd}

ret_code=$?

if [ ${ret_code} != 0 ]; then
    printf "Error : [%d] when executing command: '$cmnd' \n" ${ret_code}
    exit ${ret_code}
fi