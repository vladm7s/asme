#!/usr/bin/env bash

function help_stop {
            echo "Script for stopping ASME"
            echo "Usage:"
            echo "  $(basename $(readlink -f $0)) [options]"
            echo "General options:"
            echo "  -h                          Help"
            echo "  -f <docker-compose-file>    Docker compose file to use"
            echo
}

docker_compose_file=

OPTIND=1
while getopts "hf:" opt
do
  case "$opt" in
    "h") help_stop; exit 0 ;;
    "f") docker_compose_file="--file $OPTARG" ;;
    "?") help_stop; exit 1 ;;
  esac
done
shift $(expr $OPTIND - 1) # remove options from positional parameters

echo -e "Run $(basename $(readlink -f $0)) with parameters:
    \n\t docker_compose_file = $docker_compose_file"

docker-compose ${docker_compose_file} stop