#!/usr/bin/env bash

function help_run {
            echo "Script for running ASME"
            echo "Usage:"
            echo "  $(basename $(readlink -f $0)) [options]"
            echo "General options:"
            echo "  -h                          Help"
            echo "  -f <docker-compose-file>    Docker compose file to use"
            echo "  -s <service>                Service to start"
            echo "  -d                          Daemon mode"
            echo "  -v                          Docker image tag (default: 'latest')"
            echo
}

daemon=
docker_compose_file=
service=
version=latest

OPTIND=1
while getopts "hdf:s:v:" opt
do
  case "$opt" in
    "h") help_run; exit 0 ;;
    "d") daemon="-d" ;;
    "f") docker_compose_file="--file $OPTARG" ;;
    "s") service="$OPTARG" ;;
    "v") version="$OPTARG" ;;
    "?") help_run; exit 1 ;;
  esac
done
shift $(expr $OPTIND - 1) # remove options from positional parameters


echo -e "Run $(basename $(readlink -f $0)) with parameters:
    \n\t docker_compose_file = $docker_compose_file \n\t service = $service \n\t daemon = $daemon \n\t version = $version"

DOCKER_IMAGE_TAG=${version} docker-compose ${docker_compose_file} up ${daemon} ${service}