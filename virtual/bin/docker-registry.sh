#!/usr/bin/env bash

time_start=$SECONDS

BIN_DIR=$(dirname $(readlink -f $0))
ROOT_DIR=$(readlink -f "$BIN_DIR/../../")

DOCKER_REGISTRY_DOMAIN=192.168.33.30
DOCKER_REGISTRY_USERNAME=asme
DOCKER_REGISTRY_PASSWORD=SomeRandomPassword_1234567890

DOCKER_HUB_LOGIN="asme"
DOCKER_REGISTRY_REPO_PREFIX="${DOCKER_REGISTRY_DOMAIN}:5000/$DOCKER_HUB_LOGIN"

ALL_IMAGES=("java" "authserver" "api" "websocket-api" "mbtileserver" "osrm" "postgis" "rabbitmq" "elasticsearch")

# curl -X DELETE http://192.168.33.30:5000/v2/asme/java/tags/...
# https://192.168.33.30:5000/v2/asme/java/tags/list

function help {
            echo "Script for working with private Docker Registry"
            echo "Usage:"
            echo "  $(basename $(readlink -f $0)) [options] CDM"
            echo "General options:"
            echo "  -h              Help"
            echo "  -v <version>    ASME version (docker image tag)"
            echo "  -i <image>      Docker image to push / pull"
            echo "  push            Push to private Docker Registry (${DOCKER_REGISTRY_DOMAIN})"
            echo "  pull            Pull from private Docker Registry (${DOCKER_REGISTRY_DOMAIN})"
            echo
}

function login {
    docker login --username="${DOCKER_REGISTRY_USERNAME}" --password="${DOCKER_REGISTRY_PASSWORD}" ${DOCKER_REGISTRY_DOMAIN}:5000
}

function push {
    declare -a images=("${@}")
    for image in "${images[@]}"
    do
        docker tag "$DOCKER_HUB_LOGIN/${image}:${version}" "$DOCKER_REGISTRY_REPO_PREFIX/${image}:${version}"
        docker $cmd "$DOCKER_REGISTRY_REPO_PREFIX/${image}:${version}"
        docker rmi "$DOCKER_REGISTRY_REPO_PREFIX/${image}:${version}"
    done
}

function pull {
    declare -a images=("${@}")
    for image in "${images[@]}"
    do
        docker $cmd "$DOCKER_REGISTRY_REPO_PREFIX/${image}:${version}"
        docker tag "$DOCKER_REGISTRY_REPO_PREFIX/${image}:${version}" "$DOCKER_HUB_LOGIN/${image}:${version}"
        docker rmi "$DOCKER_REGISTRY_REPO_PREFIX/${image}:${version}"
    done
}

if [ "$1" == "" ]; then
    help
    exit
fi;

cmd=
version=latest
image=

OPTIND=1
while getopts "hv:i:" opt
do
  case "$opt" in
    "h") help; exit 0 ;;
    "v") version="$OPTARG" ;;
    "i") image="$OPTARG" ;;
    "?") help; exit 1 ;;
  esac
done
shift $(expr $OPTIND - 1) # remove options from positional parameters

cmd=$1

if [ "$cmd" == "" ]; then
    echo "Error: command must be defined!"
    help
    exit 1
fi;

echo -e "Run $(basename $(readlink -f $0)) with parameters: \n\t cmd = $cmd \n\t tag = $version \n\t image = $image"

if [ "$cmd" == "push" ]; then
    login

    if [ "$image" == "" ] ; then
        push ${ALL_IMAGES[@]}
    else
        push $image
    fi;
fi;

if [ "$cmd" == "pull" ]; then
    login

    if [ "$image" == "" ] ; then
        pull ${ALL_IMAGES[@]}
    else
        pull $image
    fi;
fi;

time_finish=$SECONDS
time_diff=`expr ${time_finish} - ${time_start}`
echo Time `date +%H:%M:%S -ud @${time_diff}`