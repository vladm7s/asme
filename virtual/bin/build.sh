#!/usr/bin/env bash

cd "$(dirname "$0")"/../../

cmnd="mvn clean install"

eval ${cmnd}

ret_code=$?

if [ ${ret_code} != 0 ]; then
    printf "Error : [%d] when executing command: '$cmnd' \n" ${ret_code}
    exit ${ret_code}
fi