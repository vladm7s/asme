package com.asme.authclient.property;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author Vladyslav Mykhalets
 * @since 23.02.18
 */
@ConfigurationProperties("authclient")
public class AuthClientProperties implements Serializable {

    @NotNull
    private String revokedTokenEndpoint;

    public String getRevokedTokenEndpoint() {
        return revokedTokenEndpoint;
    }

    public void setRevokedTokenEndpoint(String revokedTokenEndpoint) {
        this.revokedTokenEndpoint = revokedTokenEndpoint;
    }
}
