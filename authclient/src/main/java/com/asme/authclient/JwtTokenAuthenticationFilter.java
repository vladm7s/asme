package com.asme.authclient;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import com.asme.authclient.domain.JwtTokenAuthentication;
import com.asme.authclient.service.RevokedTokenService;
import com.asme.common.enums.messaging.CloseReason;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;

/**
 * @author Vladyslav Mykhalets
 * @since 07.02.18
 */
public class JwtTokenAuthenticationFilter extends GenericFilterBean {

    private RevokedTokenService revokedTokenService;

    public JwtTokenAuthenticationFilter(RevokedTokenService revokedTokenService) {
        this.revokedTokenService = revokedTokenService;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        Authentication authentication = getAuthentication((HttpServletRequest) request);

        if (authentication != null) {
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }

        filterChain.doFilter(request,response);
    }

    private Authentication getAuthentication(HttpServletRequest request) throws AuthenticationException {
        String token = request.getHeader(JwtTokenAuthentication.HEADER_STRING);

        if (token != null && token.startsWith(JwtTokenAuthentication.TOKEN_PREFIX)) {

            try {
                // parse the token
                Claims claims = Jwts.parser()
                        .setSigningKey(JwtTokenAuthentication.SECRET)
                        .parseClaimsJws(token.replace(JwtTokenAuthentication.TOKEN_PREFIX, ""))
                        .getBody();

                String tokenId = claims.getId();
                if (revokedTokenService.isRevoked(tokenId)) {
                    throw new BadCredentialsException(CloseReason.TOKEN_REVOKED.name());
                }

                String email = (String) claims.get(JwtTokenAuthentication.EMAIL_CLAIM);
                String userIdAsString = claims.get(JwtTokenAuthentication.USER_ID_CLAIM, String.class);
                Long userId = Long.valueOf(userIdAsString);

                List<String> roles = (List<String>) claims.get(JwtTokenAuthentication.ROLES_CLAIM);
                List<GrantedAuthority> grantedAuthorities = roles.stream()
                        .map(role -> new SimpleGrantedAuthority(role)).collect(Collectors.toList());

                String user = claims.getSubject();
                return user != null ?
                        new JwtTokenAuthentication(tokenId, userId, email, user, null, grantedAuthorities) :
                        null;

            } catch (ExpiredJwtException expiredException) {
                throw new CredentialsExpiredException(CloseReason.TOKEN_EXPIRED.name());
            }
        }

        return null;
    }
}
