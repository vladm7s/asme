package com.asme.authclient.provider;

import java.util.List;

/**
 * @author Vladyslav Mykhalets
 * @since 18.02.18
 */
public interface RevokedTokenListProvider {

    boolean hasNext();

    List<String> next();
}
