package com.asme.authclient.provider;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.asme.authclient.domain.PageWrapper;
import com.asme.authclient.property.AuthClientProperties;

/**
 * @author Vladyslav Mykhalets
 * @since 18.02.18
 */
@Component
@Scope("prototype")
public class RevokedTokenListProviderRestImpl implements RevokedTokenListProvider {

    private static final int SIZE = 100;
    private static final ParameterizedTypeReference<PageWrapper<String>> TYPE_REFERENCE = new ParameterizedTypeReference<PageWrapper<String>>() {};

    @Autowired
    private AuthClientProperties authClientProperties;

    @Autowired
    private RestTemplate restTemplate;

    private Page<String> currentPage;

    @Override
    public boolean hasNext() {
        int page = (currentPage == null) ? 0 : currentPage.getNumber() + 1;
        URI url = UriComponentsBuilder.fromUriString(authClientProperties.getRevokedTokenEndpoint())
                .queryParam("page", page)
                .queryParam("size", SIZE)
                .build()
                .toUri();
        currentPage = restTemplate.exchange(url, HttpMethod.GET, null, TYPE_REFERENCE).getBody();
        return currentPage.getContent().size() > 0;
    }

    @Override
    public List<String> next() {
        if (currentPage == null) {
            throw new IllegalStateException();
        }
        return currentPage.getContent();
    }
}
