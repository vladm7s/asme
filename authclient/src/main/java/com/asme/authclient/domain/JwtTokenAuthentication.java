package com.asme.authclient.domain;

import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

/**
 * @author Vladyslav Mykhalets
 * @since 22.02.18
 */
public class JwtTokenAuthentication extends UsernamePasswordAuthenticationToken {

    public static final String TOKEN_PREFIX = "Bearer";
    public static final String HEADER_STRING = "Authorization";

    public static final String SECRET = "ASME-JWT-ENCODING-SECRET";

    public static final String USER_ID_CLAIM = "USER_ID";
    public static final String EMAIL_CLAIM = "EMAIL";
    public static final String ROLES_CLAIM = "ROLES";
    public static final String PERMISSIONS_CLAIM = "PERMISSIONS";

    private final String tokenId;
    private final Long userId;
    private final String email;

    public JwtTokenAuthentication(String tokenId, Long userId, String email,
                                  Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities) {
        super(principal, credentials, authorities);
        this.tokenId = tokenId;
        this.userId = userId;
        this.email = email;
    }

    public String getTokenId() {
        return tokenId;
    }

    public Long getUserId() {
        return userId;
    }

    public String getEmail() {
        return email;
    }
}
