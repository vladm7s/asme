package com.asme.authclient.domain;

import java.util.List;

import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Vladyslav Mykhalets
 * @since 23.02.18
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PageWrapper<T> extends PageImpl<T> {

    @JsonCreator
    public PageWrapper(@JsonProperty("size") int size,
                       @JsonProperty("number") int number,
                       @JsonProperty("content") List<T> content,
                       @JsonProperty("totalElements") Long totalElements) {
        super(content, new PageRequest(number, size), totalElements);
    }
}
