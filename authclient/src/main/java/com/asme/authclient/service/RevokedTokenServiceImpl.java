package com.asme.authclient.service;

import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asme.authclient.provider.RevokedTokenListProvider;
import com.asme.messaging.dto.messaging.token.TokenType;
import com.asme.messaging.service.messaging.token.TokenMessageListener;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

/**
 * @author Vladyslav Mykhalets
 * @since 12.02.18
 */
@Service
public class RevokedTokenServiceImpl implements RevokedTokenService, TokenMessageListener {

    @Autowired
    private RevokedTokenListProvider revokedTokenListProvider;

    private final Cache<String, Boolean> cache = CacheBuilder.newBuilder()
            .expireAfterWrite(30, TimeUnit.MINUTES) // set time-to-live to maximum possible token can live
            .build();

    @PostConstruct
    public void postConstruct() {
        while (revokedTokenListProvider.hasNext()) {
            revokedTokenListProvider.next().forEach(tokenId -> cache.put(tokenId, true));
        }
    }

    @Override
    public void onRevoke(TokenType tokenType, String tokenId) {
        cache.put(tokenId, true);
    }

    public boolean isRevoked(String tokenId) {
        Boolean revoked = cache.getIfPresent(tokenId);
        return revoked != null && revoked.booleanValue();
    }
}
