package com.asme.authclient.service;

/**
 * @author Vladyslav Mykhalets
 * @since 16.02.18
 */
public interface RevokedTokenService {

    boolean isRevoked(String tokenId);
}
