package com.asme.authclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.Http401AuthenticationEntryPoint;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.client.RestTemplate;

import com.asme.authclient.property.AuthClientProperties;
import com.asme.authclient.service.RevokedTokenService;

/**
 * @author Vladyslav Mykhalets
 * @since 07.02.18
 */
@Configuration
@EnableWebSecurity
@ComponentScan({"com.asme.authclient.service", "com.asme.authclient.provider"})
@EnableConfigurationProperties(AuthClientProperties.class)
@PropertySource("authclient.properties")
public class JwtTokenAuthClient extends WebSecurityConfigurerAdapter {

    @Autowired
    private RevokedTokenService revokedTokenService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // @formatter:off
        http
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/web-socket/api/health").permitAll()
                .anyRequest().authenticated()
            .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
                .exceptionHandling()
                .authenticationEntryPoint(new Http401AuthenticationEntryPoint("Bearer"))
            .and()
                .csrf().disable()
                .addFilterBefore(new JwtTokenAuthenticationFilter(revokedTokenService), BasicAuthenticationFilter.class);
        // @formatter:on
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.setConnectTimeout(60000).build();
    }
}
