package com.asme.websocket.api.handler;

import java.io.IOException;

import org.springframework.web.socket.WebSocketSession;

import com.asme.authclient.domain.JwtTokenAuthentication;

/**
 * @author Vladyslav Mykhalets
 * @since 10.04.18
 */
public interface WebSocketAuthenticator {

    JwtTokenAuthentication extractAuthentication(WebSocketSession session);

    boolean isAuthenticated(WebSocketSession session) throws IOException;
}
