package com.asme.websocket.api.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.server.support.HttpSessionHandshakeInterceptor;

import com.asme.websocket.api.handler.ApiRequestHandler;

/**
 * @author Vladyslav Mykhalets
 * @since 21.01.18
 */
@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(handler(), "/handler")
                .addInterceptors(new HttpSessionHandshakeInterceptor());
    }

//    @Bean
//    public SimpleUrlHandlerMapping handlerMapping() {
//
//        Map<String, Object> urlMap = new HashMap<String, Object>();
//        urlMap.put("/echoWebSocketHandler", new WebSocketHttpRequestHandler(new EchoWebSocketHandler()));
//
//        SimpleUrlHandlerMapping hm = new SimpleUrlHandlerMapping();
//        hm.setOrder(0);
//        hm.setUrlMap(urlMap);
//        return hm;
//    }

    @Bean
    public WebSocketHandler handler() {
        return new ApiRequestHandler();
    }
}
