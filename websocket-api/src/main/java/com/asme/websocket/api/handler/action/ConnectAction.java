package com.asme.websocket.api.handler.action;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import com.asme.common.dto.messaging.WebSocketMessage;
import com.asme.common.dto.messaging.reply.ConnectReply;
import com.asme.common.enums.messaging.CloseReason;

/**
 * @author Vladyslav Mykhalets
 * @since 07.04.18
 */
@Component
@Scope("prototype")
public class ConnectAction extends BaseHandlerAction {

    public ConnectAction(WebSocketSession session) {
        super(session);
    }

    public void execute(WebSocketMessage message) {
        log.info("Connecting user '{}', id = '{}'", authentication.getName());

        boolean result = userMessagingService.subscribeUser(authentication.getUserId(), incomingMessage -> {
            try {
                if (revokedTokenService.isRevoked(authentication.getTokenId())) {

                    log.warn("Can not receive message, because access token was revoked.\n" +
                            "Disconnecting user '{}', id = '{}'", authentication.getName());

                    userMessagingService.unsubscribeUser(authentication.getUserId());

                    session.close(new CloseStatus(CloseStatus.SERVER_ERROR.getCode(), CloseReason.TOKEN_REVOKED.name()));
                } else {

                    log.info("Received message for user '{}', id = '{}': \n {}", authentication.getName(), authentication.getUserId(), incomingMessage);

                    session.sendMessage(new TextMessage(incomingMessage));
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });

        ConnectReply connectReply = new ConnectReply();
        connectReply.setConnected(result);

        try {
            String response = mapper.writeValueAsString(connectReply);
            session.sendMessage(new TextMessage(response));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }
}
