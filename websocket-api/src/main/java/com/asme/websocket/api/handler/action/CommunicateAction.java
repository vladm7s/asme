package com.asme.websocket.api.handler.action;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import com.asme.common.dto.messaging.CommunicateMessage;
import com.asme.common.dto.messaging.WebSocketMessage;

/**
 * @author Vladyslav Mykhalets
 * @since 07.04.18
 */
@Component
@Scope("prototype")
public class CommunicateAction extends BaseHandlerAction {

    public CommunicateAction(WebSocketSession session) {
        super(session);
    }

    public void execute(WebSocketMessage message) {
        CommunicateMessage communicateMessage = (CommunicateMessage) message;

        log.info("Sending message to user id '{}': \n {}", communicateMessage.getToUser(), communicateMessage.getMessage());

        userMessagingService.sendMessageToUser(communicateMessage.getToUser(), communicateMessage.getMessage());
    }
}
