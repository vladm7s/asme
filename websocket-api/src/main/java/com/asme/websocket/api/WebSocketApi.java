package com.asme.websocket.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.asme.authclient.JwtTokenAuthClient;
import com.asme.core.CoreConfiguration;
import com.asme.messaging.configuration.MessagingConfiguration;
import com.asme.websocket.api.configuration.ServletConfig;
import com.asme.websocket.api.configuration.WebSocketConfig;

/**
 * @author Vladyslav Mykhalets
 * @since 21.01.18
 */
@SpringBootApplication
@Import(value = {
        ServletConfig.class,
        WebSocketConfig.class,
        CoreConfiguration.class,
        MessagingConfiguration.class,
        JwtTokenAuthClient.class
})
public class WebSocketApi {

    public static void main(String[] args) {
        SpringApplication.run(WebSocketApi.class, args);
    }
}
