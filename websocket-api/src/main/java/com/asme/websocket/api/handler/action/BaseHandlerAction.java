package com.asme.websocket.api.handler.action;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.WebSocketSession;

import com.asme.authclient.domain.JwtTokenAuthentication;
import com.asme.authclient.service.RevokedTokenService;
import com.asme.messaging.service.messaging.user.UserMessagingService;
import com.asme.websocket.api.handler.WebSocketAuthenticator;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Vladyslav Mykhalets
 * @since 09.04.18
 */
public abstract class BaseHandlerAction implements HandlerAction {

    protected static final ObjectMapper mapper = new ObjectMapper();

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    protected final WebSocketSession session;

    protected JwtTokenAuthentication authentication;

    @Autowired
    protected UserMessagingService userMessagingService;

    @Autowired
    protected RevokedTokenService revokedTokenService;

    @Autowired
    protected WebSocketAuthenticator webSocketAuthenticator;

    protected BaseHandlerAction(WebSocketSession session) {
        this.session = session;
    }

    @PostConstruct
    void postConstruct() {
        this.authentication = webSocketAuthenticator.extractAuthentication(session);
    }

}
