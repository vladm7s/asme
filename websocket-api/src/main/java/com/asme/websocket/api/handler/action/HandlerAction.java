package com.asme.websocket.api.handler.action;

import com.asme.common.dto.messaging.WebSocketMessage;

/**
 * @author Vladyslav Mykhalets
 * @since 09.04.18
 */
public interface HandlerAction {

    void execute(WebSocketMessage message);
}
