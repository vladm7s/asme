package com.asme.websocket.api.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import com.asme.common.dto.messaging.MessageType;
import com.asme.common.dto.messaging.WebSocketMessage;
import com.asme.websocket.api.handler.action.CommunicateAction;
import com.asme.websocket.api.handler.action.ConnectAction;
import com.asme.websocket.api.handler.action.HandlerAction;
import com.asme.websocket.api.handler.action.StartRequestAction;
import com.asme.websocket.api.handler.action.StartRouteAction;

/**
 * @author Vladyslav Mykhalets
 * @since 09.04.18
 */
@Component
public class HandlerActionFactory {

    @Autowired
    private ApplicationContext applicationContext;

    public HandlerAction create(WebSocketMessage message, WebSocketSession session) {
        MessageType messageType = MessageType.valueOf(message.getType());

        switch (messageType) {
            case CONNECT: {
                return applicationContext.getBean(ConnectAction.class, session);
            }
            case COMMUNICATE: {
                return applicationContext.getBean(CommunicateAction.class, session);
            }
            case START_REQUEST: {
                return applicationContext.getBean(StartRequestAction.class, session);
            }
            case START_ROUTE: {
                return applicationContext.getBean(StartRouteAction.class, session);
            }
            default: {
                throw new IllegalArgumentException("Message type " + messageType + " not supported");
            }
        }
    }

}
