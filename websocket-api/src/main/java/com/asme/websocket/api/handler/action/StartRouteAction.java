package com.asme.websocket.api.handler.action;

import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import com.asme.common.dto.messaging.WebSocketMessage;
import com.asme.common.dto.messaging.notification.MatchedRouteNotification;
import com.asme.common.dto.messaging.reply.StartRouteReply;
import com.asme.common.dto.messaging.request.StartRouteMessage;
import com.asme.core.entity.jpa.JpaDriveRoute;
import com.asme.core.service.jpa.JpaDriveRequestService;
import com.asme.core.service.jpa.JpaDriveRouteService;

/**
 * @author Vladyslav Mykhalets
 * @since 09.04.18
 */
@Component
@Scope("prototype")
public class StartRouteAction extends BaseHandlerAction {

    @Autowired
    private JpaDriveRouteService driveRouteService;

    @Autowired
    private JpaDriveRequestService driveRequestService;

    public StartRouteAction(WebSocketSession session) {
        super(session);
    }

    @Override
    public void execute(WebSocketMessage message) {
        StartRouteMessage startRouteMessage = (StartRouteMessage) message;

        CompletableFuture<JpaDriveRoute> driveRouteFuture = driveRouteService.createFromPlanRouteAsync(
                startRouteMessage.getPlanRouteId(), authentication.getUserId(), authentication.getName());

        driveRouteFuture.thenApply(driveRoute -> {
            try {
                StartRouteReply startRouteReply = new StartRouteReply();
                startRouteReply.setDriveRouteId(driveRoute.getId());

                String response = mapper.writeValueAsString(startRouteReply);
                session.sendMessage(new TextMessage(response));

                return driveRoute;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }).thenAccept(driveRoute -> {
            try {
                MatchedRouteNotification notification = new MatchedRouteNotification();

                notification.setDriveRouteId(driveRoute.getId());
                notification.setUserId(driveRoute.getUserId());
                notification.setUsername(driveRoute.getUsername());
                notification.setGeometry(driveRoute.getGeometry());

                String notificationAsString = mapper.writeValueAsString(notification);

                driveRequestService.findByRouteLine(authentication.getUserId(), driveRoute.getRoute(),
                        driveRequest -> userMessagingService.sendMessageToUser(driveRequest.getUserId(), notificationAsString));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }
}
