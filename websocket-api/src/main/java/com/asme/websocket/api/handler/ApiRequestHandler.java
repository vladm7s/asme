package com.asme.websocket.api.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.AbstractWebSocketHandler;

import com.asme.authclient.domain.JwtTokenAuthentication;
import com.asme.common.dto.messaging.WebSocketMessage;
import com.asme.messaging.service.messaging.user.UserMessagingService;
import com.asme.websocket.api.handler.action.HandlerAction;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Vladyslav Mykhalets
 * @since 21.01.18
 */
public class ApiRequestHandler extends AbstractWebSocketHandler {
    private static final Logger log = LoggerFactory.getLogger(ApiRequestHandler.class);

    private static final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private HandlerActionFactory handlerActionFactory;

    @Autowired
    private WebSocketAuthenticator webSocketAuthenticator;

    @Autowired
    protected UserMessagingService userMessagingService;

    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage textMessage) throws Exception {

        if (!webSocketAuthenticator.isAuthenticated(session)) {
            return;
        }

        WebSocketMessage webSocketMessage = mapper.readValue(textMessage.getPayload(), WebSocketMessage.class);
        HandlerAction action = handlerActionFactory.create(webSocketMessage, session);
        action.execute(webSocketMessage);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        super.afterConnectionClosed(session, status);

        JwtTokenAuthentication authentication = (JwtTokenAuthentication) session.getPrincipal();

        if (authentication != null) {
            log.warn("Connection closed.\n" + "Disconnecting user '{}', id '{}'", authentication.getName(), authentication.getUserId());
            userMessagingService.unsubscribeUser(authentication.getUserId());
        }
    }
}
