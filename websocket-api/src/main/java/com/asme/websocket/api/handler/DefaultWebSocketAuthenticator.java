package com.asme.websocket.api.handler;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketSession;

import com.asme.authclient.domain.JwtTokenAuthentication;
import com.asme.authclient.service.RevokedTokenService;
import com.asme.common.enums.messaging.CloseReason;
import com.asme.messaging.service.messaging.user.UserMessagingService;

/**
 * @author Vladyslav Mykhalets
 * @since 10.04.18
 */
@Component
public class DefaultWebSocketAuthenticator implements WebSocketAuthenticator {

    private static final Logger log = LoggerFactory.getLogger(DefaultWebSocketAuthenticator.class);

    @Autowired
    private UserMessagingService userMessagingService;

    @Autowired
    private RevokedTokenService revokedTokenService;

    @Override
    public JwtTokenAuthentication extractAuthentication(WebSocketSession session) {
        return (JwtTokenAuthentication) session.getPrincipal();
    }

    @Override
    public boolean isAuthenticated(WebSocketSession session) throws IOException {

        JwtTokenAuthentication authentication = (JwtTokenAuthentication) session.getPrincipal();
        if (revokedTokenService.isRevoked(authentication.getTokenId())) {

            log.warn("Can not handle message, because access token was revoked.\n" +
                    "Disconnecting user '{}', id '{}'", authentication.getName(), authentication.getUserId());

            userMessagingService.unsubscribeUser(authentication.getUserId());

            session.close(new CloseStatus(CloseStatus.SERVER_ERROR.getCode(), CloseReason.TOKEN_REVOKED.name()));
            return false;
        }

        return true;
    }
}
