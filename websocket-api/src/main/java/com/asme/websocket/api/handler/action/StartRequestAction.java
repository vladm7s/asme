package com.asme.websocket.api.handler.action;

import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import com.asme.common.dto.messaging.WebSocketMessage;
import com.asme.common.dto.messaging.notification.MatchedRequestNotification;
import com.asme.common.dto.messaging.reply.StartRequestReply;
import com.asme.common.dto.messaging.request.StartRequestMessage;
import com.asme.core.entity.jpa.JpaDriveRequest;
import com.asme.core.service.jpa.JpaDriveRequestService;
import com.asme.core.service.jpa.JpaDriveRouteService;
import com.asme.core.utils.GeoUtils;

/**
 * @author Vladyslav Mykhalets
 * @since 09.04.18
 */
@Component
@Scope("prototype")
public class StartRequestAction extends BaseHandlerAction {

    @Autowired
    private JpaDriveRouteService driveRouteService;

    @Autowired
    private JpaDriveRequestService driveRequestService;

    public StartRequestAction(WebSocketSession session) {
        super(session);
    }

    @Override
    public void execute(WebSocketMessage message) {
        StartRequestMessage startRequestMessage = (StartRequestMessage) message;

        CompletableFuture<JpaDriveRequest> driveRequestFuture = driveRequestService.createFromPlanRequestAsync(
                startRequestMessage.getPlanRequestId(), authentication.getUserId(), authentication.getName());

        driveRequestFuture.thenApply(driveRequest -> {
            try {
                StartRequestReply startRequestReply = new StartRequestReply();
                startRequestReply.setDriveRequestId(driveRequest.getId());

                String response = mapper.writeValueAsString(startRequestReply);
                session.sendMessage(new TextMessage(response));

                return driveRequest;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }).thenAccept(driveRequest -> {
            try {
                MatchedRequestNotification notification = new MatchedRequestNotification();

                notification.setDriveRequestId(driveRequest.getId());
                notification.setUserId(driveRequest.getUserId());
                notification.setUsername(driveRequest.getUsername());
                notification.setStartLocation(GeoUtils.toGeoPoint(driveRequest.getStartLocation()));
                notification.setDestination(GeoUtils.toGeoPoint(driveRequest.getDestinationLocation()));

                String notificationAsString = mapper.writeValueAsString(notification);

                driveRouteService.findByStartAndDestination(authentication.getUserId(),
                        driveRequest.getStartLocation(),
                        driveRequest.getDestinationLocation(),
                        driveRequest.getStartLocationBoundingBox(),
                        driveRequest.getDestinationLocationBoundingBox(),
                        driveRoute -> userMessagingService.sendMessageToUser(driveRoute.getUserId(), notificationAsString));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }
}
