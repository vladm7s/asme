package com.asme.websocket.api;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

import com.asme.authclient.service.RevokedTokenService;
import com.asme.common.dto.messaging.ConnectMessage;
import com.asme.common.utils.WebSocketUtils;
import com.asme.messaging.service.messaging.user.UserMessagingService;
import com.asme.websocket.api.WebSocketApiTest.MocksConfig;
import com.asme.websocket.api.configuration.TestWebSocketApi;
import com.asme.websocket.api.handler.HandlerActionFactory;
import com.asme.websocket.api.handler.WebSocketAuthenticator;
import com.fasterxml.jackson.databind.ObjectMapper;

import static org.easymock.EasyMock.createMock;
import static org.junit.Assert.fail;
import okhttp3.OkHttpClient;
import okhttp3.WebSocket;

/**
 * @author Vladyslav Mykhalets
 * @since 09.04.18
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
        TestWebSocketApi.class,
        MocksConfig.class
}, webEnvironment = WebEnvironment.RANDOM_PORT)
public class WebSocketApiTest {

    private ObjectMapper mapper = new ObjectMapper();

    @LocalServerPort
    private int port;

    @Test
    public void startRequestMessageTest() throws Exception {

        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(300,  TimeUnit.SECONDS)
                .build();

        CountDownLatch openSocketCounter = new CountDownLatch(1);
        CountDownLatch closeSocketCounter = new CountDownLatch(1);

        WebSocket webSocket = WebSocketUtils.openWebSocket(client, "ws://localhost:" + port + "/web-socket/api/handler",
                null,
                response -> { openSocketCounter.countDown(); },
                text -> {},
                (exception, response) -> {},
                closeReason -> { closeSocketCounter.countDown(); }
        );

        ConnectMessage connectMessage_1 = new ConnectMessage();
        webSocket.send(mapper.writeValueAsString(connectMessage_1));

        if (!openSocketCounter.await(10, TimeUnit.SECONDS)) {
            fail("WebSocket not opened");
        }

        webSocket.close(1000, null);

        if (!closeSocketCounter.await(10, TimeUnit.SECONDS)) {
            fail("WebSocket not closed");
        }
    }

    @Configuration
    public static class MocksConfig {

        @Bean
        public WebSocketAuthenticator webSocketAuthenticator() {
            return createMock(WebSocketAuthenticator.class);
        }

        @Bean
        public HandlerActionFactory handlerActionFactory() {
            return new HandlerActionFactory();
        }

        @Bean
        public UserMessagingService userMessagingService() {
            return createMock(UserMessagingService.class);
        }

        @Bean
        public RevokedTokenService revokedTokenService() {
            return createMock(RevokedTokenService.class);
        }
    }

}
