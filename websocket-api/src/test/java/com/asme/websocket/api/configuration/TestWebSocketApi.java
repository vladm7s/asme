package com.asme.websocket.api.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.web.EmbeddedServletContainerAutoConfiguration;
import org.springframework.boot.autoconfigure.web.ServerPropertiesAutoConfiguration;
import org.springframework.boot.autoconfigure.websocket.WebSocketAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.asme.websocket.api.WebSocketApi;

/**
 * @author Vladyslav Mykhalets
 * @since 09.04.18
 */
@Configuration
@Import(value = {
        ServletConfig.class,
        WebSocketConfig.class,
        WebSocketAutoConfiguration.class,
        ServerPropertiesAutoConfiguration.class,
        EmbeddedServletContainerAutoConfiguration.class
})
public class TestWebSocketApi {

    public static void main(String[] args) {
        SpringApplication.run(WebSocketApi.class, args);
    }

}
